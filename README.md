## Welcome to Evelynn Collections

Hi! I'm [Chandrakant Kori](https://www.linkedin.com/in/chandrakant-kori/) and I have been developing advance data structures with optimistic approach.
I have created below collections. 

## Bag
Type |Java Compatible Class |  Iterator Class                          |
|----------------|-------------------------------|-----------------------------|
|`Bag`| `N/A` |`N/A`|
|`HashBag`| `N/A` |`N/A`|


## List
Type |Java Compatible Class |  Iterator Class                          |
|----------------|-------------------------------|-----------------------------|
|`ArrayList`| `JavaCompatibleArrayList`           |`N/A`|
|`SinglyLinkedList`| `JavaCompatibleSinglyLinkedList`           |`SinglyLinkedListIterator`|
|`DoublyLinkedList`| `JavaCompatibleDoublyLinkedList`           |`DoublyLinkedListIterator`|
|`ImplicitKeyTreap`| `JavaCompatibleImplicitKeyTreap`           |`N/A`|

## Map
Type |Java Compatible Class |  Iterator Class                          |
|----------------|-------------------------------|-----------------------------|
|`ChainingHashMap`| `JavaCompatibleChainingHashMap`           |`N/A`|
|`LinearProbingHashMap`| `JavaCompatibleLinearProbingHashMap`           |`N/A`|
|`HashMap`|`N/A`|`JavaCompatibleHashMapIterator`|
|`HashArrayMappedTrie`|`N/A`|`N/A`|
|`RadixTrie`| `JavaCompatibleRadixTrieMap`           |`JavaCompatibleIterator`|
|`SkipListMap`| `JavaCompatibleSkipListMap`           |`JavaCompatibleSkipListIterator`|	
|`TreeMap`| `JavaCompatibleTreeMap`           |`JavaCompatibleTreeMapIterator`|
|`TrieMap`| `JavaCompatibleTrieMap`           |`JavaCompatibleTrieMapIterator`|

## Heap
Type |Java Compatible Class |  Iterator Class |
|----------------|-------------------------------|-----------------------------|
|`BinaryHeapTree`| `JavaCompatibleBinaryHeapTree` |`BinaryHeapArrayIterator`|
|`BinaryHeapArray`| `JavaCompatibleBinaryHeapArray` |`BinaryHeapTreeIterator`|

## Tree
Type |Java Compatible Class |  Iterator Class |
|----------------|-------------------------------|-----------------------------|
|`BinarySearchTree`| `JavaCompatibleBinarySearchTree` |`BinarySearchTreeIterator`|
|`TernarySearchTree`| `JavaCompatibleTernaryTree` |`TernarySearchTreeIterator`|
|`BTree`| `JavaCompatibleBTree` |`BTreeIterator`|
|`RedBlackTree`| `JavaCompatibleRedBlackTree` |`RedBlackTreeIterator`|
|`AVLTree`| `N/A` |`N/A`|
|`FenwickTree`| `N/A` |`N/A`|
|`IntervalTree`| `N/A` |`N/A`|
|`KDTree`| `N/A` |`N/A`|
|`QuadTree`| `N/A` |`N/A`|
|`PointRegionQuadTree`| `N/A` |`N/A`|
|`MxCifQuadTree`| `N/A` |`N/A`|
|`SegmentTree`| `N/A` |`N/A`|
|`DynamicSegmentTree`| `N/A` |`N/A`|
|`FlatSegmentTree`| `N/A` |`N/A`|
|`SuffixTree`| `N/A` |`N/A`|

## Trie
Type |Java Compatible Class |  Iterator Class |
|----------------|-------------------------------|-----------------------------|
|`Trie`| `JavaCompatibleTrie` |`TrieIterator`|
|`PatriciaTrie`| `JavaCompatiblePatriciaTrie` |`PatriciaTrieIterator`|
|`RadixTrie`| `JavaCompatibleRadixTrieMap`|`JavaCompatibleIterator`|
|`SuffixTrie`| `N/A` |`N/A`|
|`CompactSuffixTrie`| `N/A` |`N/A`|

## Set
Type |Java Compatible Class |  Iterator Class |
|----------------|-------------------------------|-----------------------------|
|`SkipListSet`| `JavaCompatibleSkipListSet` |`SkipListSetIterator`|
|`DisjointSet`| `N/A` |`N/A`|

## Stack
Type |Java Compatible Class |  Iterator Class |
|----------------|-------------------------------|-----------------------------|
|`ArrayStack`| `JavaCompatibleArrayStack` |`ArrayStackIterator`|
|`LinkedStack`| `JavaCompatibleLinkedStack` |`LinkedStackIterator`|


## Queue
Type |Java Compatible Class |  Iterator Class |
|----------------|-------------------------------|-----------------------------|
|`ArrayQueue`| `JavaCompatibleArrayQueue` |`ArrayQueueIterator`|
|`LinkedQueue`| `JavaCompatibleLinkedQueue` |`LinkedQueueIterator`|

##  ND Array
Type |Java Compatible Class |  Iterator Class |
|----------------|-------------------------------|-----------------------------|
|`Graph`| `N/A` |`N/A`|
|`Matrix`| `N/A` |`N/A`|
|`LCPArray`| `N/A` |`N/A`|
|`SuffixArray`| `N/A` |`N/A`|
