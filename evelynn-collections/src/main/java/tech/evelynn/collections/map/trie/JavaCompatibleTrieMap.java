package tech.evelynn.collections.map.trie;

import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;

import tech.evelynn.collections.trie.TrieNode;

@SuppressWarnings("unchecked")
public class JavaCompatibleTrieMap<K extends CharSequence, V> extends AbstractMap<K, V> {

	private TrieMap<K, V> map = null;

	protected JavaCompatibleTrieMap(TrieMap<K, V> map) {
		this.map = map;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V put(K key, V value) {
		return map.put(key, value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public V remove(Object key) {
		return map.remove((K) key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		map.clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsKey(Object key) {
		return map.contains((K) key);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return map.size();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.util.Set<java.util.Map.Entry<K, V>> entrySet() {
		java.util.Set<java.util.Map.Entry<K, V>> set = new java.util.HashSet<java.util.Map.Entry<K, V>>() {

			private static final long serialVersionUID = 1L;

			/**
			 * {@inheritDoc}
			 */
			@Override
			public java.util.Iterator<java.util.Map.Entry<K, V>> iterator() {
				return (new JavaCompatibleTrieMapIterator<K, V>(map, super.iterator()));
			}
		};
		if (map.getTrie() != null && map.getTrie().getRoot() != null) {
			TrieNode n = map.getTrie().getRoot();
			levelOrder(n, "", set);
		}
		return set;
	}

	private void levelOrder(TrieNode node, String string, Set<Map.Entry<K, V>> set) {
		StringBuilder builder = new StringBuilder(string);
		TrieMapNode<V> tmn = null;
		if (node instanceof TrieMapNode) {
			tmn = (TrieMapNode<V>) node;
			if (tmn.getCharacter() != TrieNode.SENTINAL)
				builder.append(tmn.getCharacter());
			if (tmn.isWord()) {
				K s = (K) builder.toString();
				java.util.Map.Entry<K, V> entry = new JavaCompatibleTrieMapEntry<K, V>(s, tmn.value);
				set.add(entry);
			}
		}

		String b = builder.toString();
		for (int i = 0; i < node.getChildrenSize(); i++) {
			TrieNode n = node.getChild(i);
			levelOrder(n, b, set);
		}
	}
}
