package tech.evelynn.collections.map.trie;

import tech.evelynn.collections.trie.TrieNode;

@SuppressWarnings("unchecked")
public class TrieMapPrinter {

	public static <K extends CharSequence, V> String getString(TrieMap<K, V> map) {
        return getString(map.getTrie().getRoot(), "", null, true);
    }

    
	protected static <V> String getString(TrieNode node, String prefix, String previousString, boolean isTail) {
        StringBuilder builder = new StringBuilder();
        String string = null;
        if (node.getCharacter() != TrieNode.SENTINAL) {
            String temp = String.valueOf(node.getCharacter());
            if (previousString != null) {
                string = previousString + temp;
            } else {
                string = temp;
            }
        }
        if (node instanceof TrieMapNode) {
            TrieMapNode<V> hashNode = (TrieMapNode<V>) node;
            builder.append(prefix
                    + (isTail ? "└── " : "├── ")
                    + ((node.isWord()) ? 
                          ("(" + String.valueOf(node.getCharacter()) + ") " + string + " = {" + hashNode.value + "}")
                      : 
                          string)
                    + "\n");
        }
        if (node.getChildrenSize() > 0) {
            for (int i = 0; i < node.getChildrenSize() - 1; i++) {
                builder.append(getString(node.getChild(i), prefix + (isTail ? "    " : "│   "), string, false));
            }
            if (node.getChildrenSize() >= 1) {
                builder.append(getString(node.getChild(node.getChildrenSize() - 1), prefix
                        + (isTail ? "    " : "│   "), string, true));
            }
        }

        return builder.toString();
    }
}
