package tech.evelynn.collections.map.skiplist;

import tech.evelynn.collections.map.Map;
import tech.evelynn.collections.set.skiplist.SkipListNode;
import tech.evelynn.collections.set.skiplist.SkipListNodeCreator;
import tech.evelynn.collections.set.skiplist.SkipListSet;

@SuppressWarnings("unchecked")
public class SkipListMap<K extends Comparable<K>, V> implements SkipListNodeCreator<K>, Map<K,V> {
	
	private SkipListSet<K> list = null;

    public SkipListMap() {
        setList(new SkipListSet<K>(this));
    }
    
    @Override
    public V put(K key, V value) {
        V prev = null;
        SkipListNode<K> node = getList().addValue(key);
        if (node instanceof SkipListMapNode) {
            SkipListMapNode<K, V> treeMapNode = (SkipListMapNode<K, V>) node;
            if (treeMapNode.value!=null) prev = treeMapNode.value;
            treeMapNode.value = value;
        }

        return prev;
    }
    
    @Override
    public V get(K key) {
        SkipListNode<K> node = getList().getNode(key);
        if (node instanceof SkipListMapNode) {
            SkipListMapNode<K, V> mapNode = (SkipListMapNode<K, V>) node;
            return mapNode.value;
        }
        return null;
    }

    @Override
    public boolean contains(K key) {
        return getList().contains(key);
    }
    
    @Override
    public V remove(K key) {
        SkipListNode<K> node = getList().removeValue(key);
        V value = null;
        if (node instanceof SkipListMapNode) {
			SkipListMapNode<K, V> treeMapNode = (SkipListMapNode<K, V>) node;
            value = treeMapNode.value;
            treeMapNode.setData(null);
            treeMapNode.value = null;
        }
        return value;
    }

    @Override
    public void clear() {
        getList().clear();
    }
    
    @Override
    public int size() {
        return getList().size();
    }

    @Override
    public java.util.Map<K,V> toMap() {
        return (new JavaCompatibleSkipListMap<K,V>(this));
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        if (getList()!=null && getList().getHead()!=null) {
            SkipListNode<K> node = getList().getHead();
            while (node!=null) {
                if (!(node instanceof SkipListMapNode)) continue;

                SkipListMapNode<K,V> sln = (SkipListMapNode<K, V>) node;
                builder.append(sln.getData()).append("=").append(sln.value);

                node = node.getNext(0);
                if (node!=null) builder.append("\n");
            }
        }
        return builder.toString();
    }
    
    @Override
    public SkipListNode<K> createNewNode(int level, K key) {
        return (new SkipListMapNode<K, V>(level, key));
    }
    
    @Override
    public void swapNode(SkipListNode<K> node, SkipListNode<K> next) {
        K key = node.getData();
        node.setData(next.getData());
        next.setData(key);
        if (node instanceof SkipListMapNode && next instanceof SkipListMapNode) {
            SkipListMapNode<K,V> node2 = (SkipListMapNode<K,V>) node;
            SkipListMapNode<K,V> next2 = (SkipListMapNode<K,V>) next;
            V value = node2.value;
            node2.value = next2.value;
            next2.value = value;
        }
    }

	public SkipListSet<K> getList() {
		return list;
	}

	public void setList(SkipListSet<K> list) {
		this.list = list;
	}
    
    
}
