package tech.evelynn.collections.map;

import tech.evelynn.collections.list.ArrayList;
import tech.evelynn.collections.list.List;

@SuppressWarnings("unchecked")
public class ChainingHashMap<K, V> extends HashMap<K, V> {

	private float loadFactor = 10.0f;
	private int minimumSize = 1024;
	private int initialListSize = 10;
	private ArrayList<Pair<K, V>>[] array = null;
	private int size = 0;

	public ChainingHashMap(int size) {
		super(Type.CHAINING);
		initializeMap(size);
	}

	public ChainingHashMap() {
		super(Type.CHAINING);
		initializeMap(minimumSize);
	}

	@Override
	public V put(K key, V value) {
		return put(new Pair<K, V>(key, value));
	}

	public V put(Pair<K, V> newPair) {
		int index = indexOf(newPair.getKey().hashCode());
		List<Pair<K, V>> list = getArray()[index];
		V prev = null;
		boolean exist = false;
		// Do not add duplicates
		for (Pair<K, V> p : list.toList()) {
			if (p.getKey().equals(newPair.getKey())) {
				prev = p.getValue();
				p.setValue(newPair.getValue());
				exist = true;
				break;
			}
		}

		if (!exist)
			list.add(newPair);

		size++;

		// If size is greater than threshold
		int maxSize = (int) (loadFactor * getArray().length);
		if (size >= maxSize)
			increase();

		return prev;
	}

	@Override
	public V get(K key) {
		int index = indexOf(key.hashCode());
		List<Pair<K, V>> list = getArray()[index];
		for (Pair<K, V> p : list.toList()) {
			if (p.getKey().equals(key))
				return p.getValue();
		}
		return null;
	}

	@Override
	public boolean contains(K key) {
		return (get(key) != null);
	}

	@Override
	public V remove(K key) {
		int index = indexOf(key.hashCode());
		List<Pair<K, V>> list = getArray()[index];
		for (Pair<K, V> pair : list.toList()) {
			if (pair.getKey().equals(key)) {
				list.remove(pair);
				size--;

				V value = pair.getValue();
				pair.setKey(null);
				pair.setValue(null);

				int loadFactored = (int) (size / loadFactor);
				int smallerSize = getSmallerSize(getArray().length);
				if (loadFactored < smallerSize && smallerSize > minimumSize)
					reduce();

				return value;
			}
		}
		return null;
	}

	@Override
	public void clear() {
		for (int i = 0; i < getArray().length; i++)
			getArray()[i].clear();
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	private void increase() {
		// Save old data
		List<Pair<K, V>>[] temp = this.getArray();

		// Calculate new size and assign
		int length = getLargerSize(getArray().length);
		// System.out.println("increase from "+array.length+" to "+length);
		initializeMap(length);

		// Re-hash old data
		for (List<Pair<K, V>> list : temp) {
			for (Pair<K, V> p : list.toList()) {
				this.put(p);
			}
		}
	}

	private void reduce() {
		// Save old data
		List<Pair<K, V>>[] temp = this.getArray();

		// Calculate new size and check minimum
		int length = getSmallerSize(getArray().length);
		// System.out.println("reduce from "+array.length+" to "+length);
		initializeMap(length);

		// Re-hash old data
		for (List<Pair<K, V>> list : temp) {
			for (Pair<K, V> p : list.toList()) {
				this.put(p);
			}
		}
	}

	private static final int getLargerSize(int input) {
		return input << 1;
	}

	private static final int getSmallerSize(int input) {
		return input >> 1 >> 1;
	}

	private void initializeMap(int length) {
		this.setArray(new ArrayList[length]);
		for (int i = 0; i < getArray().length; i++)
			this.getArray()[i] = new ArrayList<Pair<K, V>>(initialListSize);
		this.size = 0;
	}

	private int indexOf(int h) {
		return h & (getArray().length - 1);
	}

	@Override
	public java.util.Map<K, V> toMap() {
		return (new JavaCompatibleChainingHashMap<K, V>(this));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int key = 0; key < getArray().length; key++) {
			ArrayList<Pair<K, V>> list = getArray()[key];
			for (int item = 0; item < list.size(); item++) {
				Pair<K, V> p = list.get(item);
				V value = p.getValue();
				if (value != null)
					builder.append(key).append("=").append(value).append(", ");
			}
		}
		return builder.toString();
	}

	public ArrayList<Pair<K, V>>[] getArray() {
		return array;
	}

	public void setArray(ArrayList<Pair<K, V>>[] array) {
		this.array = array;
	}

}
