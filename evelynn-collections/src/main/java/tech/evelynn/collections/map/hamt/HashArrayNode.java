package tech.evelynn.collections.map.hamt;

public class HashArrayNode extends HashArrayMappedNode{

	 private byte height = 0;
     private int bitmap = 0;
     private HashArrayMappedNode[] children = new HashArrayMappedNode[2];

     public HashArrayNode(HashArrayNode parent, int key, byte height) {
         this.parent = parent;
         this.key = key;
         this.setHeight(height);
     }
     
     private void set(int position) {
         bitmap |= (1 << position);
     }
     
     private void unset(int position) {
         bitmap &= ~(1 << position);
     }

     private boolean isSet(int position) {
         return ((bitmap &(1 << position)) >>> position)==1;
     }

     private int getIndex(int pos){
         int position = pos;
         position = (1 << position)-1;
         return Integer.bitCount(bitmap & position);
     }

     private static int calcNewLength(int size) {
         int newSize = size;
         if (newSize==HashArrayMappedTrie.MAX_BITS) return newSize;

         newSize = (newSize + (newSize>>1));
         if (newSize>HashArrayMappedTrie.MAX_BITS) newSize = HashArrayMappedTrie.MAX_BITS;
         return newSize;
     }
     
     public void addChild(int position, HashArrayMappedNode child) {
         boolean overwrite = false;
         if (isSet(position)) overwrite = true;

         set(position);
         int idx = getIndex(position);
         if (!overwrite) {
             int len = calcNewLength(getNumberOfChildren());
             // Array size changed, copy the array to the new array
             if (len>children.length) {
            	 HashArrayMappedNode[] temp = new HashArrayMappedNode[len];
                 System.arraycopy(children, 0, temp, 0, children.length);
                 children = temp;
             }
             // Move existing elements up to make room
             if (children[idx]!=null)
                 System.arraycopy(children, idx, children, idx+1, (children.length-(idx+1)));
         }
         children[idx] = child;
     }
     
     public void removeChild(int position) {
         if (!isSet(position)) return;

         unset(position);
         int lastIdx = getNumberOfChildren();
         int idx = getIndex(position);
         // Shift the entire array down one spot starting at idx
         System.arraycopy(children, idx+1, children, idx, (children.length-(idx+1)));
         children[lastIdx] = null;
     }
     
     public HashArrayMappedNode getChild(int position) {
         if (!isSet(position)) return null;

         int idx = getIndex(position);
         return children[idx];
     }

     public int getNumberOfChildren() {
         return Integer.bitCount(bitmap);
     }
     
     @Override
     public String toString() {
         StringBuilder builder = new StringBuilder();
         builder.append("height=").append(getHeight()).append(" key=").append(HashArrayMappedTrie.toBinaryString(key)).append("\n");
         for (int i=0; i<HashArrayMappedTrie.MAX_BITS; i++) {
        	 HashArrayMappedNode c = getChild(i);
             if (c!=null) builder.append(c.toString()).append(", ");
         }
         return builder.toString();
     }

	public byte getHeight() {
		return height;
	}

	public void setHeight(byte height) {
		this.height = height;
	}

     
     
     
}
