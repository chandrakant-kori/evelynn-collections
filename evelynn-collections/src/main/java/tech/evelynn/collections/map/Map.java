package tech.evelynn.collections.map;

public interface Map<K, V> {

	public V put(K key, V value);

	public V get(K key);

	public V remove(K key);

	public void clear();

	public boolean contains(K key);

	public int size();

	public java.util.Map<K, V> toMap();

}
