package tech.evelynn.collections.map.trie;

import java.util.Iterator;
import java.util.Map;

public class JavaCompatibleTrieMapIterator<K extends CharSequence, V> implements Iterator<Map.Entry<K, V>> {

	private TrieMap<K,V> map = null;
    private java.util.Iterator<java.util.Map.Entry<K, V>> iter = null;
    private java.util.Map.Entry<K, V> lastEntry = null;

    public JavaCompatibleTrieMapIterator(TrieMap<K,V> map, java.util.Iterator<java.util.Map.Entry<K, V>> iter) {
        this.map = map;
        this.iter = iter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext() {
        if (iter==null) return false;
        return iter.hasNext();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public java.util.Map.Entry<K, V> next() {
        if (iter==null) return null;

        lastEntry = iter.next();
        return lastEntry;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove() {
        if (iter==null || lastEntry==null) return;

        map.remove(lastEntry.getKey());
        iter.remove();
    }
	
}
