package tech.evelynn.collections.map.hamt;

import tech.evelynn.collections.map.Map;

@SuppressWarnings("unchecked")
public class HashArrayMappedTrie <K, V> implements Map<K,V>  {

	public static final byte MAX_BITS = 32;
    private static final byte BITS = 5;
    private static final byte MAX_DEPTH = MAX_BITS/BITS; // 6
    private static final int  MASK = (int)Math.pow(2, BITS)-1;

    private HashArrayMappedNode root = null;
    private int size = 0;
    
    private static final int getPosition(int height, int value) {
        return (value >>> height*BITS) & MASK;
    }

    private V put(HashArrayNode parent, HashArrayMappedNode node, byte height, int key, V value) {
        byte newHeight = height;
        if (node instanceof KeyValueNode) {
			KeyValueNode<V> kvNode = (KeyValueNode<V>) node;
            if (key==kvNode.key) {
                // Key already exists as key-value pair, replace value
                kvNode.setValue(value);
                return value;
            }

            // Key already exists but doesn't match current key
            KeyValueNode<V> oldParent = kvNode;
            int newParentPosition = getPosition(newHeight-1, key);
            int oldParentPosition = getPosition(newHeight, oldParent.key);
            int childPosition = getPosition(newHeight, key);
            HashArrayNode newParent = new HashArrayNode(parent, key, newHeight);
            newParent.parent = parent;

            if (parent==null) {
                // Only the root doesn't have a parent, so new root
                root = newParent;
            } else {
                // Add the child to the parent in it's parent's position
                parent.addChild(newParentPosition, newParent);
            }

            if (oldParentPosition != childPosition) {
                // The easy case, the two children have different positions in parent
                newParent.addChild(oldParentPosition, oldParent);
                oldParent.parent = newParent;
                newParent.addChild(childPosition, new KeyValueNode<V>(newParent, key, value));
                return null;
            }

            while (oldParentPosition == childPosition) {
                // Handle the case when the new children map to same position.
                newHeight++;
                if (newHeight>MAX_DEPTH) {
                    // We have found two keys which match exactly. I really don't know what to do.
                    throw new RuntimeException("Yikes! Found two keys which match exactly.");
                }
                newParentPosition = getPosition(newHeight-1, key);
                HashArrayNode newParent2 = new HashArrayNode(newParent, key, newHeight);
                newParent.addChild(newParentPosition, newParent2);

                oldParentPosition = getPosition(newHeight, oldParent.key);
                childPosition = getPosition(newHeight, key);
                if (oldParentPosition != childPosition) {
                    newParent2.addChild(oldParentPosition, oldParent);
                    oldParent.parent = newParent2;
                    newParent2.addChild(childPosition, new KeyValueNode<V>(newParent2, key, value));   
                } else {
                    newParent = newParent2;
                }
            }
            return null;
        } else if (node instanceof HashArrayNode) {
            HashArrayNode arrayRoot = (HashArrayNode) node;
            int position = getPosition(arrayRoot.getHeight(), key);
            HashArrayMappedNode child = arrayRoot.getChild(position);

            if (child==null) {
                // Found an empty slot in parent
                arrayRoot.addChild(position, new KeyValueNode<V>(arrayRoot, key, value));
                return null;
            }

            return put(arrayRoot, child, (byte)(newHeight+1), key, value);
        }
        return null;
    }
    
    @Override
    public V put(K key, V value) {
        int intKey = key.hashCode();
        V toReturn = null;
        if (root==null)
            root = new KeyValueNode<V>(null, intKey, value);
        else
            toReturn = put(null, root, (byte)0, intKey, value);
        if (toReturn==null) size++;
        return toReturn;
    }
    
   
    private HashArrayMappedNode find(HashArrayMappedNode node, int key) {
        if (node instanceof KeyValueNode) {
			KeyValueNode<V> kvNode = (KeyValueNode<V>) node;
            if (kvNode.key==key)
                return kvNode;
            return null;
        } else if (node instanceof HashArrayNode) {
            HashArrayNode arrayNode = (HashArrayNode)node;
            int position = getPosition(arrayNode.getHeight(), key);
            HashArrayMappedNode possibleNode = arrayNode.getChild(position);
            if (possibleNode==null) 
                return null;
            return find(possibleNode,key);
        }
        return null;
    }
    
    private HashArrayMappedNode find(int key) {
        if (root==null) return null;
        return find(root, key);
    }
    
    @Override
    public V get(K key) {
        HashArrayMappedNode node = find(key.hashCode());
        if (node==null) 
            return null;
        if (node instanceof KeyValueNode) 
            return ((KeyValueNode<V>)node).getValue();
        return null;
    }
    
    @Override
    public boolean contains(K key) {
        HashArrayMappedNode node = find(key.hashCode());
        return (node!=null);
    }

    @Override
    public V remove(K key) {
        HashArrayMappedNode node = find(key.hashCode());
        if (node==null) 
            return null;
        if (node instanceof HashArrayNode) 
            return null;

        KeyValueNode<V> kvNode = (KeyValueNode<V>)node;
        V value = kvNode.getValue();
        if (node.parent==null) {
            // If parent is null, removing the root
            root = null;
        } else {
            HashArrayNode parent = node.parent;
            // Remove child from parent
            int position = getPosition(parent.getHeight(), node.key);
            parent.removeChild(position);
            // Go back up the tree, pruning any array nodes which no longer have children.
            int numOfChildren = parent.getNumberOfChildren();
            while (numOfChildren==0) {
                node = parent;
                parent = node.parent;
                if (parent==null) {
                    // Reached root of the tree, need to stop
                    root = null;
                    break;
                }
                position = getPosition(parent.getHeight(), node.key);
                parent.removeChild(position);
                numOfChildren = parent.getNumberOfChildren();
            }
        }
        kvNode.key = 0;
        kvNode.setValue(null);
        size--;
        return value;
    }
    
    @Override
    public void clear() {
        root = null;
        size = 0;
    }
    
    @Override
    public int size() {
        return size;
    }
    
    public static final String toBinaryString(int value) {
        StringBuilder builder = new StringBuilder(Integer.toBinaryString(value));
        builder = builder.reverse();
        while (builder.length()<BITS) {
            builder.append('0');
        }
        return builder.toString();
    }

	@Override
	public java.util.Map<K, V> toMap() {
		// TODO Auto-generated method stub
		return null;
	}
    
}
