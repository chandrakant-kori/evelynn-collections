package tech.evelynn.collections.map;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Set;

import tech.evelynn.collections.list.List;

import java.util.Map;

@SuppressWarnings("unchecked")
public class JavaCompatibleChainingHashMap<K, V> extends AbstractMap<K, V> {

	private ChainingHashMap<K, V> map = null;

	
	protected JavaCompatibleChainingHashMap(ChainingHashMap<K, V> map) {
		this.map = map;
	}

	@Override
	public V put(K key, V value) {
		return map.put(key, value);
	}

	@Override
	public V remove(Object key) {
		return map.remove((K) key);
	}

	@Override
	public boolean containsKey(Object key) {
		return map.contains((K) key);
	}

	@Override
	public void clear() {
		map.clear();
	}

	@Override
	public int size() {
		return map.size();
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		Set<Map.Entry<K, V>> set = new HashSet<Map.Entry<K, V>>() {

			private static final long serialVersionUID = 1L;

			@Override
			public java.util.Iterator<Map.Entry<K, V>> iterator() {
				return (new JavaCompatibleHashMapIterator<K, V>(map, super.iterator()));
			}
		};
		for (List<Pair<K, V>> list : map.getArray()) {
			for (Pair<K, V> p : list.toList()) {
				JavaCompatibleMapEntry<K, V> entry = new JavaCompatibleMapEntry<K, V>(p.getKey(), p.getValue());
				set.add(entry);
			}
		}
		return set;
	}

}
