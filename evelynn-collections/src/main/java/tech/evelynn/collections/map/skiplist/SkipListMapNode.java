package tech.evelynn.collections.map.skiplist;

import tech.evelynn.collections.set.skiplist.SkipListNode;

public class SkipListMapNode<K extends Comparable<K>, V> extends SkipListNode<K> {

	protected V value = null;

    protected SkipListMapNode(int level, K key) {
        super(level, key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append("value = ").append(value).append("\n");
        return builder.toString();
    }

}
