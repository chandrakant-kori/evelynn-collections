package tech.evelynn.collections.map.trie;

import tech.evelynn.collections.map.Map;
import tech.evelynn.collections.trie.Trie;
import tech.evelynn.collections.trie.TrieNode;
import tech.evelynn.collections.trie.TrieNodeCreator;

@SuppressWarnings("unchecked")
public class TrieMap<K extends CharSequence, V> implements TrieNodeCreator, Map<K,V> {

	private Trie<K> trie = null;

    public TrieMap() {
        setTrie(new Trie<K>(this));
    }
    
    @Override
    public V put(K key, V value) {
        V prev = null;
        TrieNode node = getTrie().addSequence(key);

        if (node!=null && node instanceof TrieMapNode) {
            TrieMapNode<V> trieMapNode = (TrieMapNode<V>) node;
            if (trieMapNode.value!=null) prev = trieMapNode.value;
            trieMapNode.value = value;
        }

        return prev;
    }
    
    @Override
    public V get(K key) {
        TrieNode node = getTrie().getNode(key);
        if (node!=null && node instanceof TrieMapNode) {
            TrieMapNode<V> mapNode = (TrieMapNode<V>) node;
            return mapNode.value;
        }
        return null;
    }
    
    @Override
    public boolean contains(K key) {
        return getTrie().contains(key);
    }

    
    @Override
    public V remove(K key) {
        TrieNode node = getTrie().getNode(key);
        V value = null;
        if (node!=null) {
            if (node instanceof TrieMapNode) {
                TrieMapNode<V> tmn = (TrieMapNode<V>)node;
                value = tmn.value;
                tmn.value = null;
            }
            getTrie().remove(node);
        }
        return value;
    }
    
    @Override
    public void clear() {
        getTrie().clear();
    }
    
    @Override
    public int size() {
        return getTrie().size();
    }
    
    @Override
    public java.util.Map<K,V> toMap() {
        return (new JavaCompatibleTrieMap<K,V>(this));
    }
    
    @Override
    public String toString() {
        return TrieMapPrinter.getString(this);
    }

    @Override
    public TrieNode createNewNode(TrieNode parent, Character character, boolean type) {
        return (new TrieMapNode<V>(parent, character, type));
    }

	public Trie<K> getTrie() {
		return trie;
	}

	public void setTrie(Trie<K> trie) {
		this.trie = trie;
	}
 
}
