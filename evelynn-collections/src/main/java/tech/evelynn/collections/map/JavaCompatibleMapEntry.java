package tech.evelynn.collections.map;

public class JavaCompatibleMapEntry<K, V> extends java.util.AbstractMap.SimpleEntry<K, V> {

	private static final long serialVersionUID = 3282082818647198608L;

	public JavaCompatibleMapEntry(K key, V value) {
		super(key, value);
	}
}