package tech.evelynn.collections.map.tree;

import tech.evelynn.collections.list.ArrayList;

@SuppressWarnings("unchecked")
public class TreeMapPrinter {

	public static <K extends Comparable<K>, V> String getString(TreeMap<K, V> map) {
        if (map.getTree().getRoot() == null) return "Tree has no nodes.";
        return getString((TreeMapNode<K, V>) map.getTree().getRoot(), "", true);
    }

    private static <K extends Comparable<K>, V> String getString(TreeMapNode<K, V> node, String prefix, boolean isTail) {
        StringBuilder builder = new StringBuilder();

        builder.append(prefix + (isTail ? "└── " : "├── ") + ((node.getId() != null) ? (node.getId() + " = " + node.getValue()) : node.getId()) + "\n");
        ArrayList<TreeMapNode<K, V>> children = null;
        if (node.getLesser() != null || node.getGreater() != null) {
            children = new ArrayList<TreeMapNode<K, V>>(2);
            if (node.getLesser() != null) children.add((TreeMapNode<K, V>) node.getLesser());
            if (node.getGreater() != null) children.add((TreeMapNode<K, V>) node.getGreater());
        }
        if (children != null) {
            for (int i = 0; i < children.size() - 1; i++) {
                builder.append(getString(children.get(i), prefix + (isTail ? "    " : "│   "), false));
            }
            if (children.size() >= 1) {
                builder.append(getString(children.get(children.size() - 1), prefix + (isTail ? "    " : "│   "), true));
            }
        }

        return builder.toString();
    }
}
