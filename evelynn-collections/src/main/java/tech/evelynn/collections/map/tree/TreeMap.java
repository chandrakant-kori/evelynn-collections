package tech.evelynn.collections.map.tree;

import tech.evelynn.collections.map.Map;
import tech.evelynn.collections.tree.avl.AVLTree;
import tech.evelynn.collections.tree.bst.BSTNode;
import tech.evelynn.collections.tree.bst.BSTNodeCreator;

@SuppressWarnings("unchecked")
public class TreeMap<K extends Comparable<K>, V> implements Map<K, V> {

	private final BSTNodeCreator<K> creator = new BSTNodeCreator<K>() {

		@Override
		public BSTNode<K> createNewNode(BSTNode<K> parent, K id) {
			return (new TreeMapNode<K, V>(parent, id, null));
		}
	};

	private AVLTree<K> tree = null;

	public TreeMap() {

		setTree(new AVLTree<K>(creator));
	}

	public TreeMap(BSTNodeCreator<K> creator) {
		setTree(new AVLTree<K>(creator));
	}

	@Override
	public V put(K key, V value) {
		V prev = null;
		BSTNode<K> node = getTree().addValue(key);

		TreeMapNode<K, V> treeMapNode = (TreeMapNode<K, V>) node;
		if (treeMapNode.getValue() != null)
			prev = treeMapNode.getValue();
		treeMapNode.setValue(value);

		return prev;
	}

	@Override
	public V get(K key) {
		BSTNode<K> node = getTree().getNode(key);
		TreeMapNode<K, V> mapNode = (TreeMapNode<K, V>) node;
		return mapNode.getValue();
	}

	@Override
	public boolean contains(K key) {
		return getTree().contains(key);
	}

	@Override
	public V remove(K key) {
		BSTNode<K> node = getTree().removeValue(key);
		TreeMapNode<K, V> treeMapNode = (TreeMapNode<K, V>) node;
		V value = null;
		if (treeMapNode != null) {
			value = treeMapNode.getValue();
			treeMapNode.setId(null);
			treeMapNode.setValue(null);
		}
		return value;
	}
	
	@Override
    public void clear() {
        getTree().clear();
    }
	
	@Override
    public int size() {
        return getTree().size();
    }
	
	@Override
    public java.util.Map<K,V> toMap() {
        return (new JavaCompatibleTreeMap<K,V>(this));
    }

	@Override
    public String toString() {
        return TreeMapPrinter.getString(this);
    }

	public AVLTree<K> getTree() {
		return tree;
	}

	public void setTree(AVLTree<K> tree) {
		this.tree = tree;
	}

}
