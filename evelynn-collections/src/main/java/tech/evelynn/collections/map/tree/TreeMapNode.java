package tech.evelynn.collections.map.tree;

import tech.evelynn.collections.tree.avl.AVLNode;
import tech.evelynn.collections.tree.bst.BSTNode;

public class TreeMapNode<K extends Comparable<K>, V> extends AVLNode<K> {

	private V value = null;

	public TreeMapNode(BSTNode<K> parent, K key, V value) { // Redblack Tree was used here
		super(parent, key);
		this.setValue(value);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append("value = ").append(getValue()).append("\n");
		return builder.toString();
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}
}
