package tech.evelynn.collections.map.trie;

import java.util.AbstractMap;

public class JavaCompatibleTrieMapEntry<K extends CharSequence, V> extends AbstractMap.SimpleEntry<K, V> {

	private static final long serialVersionUID = -4427602384853830561L;

	public JavaCompatibleTrieMapEntry(K key, V value) {
		super(key, value);
	}

}
