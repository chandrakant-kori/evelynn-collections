package tech.evelynn.collections.map;

import java.util.Map;

@SuppressWarnings("unchecked")
public class LinearProbingHashMap<K, V> extends HashMap<K, V> {

	private int hashingKey = -1;
	private float loadFactor = 0.75f;
	private int minimumSize = 1024;
	private Pair<K, V>[] array = null;
	private int size = 0;

	
	public LinearProbingHashMap(int size) {
		initializeMap(size);
	}

	public LinearProbingHashMap() {
		initializeMap(minimumSize);
	}

	private void initializeMap(int current) {
		int length = getLargerSize(current);
		setArray(new Pair[length]);
		size = 0;
		hashingKey = length;
	}

	@Override
	public V put(K key, V value) {
		return put(new Pair<K, V>(key, value));
	}

	private V put(Pair<K, V> newPair) {
		V prev = null;
		int index = indexOf(newPair.getKey());

		// Check initial position
		Pair<K, V> pair = getArray()[index];
		if (pair == null) {
			getArray()[index] = newPair;
			size++;

			// If size is greater than threshold
			int maxSize = (int) (loadFactor * getArray().length);
			if (size >= maxSize)
				increase();

			return prev;
		}

		if (pair.getKey().equals(newPair.getKey())) {
			prev = pair.getValue();
			pair.setValue(newPair.getValue());
			return prev;
		}

		// Probing until we get back to the starting index
		int start = getNextIndex(index);
		while (start != index) {
			pair = getArray()[start];
			if (pair == null) {
				getArray()[start] = newPair;
				size++;

				// If size is greater than threshold
				int maxSize = (int) (loadFactor * getArray().length);
				if (size >= maxSize)
					increase();

				return prev;
			}

			if (pair.getKey().equals(newPair.getKey())) {
				prev = pair.getValue();
				pair.setValue(newPair.getValue());
				return prev;
			}

			start = getNextIndex(start);
		}
		// We should never get here.
		return null;
	}

	@Override
	public V get(K key) {
		int index = indexOf(key);
		Pair<K, V> pair = getArray()[index];

		// Check initial position
		if (pair == null)
			return null;
		if (pair.getKey().equals(key))
			return pair.getValue();

		// Probing until we get back to the starting index
		int start = getNextIndex(index);
		while (start != index) {
			pair = getArray()[start];

			if (pair == null)
				return null;
			if (pair.getKey().equals(key))
				return pair.getValue();

			start = getNextIndex(start);
		}
		// If we get here, probing failed.
		return null;
	}

	@Override
	public boolean contains(K key) {
		return (get(key) != null);
	}

	@Override
	public V remove(K key) {
		int index = indexOf(key);
		Pair<K, V> prev = null;

		// Check initial position
		Pair<K, V> pair = getArray()[index];
		if (pair != null && pair.getKey().equals(key)) {
			prev = getArray()[index];
			getArray()[index] = null;
			size--;

			int loadFactored = (int) (size / loadFactor);
			int smallerSize = getSmallerSize(getArray().length);
			if (loadFactored < smallerSize && smallerSize > minimumSize)
				reduce();

			return prev.getValue();
		}

		// Probing until we get back to the starting index
		int start = getNextIndex(index);
		while (start != index) {
			pair = getArray()[start];
			if (pair != null && pair.getKey().equals(key)) {
				prev = getArray()[start];
				getArray()[start] = null;
				size--;

				int loadFactored = (int) (size / loadFactor);
				int smallerSize = getSmallerSize(getArray().length);
				if (loadFactored < smallerSize && smallerSize > minimumSize)
					reduce();

				return prev.getValue();
			}
			start = getNextIndex(start);
		}
		// If we get here, probing failed.
		return null;
	}

	@Override
	public void clear() {
		for (int i = 0; i < getArray().length; i++)
			getArray()[i] = null;
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	private void increase() {
		// Save old data
		Pair<K, V>[] temp = this.getArray();

		// Calculate new size and assign
		int length = getLargerSize(getArray().length);
		// System.out.println("increase from "+array.length+" to "+length);
		initializeMap(length);

		// Re-hash old data
		for (Pair<K, V> p : temp) {
			if (p != null)
				this.put(p);
		}
	}

	private void reduce() {
		// Save old data
		Pair<K, V>[] temp = this.getArray();

		// Calculate new size and check minimum
		int length = getSmallerSize(getArray().length);
		// System.out.println("reduce from "+array.length+" to "+length);
		initializeMap(length);

		// Re-hash old data
		for (Pair<K, V> p : temp) {
			if (p != null)
				this.put(p);
		}
	}

	private static final int getLargerSize(int input) {
		return input << 1;
	}

	private static final int getSmallerSize(int input) {
		return input >> 1 >> 1;
	}

	private int getNextIndex(int input) {
		int i = input + 1;
		if (i >= getArray().length)
			i = 0;
		return i;
	}

	private int indexOf(K key) {
		int k = Math.abs(key.hashCode()) % hashingKey;
		if (k >= getArray().length)
			k = k - ((k / getArray().length) * getArray().length);
		return k;
	}

	@Override
	public Map<K, V> toMap() {
		return (new JavaCompatibleLinearProbHashMap<K, V>(this));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int key = 0; key < getArray().length; key++) {
			Pair<K, V> p = getArray()[key];
			if (p == null)
				continue;
			V value = p.getValue();
			if (value != null)
				builder.append(key).append("=").append(value).append(", ");
		}
		return builder.toString();
	}

	public Pair<K, V>[] getArray() {
		return array;
	}

	public void setArray(Pair<K, V>[] array) {
		this.array = array;
	}
}
