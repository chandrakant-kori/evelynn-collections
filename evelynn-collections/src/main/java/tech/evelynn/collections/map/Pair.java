package tech.evelynn.collections.map;

@SuppressWarnings("unchecked")
public final class Pair<K, V> {

	private K key = null;
	private V value = null;

	public Pair(K key, V value) {
		this.setKey(key);
		this.setValue(value);
	}

	@Override
	public int hashCode() {
		return 31 * (this.getKey().hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof Pair))
			return false;

		Pair<K, V> pair = (Pair<K, V>) obj;
		return getKey().equals(pair.getKey());
	}

	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}
}