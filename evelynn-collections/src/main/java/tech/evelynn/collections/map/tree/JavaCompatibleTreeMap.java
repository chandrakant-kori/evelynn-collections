package tech.evelynn.collections.map.tree;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import tech.evelynn.collections.map.JavaCompatibleMapEntry;
import tech.evelynn.collections.tree.bst.BSTNode;

@SuppressWarnings("unchecked")
public class JavaCompatibleTreeMap<K extends Comparable<K>, V> extends AbstractMap<K, V> {

	private TreeMap<K,V> map = null;
	
	public JavaCompatibleTreeMap(TreeMap<K,V> map) {
        this.map = map;
    }
	
	@Override
    public V put(K key, V value) {
        return map.put(key, value);
    }
	
	@Override
    public V remove(Object key) {
        return map.remove((K)key);
    }
	
	@Override
    public void clear() {
        map.clear();
    }

	@Override
    public boolean containsKey(Object key) {
        return map.contains((K)key);
    }

	@Override
    public int size() {
        return map.size();
    }
	
	@Override
    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = new HashSet<Map.Entry<K, V>>() {

            private static final long serialVersionUID = 1L;

            /**
             * {@inheritDoc}
             */
            @Override
            public Iterator<Map.Entry<K, V>> iterator() {
                return (new JavaCompatibleTreeMapIterator<K,V>(map,super.iterator()));
            }
        };
        if (map.getTree()!=null && map.getTree().getRoot()!=null) {
            BSTNode<K> n = map.getTree().getRoot();
            levelOrder(n,set);
        }
        return set;
    }
	
	
	private void levelOrder(BSTNode<K> node, java.util.Set<java.util.Map.Entry<K, V>> set) {
        TreeMapNode<K,V> tmn = null;
        if (node instanceof TreeMapNode) {
            tmn = (TreeMapNode<K,V>)node;
            java.util.Map.Entry<K, V> entry = new JavaCompatibleMapEntry<K, V>(tmn.getId(), tmn.getValue());
            set.add(entry);
        }

        if (node.getLesser()!=null) levelOrder(node.getLesser(),set);
        if (node.getGreater()!=null) levelOrder(node.getGreater(),set);
    }
	
}

