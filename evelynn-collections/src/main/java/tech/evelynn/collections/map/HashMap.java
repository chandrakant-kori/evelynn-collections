package tech.evelynn.collections.map;

public class HashMap<K, V> implements Map<K, V> {

	public static enum Type {
		CHAINING, PROBING
	}

	public HashMap() {
	}

	private HashMap<K, V> delegateMap = null;

	public HashMap(Type type, int size) {
		if (type == Type.CHAINING) {
			delegateMap = new ChainingHashMap<K, V>(size);
		} else if (type == Type.PROBING) {
			delegateMap = new LinearProbingHashMap<K, V>(size);
		}
	}

	public HashMap(Type type) {
		if (type == Type.CHAINING) {
			delegateMap = new ChainingHashMap<K, V>();
		} else if (type == Type.PROBING) {
			delegateMap = new LinearProbingHashMap<K, V>();
		}
	}

	@Override
	public V put(K key, V value) {
		return delegateMap.put(key, value);
	}

	@Override
	public V get(K key) {
		return delegateMap.get(key);
	}

	@Override
	public boolean contains(K key) {
		return (get(key) != null);
	}

	@Override
	public V remove(K key) {
		return delegateMap.remove(key);
	}

	@Override
	public void clear() {
		delegateMap.clear();
	}

	@Override
	public int size() {
		return delegateMap.size();
	}

	@Override
	public java.util.Map<K, V> toMap() {
		return delegateMap.toMap();
	}

	@Override
	public String toString() {
		return delegateMap.toString();
	}

}
