package tech.evelynn.collections.map;

import java.util.Iterator;
import java.util.Map;

public class JavaCompatibleHashMapIterator<K, V> implements Iterator<Map.Entry<K, V>> {

	private HashMap<K, V> map = null;
	private Iterator<Map.Entry<K, V>> iter = null;
	private Map.Entry<K, V> lastEntry = null;

	public JavaCompatibleHashMapIterator(HashMap<K, V> map, java.util.Iterator<Map.Entry<K, V>> iter) {
		this.map = map;
		this.iter = iter;
	}

	@Override
	public boolean hasNext() {
		if (iter == null)
			return false;
		return iter.hasNext();
	}

	@Override
	public Map.Entry<K, V> next() {
		if (iter == null)
			return null;

		lastEntry = iter.next();
		return lastEntry;
	}

	@Override
	public void remove() {
		if (iter == null || lastEntry == null)
			return;

		map.remove(lastEntry.getKey());
		iter.remove();
	}
}
