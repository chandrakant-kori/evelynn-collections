package tech.evelynn.collections.map.hamt;

public class KeyValueNode<V> extends HashArrayMappedNode {

	private V value;

	public KeyValueNode(HashArrayNode parent, int key, V value) {
		this.parent = parent;
		this.key = key;
		this.setValue(value);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("key=").append(HashArrayMappedTrie.toBinaryString(key)).append(" value=").append(getValue());
		return builder.toString();
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

}
