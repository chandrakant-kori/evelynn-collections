package tech.evelynn.collections.map.skiplist;

import java.util.AbstractMap;

import tech.evelynn.collections.map.JavaCompatibleMapEntry;
import tech.evelynn.collections.set.skiplist.SkipListNode;

@SuppressWarnings("unchecked")
public class JavaCompatibleSkipListMap<K extends Comparable<K>, V> extends AbstractMap<K, V> {

	 private SkipListMap<K,V> map = null;

     public JavaCompatibleSkipListMap(SkipListMap<K,V> map) {
         this.map = map;
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public V put(K key, V value) {
         return map.put(key, value);
     }

     /**
      * {@inheritDoc}
      */
	@Override
     public V remove(Object key) {
         return map.remove((K)key);
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public void clear() {
         map.clear();
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public boolean containsKey(Object key) {
         return map.contains((K)key);
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public int size() {
         return map.size();
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public java.util.Set<java.util.Map.Entry<K, V>> entrySet() {
         java.util.Set<java.util.Map.Entry<K, V>> set = new java.util.HashSet<java.util.Map.Entry<K, V>>() {

             private static final long serialVersionUID = 1L;

             /**
              * {@inheritDoc}
              */
             @Override
             public java.util.Iterator<java.util.Map.Entry<K, V>> iterator() {
                 return (new JavaCompatibleSkipListIterator(map,super.iterator()));
             }
         };
         if (map.getList()!=null && map.getList().getHead()!=null) {
             SkipListNode<K> n = map.getList().getHead();
             while (n!=null) {
                 if (!(n instanceof SkipListMapNode)) continue;

                 SkipListMapNode<K,V> node = (SkipListMapNode<K,V>)n;
                 set.add(new JavaCompatibleMapEntry<K,V>(node.getData(),node.value));

                 n = node.getNext(0);    
             }
         }
         return set;
     }
	
}
