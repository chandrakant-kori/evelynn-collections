package tech.evelynn.collections.map.trie;

import tech.evelynn.collections.trie.TrieNode;

public class TrieMapNode<V> extends TrieNode {

	protected V value = null;

	protected TrieMapNode(TrieNode parent, Character character, boolean type) {
		super(parent, character, type);
	}

	protected TrieMapNode(TrieNode parent, Character character, boolean type, V value) {
		super(parent, character, type);
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (value != null)
			builder.append("key=").append(getCharacter()).append(" value=").append(value).append("\n");
		for (int i = 0; i < getChildrenSize(); i++) {
			TrieNode c = getChild(i);
			builder.append(c.toString());
		}
		return builder.toString();
	}

}
