package tech.evelynn.collections.trie.radix;

import tech.evelynn.collections.trie.patricia.PatriciaTrieConstants;
import tech.evelynn.collections.trie.patricia.PatriciaTrieNode;

@SuppressWarnings("unchecked")
public class RadixTreePrinter<K extends CharSequence, V> {
	
	public static <K extends CharSequence, V> String getString(RadixTrie<K, V> map) {
        return getString(map.getTrie().getRoot(), "", null, true);
    }

	protected static <K extends CharSequence, V> String getString(PatriciaTrieNode node, String prefix, String previousString, boolean isTail) {
        StringBuilder builder = new StringBuilder();
        String string = null;
        if (node.getString() != null) {
            String temp = String.valueOf(node.getString());
            if (previousString != null)
                string = previousString + temp;
            else
                string = temp;
        }
        if (node instanceof RadixNode) {
            RadixNode<K,V> radix = (RadixNode<K,V>) node;
            builder.append(prefix
                            + (isTail ? "└── " : "├── ")
                            + ((radix.getString() != null) ? 
                                "(" + String.valueOf(radix.getString()) + ") " 
                                + "[" + ((node.isType() == PatriciaTrieConstants.WHITE) ? "WHITE" : "BLACK") + "] " 
                                + string
                                + ((radix.value != null) ? " = " + radix.value : "") 
                            : 
                                "[" + ((node.isType() == PatriciaTrieConstants.WHITE) ? "WHITE" : "BLACK") + "]") + "\n");
        }
        if (node.getChildrenSize() > 0) {
            for (int i = 0; i < node.getChildrenSize() - 1; i++) {
                builder.append(getString(node.getChild(i), prefix + (isTail ? "    " : "│   "), string, false));
            }
            if (node.getChildrenSize() >= 1) {
                builder.append(getString(node.getChild(node.getChildrenSize() - 1), prefix + (isTail ? "    " : "│   "), string, true));
            }
        }
		return builder.toString();
	}
}
