package tech.evelynn.collections.trie.suffix;

import java.util.Set;
import java.util.TreeSet;

import tech.evelynn.collections.tree.suffix.ISuffixTree;
import tech.evelynn.collections.trie.Trie;
import tech.evelynn.collections.trie.TrieNode;
import tech.evelynn.collections.trie.TriePrinter;

@SuppressWarnings("unchecked")
public class SuffixTrie<C extends CharSequence> implements ISuffixTree<C> {

	private Trie<C> tree;

	public SuffixTrie(C sequence) {
		tree = new Trie<C>();
		int length = sequence.length();
		for (int i = 0; i < length; i++) {
			CharSequence seq = sequence.subSequence(i, length);
			tree.add((C) seq);
		}
	}

	public boolean add(C sequence) {
		int length = sequence.length();
		for (int i = 0; i < length; i++) {
			CharSequence seq = sequence.subSequence(i, length);
			tree.add((C) seq);
		}
		return true;
	}

	@Override
	public boolean doesSubStringExist(C sequence) {
		char[] chars = sequence.toString().toCharArray();
		int length = chars.length;
		TrieNode current = tree.getRoot();
		for (int i = 0; i < length; i++) {
			int idx = current.childIndex(chars[i]);
			if (idx < 0)
				return false;
			current = current.getChild(idx);
		}
		return true;
	}

	@Override
	public Set<String> getSuffixes() {
		return this.getSuffixes(tree.getRoot());
	}

	private Set<String> getSuffixes(TrieNode node) {
		StringBuilder builder = new StringBuilder();
		if (node.getCharacter() != TrieNode.SENTINAL)
			builder.append(node.getCharacter());
		Set<String> set = new TreeSet<String>();
		if (node.isWord()) {
			set.add(builder.toString());
		}
		for (int i = 0; i < node.getChildrenSize(); i++) {
			TrieNode c = node.getChild(i);
			set.addAll(getSuffixes(c, builder.toString()));
		}
		return set;
	}

	private Set<String> getSuffixes(TrieNode node, String prefix) {
		StringBuilder builder = new StringBuilder(prefix);
		if (node.getCharacter() != TrieNode.SENTINAL)
			builder.append(node.getCharacter());
		Set<String> set = new TreeSet<String>();
		if (node.isWord()) {
			set.add(builder.toString());
		}
		for (int i = 0; i < node.getChildrenSize(); i++) {
			TrieNode c = node.getChild(i);
			set.addAll(getSuffixes(c, builder.toString()));
		}
		return set;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return TriePrinter.getString(tree);
	}
}
