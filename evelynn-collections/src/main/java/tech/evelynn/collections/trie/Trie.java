package tech.evelynn.collections.trie;

import tech.evelynn.collections.tree.Tree;
import tech.evelynn.collections.tree.bst.BSTNodeCreator;

@SuppressWarnings("unchecked")
public class Trie<C extends CharSequence> implements Tree<C> {

	protected TrieNodeCreator creator;
	private TrieNode root;

	private int size = 0;

	public Trie() {
		this.creator = new TrieNodeCreator() {
			@Override
			public TrieNode createNewNode(TrieNode parent, Character character, boolean isWord) {
				return (new TrieNode(parent, character, isWord));
			}
		};
	}

	public Trie(TrieNodeCreator creator) {
		this.creator = creator;
	}

	@Override
	public boolean add(C seq) {
		return (this.addSequence(seq) != null);
	}

	public TrieNode addSequence(C seq) {
		if (getRoot() == null)
			setRoot(this.creator.createNewNode(null, TrieNode.SENTINAL, false));

		int length = (seq.length() - 1);
		TrieNode prev = getRoot();
		// For each Character in the input, we'll either go to an already define
		// child or create a child if one does not exist
		for (int i = 0; i < length; i++) {
			TrieNode n = null;
			Character c = seq.charAt(i);
			int index = prev.childIndex(c);
			// If 'prev' has a child which starts with Character c
			if (index >= 0) {
				// Go to the child
				n = prev.getChild(index);
			} else {
				// Create a new child for the character
				n = this.creator.createNewNode(prev, c, false);
				prev.addChild(n);
			}
			prev = n;
		}

		// Deal with the first character of the input string not found in the
		// trie
		TrieNode n = null;
		Character c = seq.charAt(length);
		int index = prev.childIndex(c);
		// If 'prev' already contains a child with the last Character
		if (index >= 0) {
			n = prev.getChild(index);
			// If the node doesn't represent a string already
			if (n.isWord() == false) {
				// Set the string to equal the full input string
				n.setCharacter(c);
				n.setWord(true);
				setSize(getSize() + 1);
				return n;
			}
			// String already exists in Trie
			return null;
		}
		// Create a new node for the input string
		n = this.creator.createNewNode(prev, c, true);
		prev.addChild(n);
		setSize(getSize() + 1);
		return n;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(C seq) {
		TrieNode n = this.getNode(seq);
		if (n == null || !n.isWord())
			return false;

		// If the node found in the trie does not have it's string
		// field defined then input string was not found
		return n.isWord();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public C remove(C sequence) {
		if (getRoot() == null)
			return null;

		// Find the key in the Trie
		TrieNode node = getNode(sequence);
		if (node == null)
			return null;

		return remove(node);
	}

	public C remove(TrieNode node) {
		TrieNode previous = node.parent;
		if (node.childrenSize > 0) {
			// The node which contains the input string and has children, just
			// NULL out the string
			node.setWord(false);
		} else {
			// The node which contains the input string does NOT have children
			int index = previous.childIndex(node.getCharacter());
			// Remove node from previous node
			previous.removeChild(index);
			// Go back up the trie removing nodes until you find a node which
			// represents a string
			while (previous != null && previous.isWord() == false && previous.childrenSize == 0) {
				if (previous.parent != null) {
					int idx = previous.parent.childIndex(previous.getCharacter());
					if (idx >= 0)
						previous.parent.removeChild(idx);
				}
				previous = previous.parent;
			}
		}

		setSize(getSize() - 1);

		return (C) (String.valueOf(node.getCharacter()));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		setRoot(null);
		setSize(0);
	}

	/**
	 * Get node which represents the sequence in the trie.
	 * 
	 * @param seq to find a node for.
	 * @return Node which represents the sequence or NULL if not found.
	 */
	public TrieNode getNode(C seq) {
		if (getRoot() == null)
			return null;

		// Find the string in the trie
		TrieNode n = getRoot();
		int length = (seq.length() - 1);
		for (int i = 0; i <= length; i++) {
			char c = seq.charAt(i);
			int index = n.childIndex(c);
			if (index >= 0) {
				n = n.getChild(index);
			} else {
				// string does not exist in trie
				return null;
			}
		}

		return n;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return getSize();
	}

	@Override
	public java.util.Collection<C> toCollection() {
		return (new JavaCompatibleTrie<C>(this));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return TriePrinter.getString(this);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public TrieNode getRoot() {
		return root;
	}

	public void setRoot(TrieNode root) {
		this.root = root;
	}
}
