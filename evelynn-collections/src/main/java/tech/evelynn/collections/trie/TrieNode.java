package tech.evelynn.collections.trie;

import java.util.Arrays;

public class TrieNode {

	private static final int MINIMUM_SIZE = 2;

	public static final char SENTINAL = '\0';

	protected TrieNode[] children = new TrieNode[MINIMUM_SIZE];
	protected int childrenSize = 0;
	protected TrieNode parent = null;
	private boolean isWord = false; // Signifies this node represents a word
	private char character = SENTINAL; // First character that is different the parent's string

	protected TrieNode(TrieNode parent, Character character, boolean isWord) {
		this.parent = parent;
		this.setCharacter(character);
		this.setWord(isWord);
	}

	protected void addChild(TrieNode node) {
		int growSize = children.length;
		if (childrenSize >= children.length) {
			children = Arrays.copyOf(children, (growSize + (growSize >> 1)));
		}
		children[childrenSize++] = node;
	}

	protected boolean removeChild(int index) {
		if (index >= childrenSize)
			return false;

		children[index] = null;
		childrenSize--;

		// Shift down the array
		System.arraycopy(children, index + 1, children, index, childrenSize - index);

		int shrinkSize = childrenSize;
		if (childrenSize >= MINIMUM_SIZE && childrenSize < (shrinkSize + (shrinkSize << 1))) {
			System.arraycopy(children, 0, children, 0, childrenSize);
		}

		return true;
	}

	public int childIndex(Character parentChar) {
		for (int i = 0; i < childrenSize; i++) {
			TrieNode childChar = children[i];
			if (parentChar.equals(childChar.getCharacter()))
				return i;
		}
		return Integer.MIN_VALUE;
	}

	public TrieNode getChild(int index) {
		if (index >= childrenSize)
			return null;
		return children[index];
	}

	public int getChildrenSize() {
		return childrenSize;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (isWord() == true)
			builder.append("Node=").append(isWord()).append("\n");
		for (int i = 0; i < childrenSize; i++) {
			TrieNode c = children[i];
			builder.append(c.toString());
		}
		return builder.toString();
	}

	public char getCharacter() {
		return character;
	}

	public void setCharacter(char character) {
		this.character = character;
	}

	public boolean isWord() {
		return isWord;
	}

	public void setWord(boolean isWord) {
		this.isWord = isWord;
	}

}
