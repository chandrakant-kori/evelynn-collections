package tech.evelynn.collections.trie.patricia;

import java.util.Iterator;
import java.util.Map;

@SuppressWarnings("unchecked")
public class PatriciaTrieIterator<C extends CharSequence> implements Iterator<C> {

	private PatriciaTrie<C> trie = null;
    private PatriciaTrieNode lastNode = null;
    private Iterator<Map.Entry<PatriciaTrieNode, String>> iterator = null;
    
    protected PatriciaTrieIterator(PatriciaTrie<C> trie) {
        this.trie = trie;
        java.util.Map<PatriciaTrieNode,String> map = new java.util.LinkedHashMap<PatriciaTrieNode,String>();
        if (this.trie.getRoot()!=null)
            getNodesWhichRepresentsWords(this.trie.getRoot(),"",map);
        iterator = map.entrySet().iterator();
    }

    private void getNodesWhichRepresentsWords(PatriciaTrieNode node, String string, Map<PatriciaTrieNode,String> nodesMap) {
        StringBuilder builder = new StringBuilder(string);
        if (node.getString()!=null) 
            builder.append(node.getString());
        if (node.isType() == PatriciaTrieConstants.WHITE) 
            nodesMap.put(node,builder.toString()); //Terminating
        for (int i=0; i<node.childrenSize; i++) {
            PatriciaTrieNode child = node.getChild(i);
            getNodesWhichRepresentsWords(child,builder.toString(),nodesMap);
        }
    }

    @Override
    public boolean hasNext() {
        if (iterator!=null && iterator.hasNext()) 
            return true;
        return false;
    }
    
	@Override
    public C next() {
        if (iterator==null) 
            return null;

        java.util.Map.Entry<PatriciaTrieNode,String> entry = iterator.next();
        lastNode = entry.getKey();
        return (C)entry.getValue();
    }
    
    @Override
    public void remove() {
        if (iterator==null || trie==null) 
            return;

        iterator.remove();
        this.trie.remove(lastNode);
    }
	
}
