package tech.evelynn.collections.trie.radix;

import java.util.Iterator;
import java.util.Map;

public class JavaCompatibleIterator<K extends CharSequence, V> implements Iterator<Map.Entry<K, V>> {

	private RadixTrie<K, V> map = null;
	private Iterator<java.util.Map.Entry<K, V>> iter = null;
	private Map.Entry<K, V> lastEntry = null;

	public JavaCompatibleIterator(RadixTrie<K, V> map, java.util.Iterator<java.util.Map.Entry<K, V>> iter) {
		this.map = map;
		this.iter = iter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		if (iter == null)
			return false;
		return iter.hasNext();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.util.Map.Entry<K, V> next() {
		if (iter == null)
			return null;

		lastEntry = iter.next();
		return lastEntry;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		if (iter == null || lastEntry == null)
			return;

		map.remove(lastEntry.getKey());
		iter.remove();
	}
}
