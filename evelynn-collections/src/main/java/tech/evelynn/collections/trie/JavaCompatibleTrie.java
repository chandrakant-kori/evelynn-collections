package tech.evelynn.collections.trie;

import java.util.AbstractCollection;

public class JavaCompatibleTrie<C extends CharSequence> extends AbstractCollection<C> {

	private Trie<C> trie = null;

    public JavaCompatibleTrie(Trie<C> trie) {
        this.trie = trie;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(C value) {
        return trie.add(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(Object value) {
        return (trie.remove((C)value)!=null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return trie.contains((C)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return trie.getSize();
    }

    /**
     * {@inheritDoc}
     * 
     * WARNING: This iterator makes a copy of the trie's contents during it's construction!
     */
    @Override
    public java.util.Iterator<C> iterator() {
        return (new TrieIterator<C>(trie));
    }
}
