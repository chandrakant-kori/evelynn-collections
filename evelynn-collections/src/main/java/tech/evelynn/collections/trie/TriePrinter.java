package tech.evelynn.collections.trie;

public class TriePrinter {

	public static <C extends CharSequence> void print(Trie<C> trie) {
		System.out.println(getString(trie));
	}

	public static <C extends CharSequence> String getString(Trie<C> tree) {
		if (tree.getRoot() == null)
			return "Tree has no nodes.";
		return getString(tree.getRoot(), "", null, true);
	}

	protected static String getString(TrieNode node, String prefix, String previousString, boolean isTail) {
		StringBuilder builder = new StringBuilder();
		String string = null;
		if (node.getCharacter() != TrieNode.SENTINAL) {
			String temp = String.valueOf(node.getCharacter());
			if (previousString != null)
				string = previousString + temp;
			else
				string = temp;
		}
		builder.append(prefix + (isTail ? "└── " : "├── ")
				+ ((node.isWord() == true) ? ("(" + node.getCharacter() + ") " + string) : node.getCharacter()) + "\n");
		if (node.children != null) {
			for (int i = 0; i < node.childrenSize - 1; i++) {
				builder.append(getString(node.children[i], prefix + (isTail ? "    " : "│   "), string, false));
			}
			if (node.childrenSize >= 1) {
				builder.append(getString(node.children[node.childrenSize - 1], prefix + (isTail ? "    " : "│   "),
						string, true));
			}
		}
		return builder.toString();
	}

}
