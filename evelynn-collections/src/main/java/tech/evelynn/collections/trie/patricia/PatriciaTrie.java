package tech.evelynn.collections.trie.patricia;

import java.util.Arrays;
import java.util.Collection;

import tech.evelynn.collections.tree.Tree;

@SuppressWarnings("unchecked")
public class PatriciaTrie<C extends CharSequence> implements Tree<C> {

	private int size = 0;

	protected PatriciaTrieNodeCreator creator = null;
	private PatriciaTrieNode root = null;

	public PatriciaTrie() {
		this.creator = new PatriciaTrieNodeCreator() {
			@Override
			public PatriciaTrieNode createNewNode(PatriciaTrieNode parent, char[] seq, boolean type) {
				return (new PatriciaTrieNode(parent, seq, type));
			}
		};
	}
	
	public PatriciaTrie(PatriciaTrieNodeCreator creator) {
        this.creator = creator;
    }
	
	@Override
    public boolean add(C seq) {
		PatriciaTrieNode node = this.addSequence(seq);
        return (node != null);
    }

	public PatriciaTrieNode addSequence(C seq) {
        if (getRoot() == null)
            setRoot(this.creator.createNewNode(null, null, PatriciaTrieConstants.BLACK));

        int indexIntoParent = -1;
        int indexIntoString = -1;
        PatriciaTrieNode node = getRoot();
        for (int i = 0; i <= seq.length();) {
            indexIntoString = i;
            indexIntoParent++;
            if (i == seq.length())
                break;

            char c = seq.charAt(i);
            if (node.partOfThis(c, indexIntoParent)) {
                // PatriciaTrieNode has a char which is equal to char c at that index
                i++;
                continue;
            } else if (node.getString() != null && indexIntoParent < node.getString().length) {
                // string is equal to part of this PatriciaTrieNode's string
                break;
            }

            PatriciaTrieNode child = node.getChildBeginningWithChar(c);
            if (child != null) {
                // Found a child node starting with char c
                indexIntoParent = 0;
                node = child;
                i++;
            } else {
                // PatriciaTrieNode doesn't have a child starting with char c
                break;
            }
        }

        PatriciaTrieNode addedNode = null;
        PatriciaTrieNode parent = node.getParent();
        if (node.getString() != null && indexIntoParent < node.getString().length) {
            char[] parentString = Arrays.copyOfRange(node.getString(), 0, indexIntoParent);
            char[] refactorString = Arrays.copyOfRange(node.getString(), indexIntoParent, node.getString().length);

            if (indexIntoString < seq.length()) {
                // Creating a new parent by splitting a previous node and adding a new node

                // Create new parent
                if (parent != null) 
                    parent.removeChild(node);

                PatriciaTrieNode newParent = this.creator.createNewNode(parent, parentString, PatriciaTrieConstants.BLACK);
                if (parent != null)
                    parent.addChild(newParent);

                // Convert the previous node into a child of the new parent
                PatriciaTrieNode newNode1 = node;
                newNode1.setParent(newParent);
                newNode1.setString(refactorString);
                newParent.addChild(newNode1);

                // Create a new node from the rest of the string
                CharSequence newString = seq.subSequence(indexIntoString, seq.length());
                PatriciaTrieNode newNode2 = this.creator.createNewNode(newParent, newString.toString().toCharArray(), PatriciaTrieConstants.WHITE);
                newParent.addChild(newNode2);

                // New node which was added
                addedNode = newNode2;
            } else {
                // Creating a new parent by splitting a previous node and converting the previous node
                if (parent != null)
                    parent.removeChild(node);

                PatriciaTrieNode newParent = this.creator.createNewNode(parent, parentString, PatriciaTrieConstants.WHITE);
                if (parent != null)
                    parent.addChild(newParent);

                // Parent node was created
                addedNode = newParent;

                // Convert the previous node into a child of the new parent
                PatriciaTrieNode newNode1 = node;
                newNode1.setParent(newParent);
                newNode1.setString(refactorString);
                newParent.addChild(newNode1);
            }
        } else if (node.getString() != null && seq.length() == indexIntoString) {
            // Found a node who exactly matches a previous node

            // Already exists as a white node (leaf node)
            if (node.isType() == PatriciaTrieConstants.WHITE)
                return null;

            // Was black (branching), now white (leaf)
            node.setType(PatriciaTrieConstants.WHITE);
            addedNode = node;
        } else if (node.getString() != null) {
            // Adding a child
            CharSequence newString = seq.subSequence(indexIntoString, seq.length());
            PatriciaTrieNode newNode = this.creator.createNewNode(node, newString.toString().toCharArray(), PatriciaTrieConstants.WHITE);
            node.addChild(newNode);
            addedNode = newNode;
        } else {
            // Add to root node
            PatriciaTrieNode newNode = this.creator.createNewNode(node, seq.toString().toCharArray(), PatriciaTrieConstants.WHITE);
            node.addChild(newNode);
            addedNode = newNode;
        }

        setSize(getSize() + 1);
        return addedNode;
    }
	
	@Override
    public boolean contains(C seq) {
        PatriciaTrieNode node = getNode(seq);
        return (node != null && node.isType() == PatriciaTrieConstants.WHITE);
    }

	
	@Override
    public C remove(C seq) {
        C removed = null;
        PatriciaTrieNode node = getNode(seq);
        if (node!=null) removed = (C)(new String(node.getString()));
        remove(node);
        return removed;
    }
	
	public void remove(PatriciaTrieNode node) {
        if (node == null) 
            return;

        // No longer a white node (leaf)
        node.setType(PatriciaTrieConstants.BLACK);

        PatriciaTrieNode parent = node.getParent();
        if (node.getChildrenSize() == 0) {
            // Remove the node if it has no children
            if (parent != null) 
                parent.removeChild(node);
        } else if (node.getChildrenSize() == 1) {
            // Merge the node with it's child and add to node's parent
        	PatriciaTrieNode child = node.getChild(0);
            StringBuilder builder = new StringBuilder();
            builder.append(node.getString());
            builder.append(child.getString());
            child.setString(builder.toString().toCharArray());
            child.setParent(parent);

            if (parent != null) {
                parent.removeChild(node);
                parent.addChild(child);
            }
        }

        // Walk up the tree and see if we can compact it
        while (parent != null && parent.isType() == PatriciaTrieConstants.BLACK && parent.getChildrenSize() == 1) {
            PatriciaTrieNode child = parent.getChild(0);
            // Merge with parent
            StringBuilder builder = new StringBuilder();
            if (parent.getString() != null) 
                builder.append(parent.getString());
            builder.append(child.getString());
            child.setString(builder.toString().toCharArray());
            if (parent.getParent() != null) {
                child.setParent(parent.getParent());
                parent.getParent().removeChild(parent);
                parent.getParent().addChild(child);
            }
            parent = parent.getParent();
        }
        setSize(getSize() - 1);
    }
	
	@Override
    public void clear() {
        setRoot(null);
        setSize(0);
    }
	
	public PatriciaTrieNode getNode(C seq) {
		PatriciaTrieNode node = getRoot();
        int indexIntoParent = -1;
        for (int i = 0; i < seq.length();) {
            indexIntoParent++;

            char c = seq.charAt(i);
            if (node.partOfThis(c, indexIntoParent)) {
                // Node has a char which is equal to char c at that index
                i++;
                continue;
            } else if (node.getString() != null && indexIntoParent < node.getString().length) {
                // string is equal to part of this Node's string
                return null;
            }

            PatriciaTrieNode child = node.getChildBeginningWithChar(c);
            if (child != null) {
                // Found a child node starting with char c
                indexIntoParent = 0;
                node = child;
                i++;
            } else {
                // Node doesn't have a child starting with char c
                return null;
            }
        }

        if (node.getString()!=null && indexIntoParent == (node.getString().length - 1)) {
            // Get the partial string to compare against the node's string
            int length = node.getString().length;
            CharSequence sub = seq.subSequence(seq.length() - length, seq.length());
            for (int i = 0; i < length; i++) {
                if (node.getString()[i] != sub.charAt(i)) 
                    return null;
            }

            if (node.isType()== PatriciaTrieConstants.WHITE)
                return node;
        }
        return null;
    }
	
	@Override
    public int size() {
        return getSize();
    }
	
	@Override
    public Collection<C> toCollection() {
        return (new JavaCompatiblePatriciaTrie<C>(this));
    }

	@Override
    public String toString() {
        return PatriciaTriePrinter.getString(this);
    }

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public PatriciaTrieNode getRoot() {
		return root;
	}

	public void setRoot(PatriciaTrieNode root) {
		this.root = root;
	}

}
