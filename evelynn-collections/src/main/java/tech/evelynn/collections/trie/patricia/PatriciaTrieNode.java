package tech.evelynn.collections.trie.patricia;

import java.util.Arrays;

public class PatriciaTrieNode implements Comparable<PatriciaTrieNode> {

	private static final int MINIMUM_SIZE = 2;

	protected PatriciaTrieNode[] children = new PatriciaTrieNode[MINIMUM_SIZE];
	protected int childrenSize = 0;
	private PatriciaTrieNode parent = null;
	private boolean type = PatriciaTrieConstants.BLACK;
	private char[] string = null;

	protected PatriciaTrieNode(PatriciaTrieNode parent) {
		this.setParent(parent);
	}

	protected PatriciaTrieNode(PatriciaTrieNode parent, char[] seq) {
		this(parent);
		this.setString(seq);
	}

	protected PatriciaTrieNode(PatriciaTrieNode parent, char[] seq, boolean type) {
		this(parent, seq);
		this.setType(type);
	}

	protected void addChild(PatriciaTrieNode node) {
		int growSize = children.length;
		if (childrenSize >= children.length) {
			children = Arrays.copyOf(children, (growSize + (growSize >> 1)));
		}
		children[childrenSize++] = node;
		Arrays.sort(children, 0, childrenSize);
	}

	public boolean removeChild(PatriciaTrieNode child) {
		if (childrenSize == 0)
			return false;
		for (int i = 0; i < childrenSize; i++) {
			if (children[i].equals(child)) {
				return removeChild(i);
			}
		}
		return false;
	}

	protected int childIndex(char character) {
		for (int i = 0; i < childrenSize; i++) {
			PatriciaTrieNode c = children[i];
			if (c.getString() != null && c.getString().length > 0 && c.getString()[0] == character)
				return i;
		}
		return Integer.MIN_VALUE;
	}

	protected boolean removeChild(int index) {
		if (index >= childrenSize)
			return false;

		children[index] = null;
		childrenSize--;

		// Shift down the array
		System.arraycopy(children, index + 1, children, index, childrenSize - index);

		int shrinkSize = childrenSize;
		if (childrenSize >= MINIMUM_SIZE && childrenSize < (shrinkSize + (shrinkSize << 1))) {
			System.arraycopy(children, 0, children, 0, childrenSize);
		}

		return true;
	}

	public PatriciaTrieNode getChild(int index) {
		if (index >= childrenSize)
			return null;
		return children[index];
	}

	public int getChildrenSize() {
		return childrenSize;
	}

	protected boolean partOfThis(char c, int idx) {
		// Search myself
		if (getString() != null && idx < getString().length && getString()[idx] == c)
			return true;
		return false;
	}

	protected PatriciaTrieNode getChildBeginningWithChar(char c) {
		// Search children
		PatriciaTrieNode node = null;
		for (int i = 0; i < this.childrenSize; i++) {
			PatriciaTrieNode child = this.children[i];
			if (child.getString().length > 0 && child.getString()[0] == c)
				return child;
		}
		return node;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		String output = null;
		if (getString() != null)
			output = String.valueOf(getString());
		builder.append("string = ").append((output != null) ? output : "NULL").append("\n");
		builder.append("type = ").append(isType()).append("\n");
		return builder.toString();
	}

	@Override
	public int compareTo(PatriciaTrieNode node) {
		if (node == null)
			return -1;

		int length = getString().length;
		if (node.getString().length < length)
			length = node.getString().length;
		for (int i = 0; i < length; i++) {
			Character a = getString()[i];
			Character b = node.getString()[i];
			int c = a.compareTo(b);
			if (c != 0)
				return c;
		}

		if (this.isType() == PatriciaTrieConstants.BLACK && node.isType() == PatriciaTrieConstants.WHITE)
			return -1;
		else if (node.isType() == PatriciaTrieConstants.BLACK && this.isType() == PatriciaTrieConstants.WHITE)
			return 1;

		if (this.getChildrenSize() < node.getChildrenSize())
			return -1;
		else if (this.getChildrenSize() > node.getChildrenSize())
			return 1;

		return 0;
	}

	public char[] getString() {
		return string;
	}

	public void setString(char[] string) {
		this.string = string;
	}

	public boolean isType() {
		return type;
	}

	public void setType(boolean type) {
		this.type = type;
	}

	public PatriciaTrieNode getParent() {
		return parent;
	}

	public void setParent(PatriciaTrieNode parent) {
		this.parent = parent;
	}
}
