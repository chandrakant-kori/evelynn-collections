package tech.evelynn.collections.trie.patricia;

public class PatriciaTriePrinter {

	protected static <C extends CharSequence> String getString(PatriciaTrie<C> tree) {
        if (tree.getRoot() == null) 
            return "Tree has no nodes.";
        return getString(tree.getRoot(), "", null, true);
    }

    protected static String getString(PatriciaTrieNode node, String prefix, String previousString, boolean isTail) {
        StringBuilder builder = new StringBuilder();
        String thisString = "";
        if (node.getString() != null) 
            thisString = String.valueOf(node.getString());
        String fullString = ((previousString != null) ? previousString : "") + thisString;
        builder.append(prefix
                + (isTail ? "└── " : "├── ")
                + ((thisString != null) ? 
                            "[" + ((node.isType() == PatriciaTrieConstants.WHITE) ? "white" : "black") + "] "
                            + ((node.isType() == PatriciaTrieConstants.WHITE) ? "(" + thisString + ") " + fullString : thisString) 
                        : "["
                            + ((node.isType() == PatriciaTrieConstants.WHITE) ? "white" : "black") + "]") + "\n");
        if (node.children != null) {
            for (int i = 0; i < node.getChildrenSize() - 1; i++) {
                builder.append(getString(node.getChild(i), prefix + (isTail ? "    " : "│   "), fullString, false));
            }
            if (node.getChildrenSize() >= 1) {
                builder.append(getString(node.getChild(node.getChildrenSize() - 1), prefix + (isTail ? "    " : "│   "), fullString, true));
            }
        }
        return builder.toString();
    }
}
