package tech.evelynn.collections.trie.radix;

import tech.evelynn.collections.trie.patricia.PatriciaTrieNode;

public final class RadixNode<K extends CharSequence, V> extends PatriciaTrieNode {

	protected V value = null;

	protected RadixNode(PatriciaTrieNode node, V value) {
		super(node.getParent(), node.getString(), node.isType());
		this.value = value;
		for (int i = 0; i < node.getChildrenSize(); i++) {
			PatriciaTrieNode c = node.getChild(i);
			this.addChild(c);
		}
	}

	protected RadixNode(PatriciaTrieNode parent, char[] string, boolean type) {
		super(parent, string, type);
	}

	protected RadixNode(PatriciaTrieNode parent, char[] string, boolean type, V value) {
		super(parent, string, type);
		this.value = value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString());
		builder.append("value = ").append(value).append("\n");
		return builder.toString();
	}
}
