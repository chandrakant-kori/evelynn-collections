package tech.evelynn.collections.trie.radix;

import tech.evelynn.collections.map.JavaCompatibleMapEntry;
import tech.evelynn.collections.trie.patricia.PatriciaTrieConstants;
import tech.evelynn.collections.trie.patricia.PatriciaTrieNode;

@SuppressWarnings("unchecked")
public class JavaCompatibleRadixTrieMap<K extends CharSequence,V> extends java.util.AbstractMap<K,V> {

	private RadixTrie<K,V> map = null;

    protected JavaCompatibleRadixTrieMap(RadixTrie<K,V> map) {
        this.map = map;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V put(K key, V value) {
        return map.put(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V remove(Object key) {
        return map.remove((K)key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        map.clear();
    }

    /**
     * {@inheritDoc}
     */
	@Override
    public boolean containsKey(Object key) {
        return map.contains((K)key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return map.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public java.util.Set<java.util.Map.Entry<K, V>> entrySet() {
        java.util.Set<java.util.Map.Entry<K, V>> set = new java.util.HashSet<java.util.Map.Entry<K, V>>() {

            private static final long serialVersionUID = 1L;

            /**
             * {@inheritDoc}
             */
            @Override
            public java.util.Iterator<java.util.Map.Entry<K, V>> iterator() {
                return (new JavaCompatibleIterator<K,V>(map,super.iterator()));
            }
        };
        if (map.getTrie()!=null && map.getTrie().getRoot()!=null) {
            PatriciaTrieNode n = map.getTrie().getRoot();
            levelOrder(n,"",set);
        }
        return set;
    }

    private void levelOrder(PatriciaTrieNode node, String string, java.util.Set<java.util.Map.Entry<K, V>> set) {
        StringBuilder builder = new StringBuilder(string);
        RadixNode<K,V> tmn = null;
        if (node instanceof RadixNode) {
            tmn = (RadixNode<K,V>)node;
            if (tmn.getString()!=null) 
                builder.append(tmn.getString());
            if (tmn.isType() == PatriciaTrieConstants.WHITE) {
                K s = (K)builder.toString();
                java.util.Map.Entry<K, V> entry = new JavaCompatibleMapEntry<K, V>(s, tmn.value);
                set.add(entry);
            }
        }

        String b = builder.toString();
        for (int i=0; i<node.getChildrenSize(); i++) {
            PatriciaTrieNode n = node.getChild(i);
            levelOrder(n,b,set);
        }
    }
}
