package tech.evelynn.collections.trie.patricia;

public interface PatriciaTrieNodeCreator {

	public PatriciaTrieNode createNewNode(PatriciaTrieNode parent, char[] seq, boolean type);

}
