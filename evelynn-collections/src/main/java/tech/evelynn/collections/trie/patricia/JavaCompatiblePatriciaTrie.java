package tech.evelynn.collections.trie.patricia;

import java.util.AbstractCollection;
import java.util.Iterator;

@SuppressWarnings("unchecked")
public class JavaCompatiblePatriciaTrie<C extends CharSequence> extends AbstractCollection<C> {

	private PatriciaTrie<C> trie = null;

    public JavaCompatiblePatriciaTrie(PatriciaTrie<C> list) {
        this.trie = list;
    }
    
    @Override
    public boolean add(C value) {
        return trie.add(value);
    }
    
    @Override
    public boolean remove(Object value) {
        return (trie.remove((C)value)!=null);
    }
    
	@Override
    public boolean contains(Object value) {
        return trie.contains((C)value);
    }
    
    @Override
    public int size() {
        return trie.getSize();
    }

    @Override
    public Iterator<C> iterator() {
        return (new PatriciaTrieIterator<C>(trie));
    }
}
