package tech.evelynn.collections.trie.patricia;

import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("unchecked")
public class CompactSuffixTrie<C extends CharSequence> {

	private PatriciaTrie<C> tree = null;

	public CompactSuffixTrie(C sequence) {
		tree = new PatriciaTrie<C>();
		int length = sequence.length();
		for (int i = 0; i < length; i++) {
			CharSequence seq = sequence.subSequence(i, length);
			tree.add((C) seq);
		}
	}

	public boolean add(C sequence) {
		int length = sequence.length();
		for (int i = 0; i < length; i++) {
			CharSequence seq = sequence.subSequence(i, length);
			tree.add((C) seq);
		}
		return true;
	}

	public boolean doesSubStringExist(C sequence) {
		char[] chars = sequence.toString().toCharArray();
		int length = chars.length;
		PatriciaTrieNode current = tree.getRoot();
		int index = 0;
		for (int i = 0; i < length; i++) {
			int innerStringLength = (current.getString() != null) ? current.getString().length : 0;
			char c = chars[i];
			if (innerStringLength > index) {
				boolean inThis = current.partOfThis(c, index++);
				if (!inThis)
					return false;
			} else {
				int idx = current.childIndex(c);
				if (idx < 0)
					return false;
				current = current.getChild(idx);
				index = 1;
			}
		}
		return true;
	}

	public Set<String> getSuffixes() {
		return this.getSuffixes(tree.getRoot());
	}

	private Set<String> getSuffixes(PatriciaTrieNode node) {
		StringBuilder builder = new StringBuilder();
		if (node.getString() != null)
			builder.append(node.getString());
		Set<String> set = new TreeSet<String>();
		if (node.isType() == PatriciaTrieConstants.WHITE) {
			set.add(builder.toString());
		}
		for (int i = 0; i < node.getChildrenSize(); i++) {
			PatriciaTrieNode c = node.getChild(i);
			set.addAll(getSuffixes(c, builder.toString()));
		}
		return set;
	}

	private Set<String> getSuffixes(PatriciaTrieNode node, String prefix) {
		StringBuilder builder = new StringBuilder(prefix);
		if (node.getString() != null)
			builder.append(node.getString());
		Set<String> set = new TreeSet<String>();
		if (node.isType() == PatriciaTrieConstants.WHITE) {
			set.add(builder.toString());
		}
		for (int i = 0; i < node.getChildrenSize(); i++) {
			PatriciaTrieNode c = node.getChild(i);
			set.addAll(getSuffixes(c, builder.toString()));
		}
		return set;
	}

	@Override
	public String toString() {
		return PatriciaTriePrinter.getString(tree);
	}

	public boolean equals(CompactSuffixTrie<C> trie) {
		if (this.getSuffixes().equals(trie.getSuffixes()))
			return true;
		return false;
	}

}
