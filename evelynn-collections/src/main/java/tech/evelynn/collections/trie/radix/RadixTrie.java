package tech.evelynn.collections.trie.radix;

import tech.evelynn.collections.map.Map;
import tech.evelynn.collections.trie.patricia.PatriciaTrie;
import tech.evelynn.collections.trie.patricia.PatriciaTrieNode;
import tech.evelynn.collections.trie.patricia.PatriciaTrieNodeCreator;

@SuppressWarnings("unchecked")
public class RadixTrie<K extends CharSequence, V> implements PatriciaTrieNodeCreator, Map<K, V> {

	private PatriciaTrie<K> trie = null;

	public RadixTrie() {
		setTrie(new PatriciaTrie<K>(this));
	}

	@Override
	public V put(K key, V value) {
		V prev = null;
		PatriciaTrieNode node = getTrie().addSequence(key);

		if (node != null && node instanceof RadixNode) {
			RadixNode<K, V> radixNode = (RadixNode<K, V>) node;
			if (radixNode.value != null)
				prev = radixNode.value;
			radixNode.value = value;
		}
		return prev;
	}

	@Override
	public V get(K key) {
		PatriciaTrieNode node = getTrie().getNode(key);
		if (node != null && node instanceof RadixNode) {
			RadixNode<K, V> radixNode = (RadixNode<K, V>) node;
			return radixNode.value;
		}
		return null;
	}

	@Override
	public boolean contains(K key) {
		return getTrie().contains(key);
	}

	@Override
	public V remove(K key) {
		PatriciaTrieNode node = getTrie().getNode(key);
		V value = null;
		if (node != null) {
			if (node instanceof RadixNode) {
				RadixNode<K, V> rn = (RadixNode<K, V>) node;
				value = rn.value;
				rn.value = null;
			}
			getTrie().remove(node);
		}
		return value;
	}

	@Override
	public void clear() {
		getTrie().clear();
	}

	@Override
	public int size() {
		return getTrie().size();
	}

	@Override
	public java.util.Map<K, V> toMap() {
		return (new JavaCompatibleRadixTrieMap<K, V>(this));
	}

	@Override
	public String toString() {
		return RadixTreePrinter.getString(this);
	}

	@Override
	public PatriciaTrieNode createNewNode(PatriciaTrieNode parent, char[] seq, boolean type) {
		return (new RadixNode<K, V>(parent, seq, type));
	}

	public PatriciaTrie<K> getTrie() {
		return trie;
	}

	public void setTrie(PatriciaTrie<K> trie) {
		this.trie = trie;
	}

}
