package tech.evelynn.collections.trie;

public interface TrieNodeCreator {

	public TrieNode createNewNode(TrieNode parent, Character character, boolean isWord);
}
