package tech.evelynn.collections.trie;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class TrieIterator<C extends CharSequence> implements Iterator<C> {

	private Trie<C> trie = null;
	private TrieNode lastNode = null;
	private Iterator<Map.Entry<TrieNode, String>> iterator = null;

	protected TrieIterator(Trie<C> trie) {
		this.trie = trie;
		Map<TrieNode, String> map = new LinkedHashMap<TrieNode, String>();
		if (this.trie.getRoot() != null) {
			getNodesWhichRepresentsWords(this.trie.getRoot(), "", map);
		}
		iterator = map.entrySet().iterator();
	}

	private void getNodesWhichRepresentsWords(TrieNode node, String string, java.util.Map<TrieNode, String> nodesMap) {
		StringBuilder builder = new StringBuilder(string);
		if (node.getCharacter() != TrieNode.SENTINAL)
			builder.append(node.getCharacter());
		if (node.isWord())
			nodesMap.put(node, builder.toString());
		for (int i = 0; i < node.childrenSize; i++) {
			TrieNode child = node.getChild(i);
			getNodesWhichRepresentsWords(child, builder.toString(), nodesMap);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		if (iterator != null && iterator.hasNext())
			return true;
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public C next() {
		if (iterator == null)
			return null;

		java.util.Map.Entry<TrieNode, String> entry = iterator.next();
		lastNode = entry.getKey();
		return (C) entry.getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		if (iterator == null || trie == null)
			return;

		iterator.remove();
		this.trie.remove(lastNode);
	}

}
