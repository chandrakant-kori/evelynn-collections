package tech.evelynn.collections.list;

import java.util.Collection;
import java.util.List;

public class SinglyLinkedList<T> extends AbstractList<T> {

	private int size = 0;
	private Node<T> head = null;
	private Node<T> tail = null;

	public Node<T> getHead() {
		return head;
	}

	public void setHead(Node<T> head) {
		this.head = head;
	}
	
	public static class Node<T> {

		private T value = null;
		private Node<T> next = null;

		@SuppressWarnings("unused")
		private Node() {
		}

		Node(T value) {
			this.setValue(value);
		}

		@Override
		public String toString() {
			return "value=" + getValue() + " next=" + ((getNext() != null) ? getNext().getValue() : "NULL");
		}

		public Node<T> getNext() {
			return next;
		}

		public void setNext(Node<T> next) {
			this.next = next;
		}

		public T getValue() {
			return value;
		}

		public void setValue(T value) {
			this.value = value;
		}
	}

	@Override
	public boolean add(T value) {
		return add(new Node<T>(value));
	}

	private boolean add(Node<T> node) {
		if (getHead() == null) {
			setHead(node);
			setTail(node);
		} else {
			Node<T> prev = getTail();
			prev.setNext(node);
			setTail(node);
		}
		setSize(getSize() + 1);
		return true;
	}

	public boolean remove(T value) {
		// Find the node
		Node<T> prev = null;
		Node<T> node = getHead();
		while (node != null && (!node.getValue().equals(value))) {
			prev = node;
			node = node.getNext();
		}

		if (node == null)
			return false;

		// Update the tail, if needed
		if (node.equals(getTail())) {
			setTail(prev);
			if (prev != null)
				prev.setNext(null);
		}

		Node<T> next = node.getNext();
		if (prev != null && next != null) {
			prev.setNext(next);
		} else if (prev != null && next == null) {
			prev.setNext(null);
		} else if (prev == null && next != null) {
			// Node is the head
			setHead(next);
		} else {
			// prev==null && next==null
			setHead(null);
		}

		setSize(getSize() - 1);
		return true;
	}

	@Override
	public void clear() {
		setHead(null);
		setSize(0);
	}

	@Override
	public boolean contains(T value) {
		Node<T> node = getHead();
		while (node != null) {
			if (node.getValue().equals(value))
				return true;
			node = node.getNext();
		}
		return false;
	}

	@Override
	public int size() {
		return getSize();
	}

	@Override
	public List<T> toList() {
		return (new JavaCompatibleSinglyLinkedList<T>(this));
	}

	@Override
	public Collection<T> toCollection() {
		return (new JavaCompatibleSinglyLinkedList<T>(this));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		Node<T> node = getHead();
		while (node != null) {
			builder.append(node.getValue()).append(", ");
			node = node.getNext();
		}
		return builder.toString();
	}

	public Node<T> getTail() {
		return tail;
	}

	public void setTail(Node<T> tail) {
		this.tail = tail;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

}
