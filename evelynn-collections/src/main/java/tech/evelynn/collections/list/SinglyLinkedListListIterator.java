package tech.evelynn.collections.list;

public class SinglyLinkedListListIterator<T> implements java.util.ListIterator<T> {

	private int index = 0;

	private SinglyLinkedList<T> list = null;
	private SinglyLinkedList.Node<T> prev = null;
	private SinglyLinkedList.Node<T> next = null;
	private SinglyLinkedList.Node<T> last = null;

	public SinglyLinkedListListIterator(SinglyLinkedList<T> list) {
		this.list = list;
		this.next = list.getHead();
		this.prev = null;
		this.last = null;
	}

	@Override
	public void add(T value) {
		SinglyLinkedList.Node<T> node = new SinglyLinkedList.Node<T>(value);

		if (list.getHead() == null) {
			list.setHead(node);
			list.setTail(node);
		} else {
			SinglyLinkedList.Node<T> p = null;
			SinglyLinkedList.Node<T> n = list.getHead();
			while (n != null && !(n.equals(next))) {
				p = node;
				n = node.getNext();
			}
			if (p != null) {
				p.setNext(node);
			} else {
				// replacing head
				list.setHead(node);
			}
			node.setNext(n);
		}
		this.next = node;

		list.setSize(list.getSize() + 1);
	}

	@Override
	public void remove() {
		if (last == null)
			return;

		SinglyLinkedList.Node<T> p = null;
		SinglyLinkedList.Node<T> node = this.last;
		while (node != null && !(node.equals(last))) {
			p = node;
			node = node.getNext();
		}

		SinglyLinkedList.Node<T> n = last.getNext();
		if (p != null)
			p.setNext(n);

		if (last.equals(list.getHead()))
			list.setHead(n);
		if (last.equals(list.getTail()))
			list.setTail(p);

		list.setSize(list.getSize() - 1);
	}

	@Override
	public void set(T value) {
		if (last != null)
			last.setValue(value);
	}

	@Override
	public boolean hasNext() {
		return (next != null);
	}

	@Override
	public boolean hasPrevious() {
		return (prev != null);
	}

	@Override
	public int nextIndex() {
		return index;
	}

	@Override
	public int previousIndex() {
		return index - 1;
	}

	@Override
	public T next() {
		if (next == null)
			throw new java.util.NoSuchElementException();

		index++;
		last = next;
		prev = next;
		next = next.getNext();

		return last.getValue();
	}

	@Override
	public T previous() {
		if (prev == null)
			throw new java.util.NoSuchElementException();

		index--;
		last = prev;
		next = prev;

		SinglyLinkedList.Node<T> p = null;
		SinglyLinkedList.Node<T> node = this.list.getHead();
		while (node != null && !(node.equals(prev))) {
			p = node;
			node = node.getNext();
		}
		prev = p;

		return last.getValue();
	}
}
