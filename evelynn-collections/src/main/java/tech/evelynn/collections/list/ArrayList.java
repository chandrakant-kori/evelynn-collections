package tech.evelynn.collections.list;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@SuppressWarnings("unchecked")
public class ArrayList<T> extends AbstractList<T> {

	private static final int MINIMUM_SIZE = 1024;

	private int size = 0;
	private T[] array = (T[]) new Object[MINIMUM_SIZE];

	public ArrayList(int initialListSize) {
		this.size = initialListSize;
	}


	public ArrayList() {}


	@Override
	public boolean add(T value) {
		return add(size, value);
	}

	public boolean add(int index, T value) {
		if (size >= array.length)
			grow();
		if (index == size) {
			array[size] = value;
		} else {
			System.arraycopy(array, index, array, index + 1, size - index);
			array[index] = value;
		}
		size++;
		return true;
	}

	private void grow() {
		int growSize = size + (size << 1);
		array = Arrays.copyOf(array, growSize);
	}

	@Override
	public boolean remove(T value) {
		for (int i = 0; i < size; i++) {
			T obj = array[i];
			if (obj.equals(value)) {
				remove(i);
				return true;
			}
		}
		return false;
	}

	public T remove(int index) {
		if (index < 0 || index >= size)
			return null;
		T t = array[index];
		if (index != --size) {
			// Shift the array down one spot
			System.arraycopy(array, index + 1, array, index, size - index);
		}
		array[size] = null;

		int shrinkSize = array.length >> 1;
		if (shrinkSize >= MINIMUM_SIZE && size < shrinkSize)
			shrink();
		return t;
	}

	private void shrink() {
		int shrinkSize = array.length >> 1;
		array = Arrays.copyOf(array, shrinkSize);
	}

	public T set(int index, T value) {
		if (index < 0 || index >= size)
			return null;
		T t = array[index];
		array[index] = value;
		return t;
	}

	public T get(int index) {
		if (index < 0 || index >= size)
			return null;
		return array[index];
	}

	@Override
	public void clear() {
		size = 0;
	}

	@Override
	public boolean contains(T value) {
		for (int i = 0; i < size; i++) {
			T obj = array[i];
			if (obj.equals(value))
				return true;
		}
		return false;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public List<T> toList() {
		return (new JavaCompatibleArrayList<T>(this));
	}

	@Override
	public Collection<T> toCollection() {
		return (new JavaCompatibleArrayList<T>(this));
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < size; i++) {
			builder.append(array[i]).append(",");
		}
		return builder.toString();
	}

}
