package tech.evelynn.collections.list.implicitkey;

public class ImplicitKeyPair<T> {

	private ImplicitKeyNode<T> left;
	private ImplicitKeyNode<T> right;

	public ImplicitKeyPair(ImplicitKeyNode<T> left, ImplicitKeyNode<T> right) {
		this.setLeft(left);
		this.setRight(right);
	}

	public ImplicitKeyNode<T> getLesser() {
		return getLeft();
	}

	public ImplicitKeyNode<T> getGreater() {
		return getRight();
	}

	@Override
	public String toString() {
		return "left={" + getLeft().toString() + "} right={" + getRight().toString() + "}";
	}

	public ImplicitKeyNode<T> getLeft() {
		return left;
	}

	public void setLeft(ImplicitKeyNode<T> left) {
		this.left = left;
	}

	public ImplicitKeyNode<T> getRight() {
		return right;
	}

	public void setRight(ImplicitKeyNode<T> right) {
		this.right = right;
	}
}