package tech.evelynn.collections.list.implicitkey;

import java.util.ArrayList;

public class ImplicitKeyTreapPrinter {

	public static <T> String getString(ImplicitKeyTreap<T> tree) {
		if (tree.root == null)
			return "Tree has no nodes.";
		return getString(tree.root, "", true);
	}

	private static <T> String getString(ImplicitKeyNode<T> node, String prefix, boolean isTail) {
		StringBuilder builder = new StringBuilder();

		if (node.getParent() != null) {
			String side = "left";
			if (node.equals(node.getParent().getRight()))
				side = "right";
			builder.append(prefix + (isTail ? "└── " : "├── ") + "(" + side + ") " + node.getValue() + "\n");
		} else {
			builder.append(prefix + (isTail ? "└── " : "├── ") + node.getValue() + "\n");
		}
		java.util.List<ImplicitKeyNode<T>> children = null;
		if (node.getLeft() != null || node.getRight() != null) {
			children = new ArrayList<ImplicitKeyNode<T>>(2);
			if (node.getLeft() != null)
				children.add(node.getLeft());
			if (node.getRight() != null)
				children.add(node.getRight());
		}
		if (children != null) {
			for (int i = 0; i < children.size() - 1; i++) {
				builder.append(getString(children.get(i), prefix + (isTail ? "    " : "│   "), false));
			}
			if (children.size() >= 1) {
				builder.append(getString(children.get(children.size() - 1),
						prefix + (isTail ? "    " : "│   "), true));
			}
		}

		return builder.toString();
	}
}
