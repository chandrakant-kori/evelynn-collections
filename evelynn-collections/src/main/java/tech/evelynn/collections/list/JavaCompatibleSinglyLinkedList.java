package tech.evelynn.collections.list;

@SuppressWarnings("unchecked")
public class JavaCompatibleSinglyLinkedList<T> extends java.util.AbstractSequentialList<T> {

	private SinglyLinkedList<T> list = null;

	public JavaCompatibleSinglyLinkedList(SinglyLinkedList<T> list) {
		this.list = list;
	}

	@Override
    public boolean add(T value) {
        return list.add(value);
    }
	
	
	@Override
    public boolean remove(Object value) {
        return list.remove((T)value);
    }
	
	@Override
    public boolean contains(Object value) {
        return list.contains((T)value);
    }
	
	@Override
    public int size() {
        return list.size();
    }
	
	@Override
    public java.util.ListIterator<T> listIterator(int index) {
        return (new SinglyLinkedListListIterator<T>(list));
    }

}
