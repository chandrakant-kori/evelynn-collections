package tech.evelynn.collections.list.implicitkey;

@SuppressWarnings("unchecked")
public class JavaCompatibleImplicitKeyTreap<T> extends java.util.AbstractList<T> implements java.util.RandomAccess {

	private ImplicitKeyTreap<T> list = null;

	public JavaCompatibleImplicitKeyTreap(ImplicitKeyTreap<T> list) {
		this.list = list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean add(T value) {
		return list.add(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(Object value) {
		return list.remove((T) value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Object value) {
		return list.contains((T) value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return list.size;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(int index, T value) {
		list.add(index, value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T remove(int index) {
		return list.removeAtIndex(index);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T get(int index) {
		T t = list.getAtIndex(index);
		if (t != null)
			return t;
		throw new IndexOutOfBoundsException();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T set(int index, T value) {
		return list.set(index, value);
	}
}
