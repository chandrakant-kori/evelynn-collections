package tech.evelynn.collections.list.implicitkey;

import java.util.Random;

import tech.evelynn.collections.list.List;

@SuppressWarnings("unchecked")
public class ImplicitKeyTreap<T> implements List<T> {

	public static final Random RANDOM = new Random();

	public static int randomSeed = Integer.MAX_VALUE; // This should be at least twice the number of Nodes

	protected ImplicitKeyNode<T> root = null;
	protected int size = 0;

	public ImplicitKeyTreap() {
	}

	public ImplicitKeyTreap(int randomSeed) {
		this();
		ImplicitKeyTreap.randomSeed = randomSeed;
	}

	@Override
	public boolean add(T value) {
		final T t = add(size, value);
		return (t != null);
	}

	public T add(int index, T value) {
		addAtIndexAndUpdate(index, value);

		// Check to make sure value was added
		final ImplicitKeyNode<T> n = getNodeByIndex(index);
		if (n == null)
			return null;
		return n.getValue();
	}

	@Override
	public boolean remove(T value) {
		// See if value already exists
		final int idx = getIndexByValue(value);
		if (idx < 0)
			return false;

		final T t = removeAtIndex(idx);
		return (t != null);
	}

	public T removeAtIndex(int index) {
		// See if index already exists
		ImplicitKeyNode<T> n = getNodeByIndex(index);
		if (n == null)
			return null;

		removeAtIndexAndUpdate(index);
		return n.getValue();
	}

	public T set(int index, T value) {
		// See if index already exists
		final ImplicitKeyNode<T> n = getNodeByIndex(index);
		if (n == null)
			return null;

		n.setValue(value);
		return n.getValue();
	}

	@Override
	public boolean contains(T value) {
		final ImplicitKeyNode<T> n = getNodeByValue(value);
		return (n != null);
	}

	public T getAtIndex(int index) {
		final ImplicitKeyNode<T> n = getNodeByIndex(index);
		if (n == null)
			return null;
		return n.getValue();
	}

	@Override
	public void clear() {
		root = null;
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public java.util.List<T> toList() {
		return (new JavaCompatibleImplicitKeyTreap<T>(this));
	}

	@Override
	public java.util.Collection<T> toCollection() {
		return (new JavaCompatibleImplicitKeyTreap<T>(this));
	}

	public ImplicitKeyPair<T> split(int index) {
		final ImplicitKeyPair<T> p = split((ImplicitKeyNode<T>) root, index);
		if (p.getLeft() != null)
			p.getLeft().setParent(null);
		if (p.getRight() != null)
			p.getRight().setParent(null);
		return p;
	}

	public void addAtIndexAndUpdate(int index, T value) {
		root = insert(((ImplicitKeyNode<T>) root), index, value);
		if (root == null)
			size = 0;
		else
			size = (((ImplicitKeyNode<T>) root).getSize());
	}

	private void removeAtIndexAndUpdate(int index) {
		root = remove(((ImplicitKeyNode<T>) root), index);
		if (root == null)
			size = 0;
		else
			size = (((ImplicitKeyNode<T>) root).getSize());
	}

	private ImplicitKeyNode<T> getNodeByValue(T value) {
		return getNodeByValue(root, value);
	}

	private ImplicitKeyNode<T> getNodeByIndex(int index) {
		if (root == null)
			return null;

		final ImplicitKeyNode<T> l = (ImplicitKeyNode<T>) root.getLeft();
		final ImplicitKeyNode<T> r = (ImplicitKeyNode<T>) root.getRight();
		final int leftSize = ((l != null) ? l.getSize() : 0);
		final int idx = leftSize;

		if (idx == index) {
			return root;
		} else if (index < leftSize) {
			return getNodeByIndex(l, idx, index);
		} else {
			return getNodeByIndex(r, idx, index);
		}
	}

	private int getIndexByValue(T value) {
		final ImplicitKeyNode<T> node = (ImplicitKeyNode<T>) root;
		if (value == null || node == null)
			return Integer.MIN_VALUE;

		final ImplicitKeyNode<T> l = (ImplicitKeyNode<T>) node.getLeft();
		final ImplicitKeyNode<T> r = (ImplicitKeyNode<T>) node.getRight();
		final int leftSize = ((l != null) ? l.getSize() : 0);
		final int idx = leftSize;

		if (value.equals(node.getValue()))
			return idx;

		int i = getIndexByValue(l, idx, value);
		if (i >= 0)
			return i;

		i = getIndexByValue(r, idx, value);
		return i;
	}

	public static <T> ImplicitKeyPair<T> split(ImplicitKeyNode<T> node, int index) {
		if (node == null)
			return new ImplicitKeyPair<T>(null, null);

		final int leftSize = (node.getLeft() != null ? ((ImplicitKeyNode<T>) node.getLeft()).getSize() : 0);
		if (index <= leftSize) {
			final ImplicitKeyPair<T> sub = split((ImplicitKeyNode<T>) node.getLeft(), index);
			node.setLeft(sub.getRight());
			if (node.getLeft() != null)
				node.getLeft().setParent(node);
			sub.setRight(node);
			node.update();
			return sub;
		}
		// else
		final ImplicitKeyPair<T> sub = split((ImplicitKeyNode<T>) node.getRight(), (index - leftSize - 1));
		node.setRight(sub.getLeft());
		if (node.getRight() != null)
			node.getRight().setParent(node);
		sub.setLeft(node);
		node.update();
		return sub;
	}

	public static <T> ImplicitKeyNode<T> merge(ImplicitKeyNode<T> left, ImplicitKeyNode<T> right) {
		if (left == null)
			return right;

		if (right == null)
			return left;

		if (left.getPriority() < right.getPriority()) {
			left.setRight(merge((ImplicitKeyNode<T>) left.getRight(), right));
			if (left.getRight() != null)
				left.getRight().setParent(left);
			left.update();
			return left;
		}
		// else
		right.setLeft(merge(left, (ImplicitKeyNode<T>) right.getLeft()));
		if (right.getLeft() != null)
			right.getLeft().setParent(right);
		right.update();
		return right;
	}

	private static <T> ImplicitKeyNode<T> insert(ImplicitKeyNode<T> root, int index, T value) {
		final ImplicitKeyPair<T> p = split(root, index);
		return merge(merge((ImplicitKeyNode<T>) p.getLeft(), new ImplicitKeyNode<T>(value)),
				(ImplicitKeyNode<T>) p.getRight());
	}

	private static <T> ImplicitKeyNode<T> remove(ImplicitKeyNode<T> root, int index) {
		final ImplicitKeyPair<T> p = split(root, index);
		final int leftSize = (p.getLeft() != null ? ((ImplicitKeyNode<T>) p.getLeft()).getSize() : 0);
		return merge(p.getLeft(), (split(p.getRight(), (index + 1 - leftSize))).getRight());
	}

	private static <T> ImplicitKeyNode<T> getNodeByValue(ImplicitKeyNode<T> node, T value) {
		if (node == null)
			return null;

		if (node.getValue().equals(value))
			return node;

		ImplicitKeyNode<T> n = getNodeByValue(node.getLeft(), value);
		if (n == null)
			n = getNodeByValue(node.getRight(), value);
		return n;
	}

	private static <T> ImplicitKeyNode<T> getNodeByIndex(ImplicitKeyNode<T> node, int parentIndex, int index) {
		if (node == null)
			return null;

		final ImplicitKeyNode<T> p = (ImplicitKeyNode<T>) node.getParent();
		final ImplicitKeyNode<T> l = (ImplicitKeyNode<T>) node.getLeft();
		final ImplicitKeyNode<T> r = (ImplicitKeyNode<T>) node.getRight();
		final int leftSize = ((l != null) ? l.getSize() : 0);
		final int rightSize = ((r != null) ? r.getSize() : 0);

		int idx = Integer.MIN_VALUE;
		if (p != null && node.equals(p.getLeft())) {
			// left
			idx = parentIndex - rightSize - 1;
		} else if (p != null && node.equals(p.getRight())) {
			// right
			idx = leftSize + parentIndex + 1;
		} else {
			throw new RuntimeException("I do not have a parent :-(");
		}

		if (idx == index)
			return node;

		if (index <= idx) {
			return getNodeByIndex(l, idx, index);
		} else {
			return getNodeByIndex(r, idx, index);
		}
	}

	private static <T> int getIndexByValue(ImplicitKeyNode<T> node, int parentIndex, T value) {
		if (node == null)
			return Integer.MIN_VALUE;

		final ImplicitKeyNode<T> p = (ImplicitKeyNode<T>) node.getParent();
		final ImplicitKeyNode<T> l = (ImplicitKeyNode<T>) node.getLeft();
		final ImplicitKeyNode<T> r = (ImplicitKeyNode<T>) node.getRight();
		final int leftSize = ((l != null) ? l.getSize() : 0);
		final int rightSize = ((r != null) ? r.getSize() : 0);

		int idx = Integer.MIN_VALUE;
		if (p != null && node.equals(p.getLeft())) {
			// left
			idx = parentIndex - rightSize - 1;
		} else if (p != null && node.equals(p.getRight())) {
			// right
			idx = leftSize + parentIndex + 1;
		} else {
			throw new RuntimeException("I do not have a parent :-(");
		}

		if (value.equals(node.getValue()))
			return idx;

		int i = getIndexByValue(l, idx, value);
		if (i >= 0)
			return i;

		i = getIndexByValue(r, idx, value);
		return i;
	}

	public T[] inOrder() {
		return inOrder(root, size);
	}

	public static <T> T[] inOrder(ImplicitKeyNode<T> node, int size) {
		T[] data = (T[]) new Object[size];
		if (node == null)
			return data;

		inOrder(node, data, 0);
		return data;
	}

	private static <T> int inOrder(ImplicitKeyNode<T> node, T[] data, int idx) {
		if (node == null)
			return idx;

		idx = inOrder(node.getLeft(), data, idx);
		data[idx++] = node.getValue();
		idx = inOrder(node.getRight(), data, idx);
		return idx;
	}

	@Override
	public String toString() {
		return ImplicitKeyTreapPrinter.getString(this);
	}

}
