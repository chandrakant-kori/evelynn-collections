package tech.evelynn.collections.list;

import java.util.Collection;
import java.util.List;

public class DoublyLinkedList<T> extends AbstractList<T> {

	private int size = 0;
    private Node<T> head = null;
    private Node<T> tail = null;
    
    public static class Node<T> {

        private T value = null;
        private Node<T> prev = null;
        private Node<T> next = null;

        @SuppressWarnings("unused")
		private Node() { }

        public Node(T value) {
            this.setValue(value);
        }

        @Override
        public String toString() {
            return "value=" + getValue() + " previous=" + ((getPrev() != null) ? getPrev().getValue() : "NULL")
                    + " next=" + ((getNext() != null) ? getNext().getValue() : "NULL");
        }

		public Node<T> getNext() {
			return next;
		}

		public void setNext(Node<T> next) {
			this.next = next;
		}

		public Node<T> getPrev() {
			return prev;
		}

		public void setPrev(Node<T> prev) {
			this.prev = prev;
		}

		public T getValue() {
			return value;
		}

		public void setValue(T value) {
			this.value = value;
		}
    }
    
    @Override
    public boolean add(T value) {
        return add(new Node<T>(value));
    }
	
    private boolean add(Node<T> node) {
        if (getHead() == null) {
            setHead(node);
            setTail(node);
        } else {
            Node<T> prev = getTail();
            prev.setNext(node);
            node.setPrev(prev);
            setTail(node);
        }
        setSize(getSize() + 1);
        return true;
    }
    
    @Override
    public boolean remove(T value) {
        // Find the node
        Node<T> node = getHead();
        while (node != null && (!node.getValue().equals(value))) {
            node = node.getNext();
        }
        if (node == null)
            return false;

        // Update the tail, if needed
        if (node.equals(getTail()))
            setTail(node.getPrev());

        Node<T> prev = node.getPrev();
        Node<T> next = node.getNext();
        if (prev != null && next != null) {
            prev.setNext(next);
            next.setPrev(prev);
        } else if (prev != null && next == null) {
            prev.setNext(null);
        } else if (prev == null && next != null) {
            // Node is the head
            next.setPrev(null);
            setHead(next);
        } else {
            // prev==null && next==null
            setHead(null);
        }
        setSize(getSize() - 1);
        return true;
    }
    
    @Override
    public void clear() {
        setHead(null);
        setSize(0);
    }
    
    @Override
    public boolean contains(T value) {
        Node<T> node = getHead();
        while (node != null) {
            if (node.getValue().equals(value))
                return true;
            node = node.getNext();
        }
        return false;
    }
 
    @Override
    public int size() {
        return getSize();
    }
    
    @Override
    public List<T> toList() {
        return (new JavaCompatibleDoublyLinkedList<T>(this));
    }
    
    @Override
    public Collection<T> toCollection() {
        return (new JavaCompatibleDoublyLinkedList<T>(this));
    }

	public Node<T> getHead() {
		return head;
	}

	public void setHead(Node<T> head) {
		this.head = head;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Node<T> getTail() {
		return tail;
	}

	public void setTail(Node<T> tail) {
		this.tail = tail;
	}

}
