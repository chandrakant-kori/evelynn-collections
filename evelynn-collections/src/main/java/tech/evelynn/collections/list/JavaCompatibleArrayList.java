package tech.evelynn.collections.list;

@SuppressWarnings("unchecked")
public class JavaCompatibleArrayList<T> extends java.util.AbstractList<T> implements java.util.RandomAccess {

	private ArrayList<T> list = null;

	public JavaCompatibleArrayList(ArrayList<T> list) {
		this.list = list;
	}

	@Override
	public boolean add(T value) {
		return list.add(value);
	}

	@Override
	public void add(int index, T value) {
		list.add(index, value);
	}

	@Override
	public boolean contains(Object value) {
		return list.contains((T) value);
	}

	@Override
	public boolean remove(Object value) {
		return list.remove((T) value);
	}

	@Override
	public T remove(int index) {
		return list.remove(index);
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public T get(int index) {
		T t = list.get(index);
		if (t != null)
			return t;
		throw new IndexOutOfBoundsException();
	}

	@Override
	public T set(int index, T value) {
		return list.set(index, value);
	}

}