package tech.evelynn.collections.list.implicitkey;

public class ImplicitKeyNode<T> {
	private T value = null;
	private int priority;
	private int size;
	private ImplicitKeyNode<T> parent = null;
	private ImplicitKeyNode<T> left = null;
	private ImplicitKeyNode<T> right = null;

	public ImplicitKeyNode(T id) {
		this(null, id);
	}

	private ImplicitKeyNode(ImplicitKeyNode<T> parent, T id) {
		this.setParent(parent);
		this.setValue(id);
		this.size = 1;
		this.setPriority(ImplicitKeyTreap.RANDOM.nextInt(ImplicitKeyTreap.randomSeed));
	}

	public int getSize() {
		return size;
	}

	public void update() {
		size = 1 + (getLeft() != null ? ((ImplicitKeyNode<T>) getLeft()).size : 0)
				+ (getRight() != null ? ((ImplicitKeyNode<T>) getRight()).size : 0);
	}

	@Override
	public String toString() {
		return "id=" + getValue() + " parent=" + ((getParent() != null) ? getParent().getValue() : "NULL") + " left="
				+ ((getLeft() != null) ? getLeft().getValue() : "NULL") + " right=" + ((getRight() != null) ? getRight().getValue() : "NULL");
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public ImplicitKeyNode<T> getParent() {
		return parent;
	}

	public void setParent(ImplicitKeyNode<T> parent) {
		this.parent = parent;
	}

	public ImplicitKeyNode<T> getLeft() {
		return left;
	}

	public void setLeft(ImplicitKeyNode<T> left) {
		this.left = left;
	}

	public ImplicitKeyNode<T> getRight() {
		return right;
	}

	public void setRight(ImplicitKeyNode<T> right) {
		this.right = right;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}
}
