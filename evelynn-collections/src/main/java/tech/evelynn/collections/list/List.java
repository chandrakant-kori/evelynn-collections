package tech.evelynn.collections.list;

public interface List<T> {

	public boolean add(T value);

	public boolean remove(T value);

	public void clear();

	public boolean contains(T value);

	public int size();

	public java.util.List<T> toList();

	public java.util.Collection<T> toCollection();

}
