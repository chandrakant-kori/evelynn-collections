package tech.evelynn.collections.list;

import java.util.ListIterator;

@SuppressWarnings("unchecked")
public class JavaCompatibleDoublyLinkedList<T> extends java.util.AbstractSequentialList<T>  {

	private DoublyLinkedList<T> list = null;
	
	public JavaCompatibleDoublyLinkedList(DoublyLinkedList<T> list) {
        this.list = list;
    }
	
	@Override
    public boolean add(T value) {
        return list.add(value);
    }
	
	
	@Override
    public boolean remove(Object value) {
        return list.remove((T)value);
    }

	@Override
    public boolean contains(Object value) {
        return list.contains((T)value);
    }
	
	@Override
    public int size() {
        return list.size();
    }
	
	@Override
    public ListIterator<T> listIterator(int index) {
        return (new DoublyLinkedListListIterator<T>(list));
    }
	
}
