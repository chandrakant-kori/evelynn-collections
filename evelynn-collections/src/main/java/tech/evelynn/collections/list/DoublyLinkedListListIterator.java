package tech.evelynn.collections.list;

import java.util.ListIterator;

public class DoublyLinkedListListIterator<T> implements ListIterator<T> {

	private int index = 0;

	private DoublyLinkedList<T> list = null;
	private DoublyLinkedList.Node<T> prev = null;
	private DoublyLinkedList.Node<T> next = null;
	private DoublyLinkedList.Node<T> last = null;

	public DoublyLinkedListListIterator(DoublyLinkedList<T> list) {
		this.list = list;
		this.next = list.getHead();
		this.prev = null;
		this.last = null;
	}

	@Override
	public void add(T value) {
		DoublyLinkedList.Node<T> node = new DoublyLinkedList.Node<T>(value);
		DoublyLinkedList.Node<T> n = this.next;

		if (this.prev != null)
			this.prev.setNext(node);
		node.setPrev(this.prev);

		node.setNext(n);
		if (n != null)
			n.setPrev(node);

		this.next = node;
		if (this.prev == null)
			list.setHead(node); // new root
		list.setSize(list.getSize() + 1);
	}

	@Override
	public boolean hasNext() {
		return (next != null);
	}

	@Override
	public boolean hasPrevious() {
		return (prev != null);
	}

	@Override
	public T next() {
		if (next == null)
			throw new java.util.NoSuchElementException();

		index++;
		last = next;
		prev = next;
		next = next.getNext();

		return last.getValue();
	}

	@Override
	public int nextIndex() {
		return index;
	}

	@Override
	public T previous() {
		if (prev == null)
			throw new java.util.NoSuchElementException();

		index--;
		last = prev;
		next = prev;
		prev = next.getPrev();

		return last.getValue();
	}

	@Override
	public int previousIndex() {
		return index - 1;
	}

	@Override
	public void remove() {
		if (last == null)
			return;

		DoublyLinkedList.Node<T> p = last.getPrev();
		DoublyLinkedList.Node<T> n = last.getNext();
		if (p != null)
			p.setNext(n);
		if (n != null)
			n.setPrev(p);
		if (last.equals(list.getHead()))
			list.setHead(n);
		if (last.equals(list.getTail()))
			list.setTail(p);

		list.setSize(list.getSize() - 1);
	}

	@Override
	public void set(T value) {
		if (last != null)
			last.setValue(value);
	}

}
