/**
 * 
 */
package tech.evelynn.collections.heap;

import tech.evelynn.collections.list.ArrayList;
import tech.evelynn.collections.list.List;

/**
 * @author chand
 *
 */
public class BinaryHeapTreePrinter {

	public static <T extends Comparable<T>> void print(BinaryHeapTree<T> tree) {
        System.out.println(getString(tree.getRoot(), "", true));
    }

    public static <T extends Comparable<T>> String getString(BinaryHeapTree<T> tree) {
        if (tree.getRoot() == null)
            return "Tree has no nodes.";
        return getString(tree.getRoot(), "", true);
    }

    private static <T extends Comparable<T>> String getString(Node<T> node, String prefix, boolean isTail) {
        StringBuilder builder = new StringBuilder();

        builder.append(prefix + (isTail ? "└── " : "├── ") + node.getValue() + "\n");
        List<Node<T>> children = null;
        if (node.getLeft() != null || node.getRight() != null) {
            children = new ArrayList<Node<T>>(2);
            if (node.getLeft() != null)
                children.add(node.getLeft());
            if (node.getRight() != null)
                children.add(node.getRight());
        }
        if (children != null) {
            for (int i = 0; i < children.size() - 1; i++) {
                builder.append(getString(children.toList().get(i), prefix + (isTail ? "    " : "│   "), false));
            }
            if (children.size() >= 1) {
                builder.append(getString(children.toList().get(children.size() - 1),
                        prefix + (isTail ? "    " : "│   "), true));
            }
        }

        return builder.toString();
    }
}
