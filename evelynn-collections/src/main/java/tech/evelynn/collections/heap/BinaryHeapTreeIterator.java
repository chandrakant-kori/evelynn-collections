package tech.evelynn.collections.heap;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class BinaryHeapTreeIterator<C extends Comparable<C>> implements Iterator<C> {

	private BinaryHeapTree<C> heap = null;
    private Node<C> last = null;
    private Deque<Node<C>> toVisit = new ArrayDeque<Node<C>>();

    protected BinaryHeapTreeIterator(BinaryHeapTree<C> heap) {
        this.heap = heap;
        if (heap.getRoot()!=null) toVisit.add(heap.getRoot());
    }
    
    @Override
    public boolean hasNext() {
        if (toVisit.size()>0) return true; 
        return false;
    }
    
    @Override
    public C next() {
        while (toVisit.size()>0) {
            // Go thru the current nodes
            Node<C> n = toVisit.pop();

            // Add non-null children
            if (n.getLeft()!=null) toVisit.add(n.getLeft());
            if (n.getRight()!=null) toVisit.add(n.getRight());

            // Update last node (used in remove method)
            last = n;
            return n.getValue();
        }
        return null;
    }
    
    @Override
    public void remove() {
        heap.replaceNode(last);
    }

}
