package tech.evelynn.collections.heap;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;

/**
 * Heaps are usually implemented with an implicit heap data structure, which is
 * an implicit data structure consisting of an array (fixed size or dynamic
 * array) where each element represents a tree node whose parent/children
 * relationship is defined implicitly by their index. After an element is
 * inserted into or deleted from a heap, the heap property may be violated and
 * the heap must be balanced by swapping elements within the array.
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <T> Type of element going to be kept inside the
 *        {@link BinaryHeapArray}
 */
@SuppressWarnings("unchecked")
public class BinaryHeapArray<T extends Comparable<T>> implements BinaryHeap<T> {

	private static final int MINIMUM_SIZE = 1024;

	private Type type = Type.MIN;
	private int size = 0;
	private T[] array = (T[]) new Comparable[MINIMUM_SIZE];

	/**
	 * Returns the index of the parent node from the current node index using below
	 * equation.
	 * <p>
	 * <code>
	 * PARENT_INDEX = (CURRENT_INDEX - 1)/2; 
	 * <code> 
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(getParentIndex) = 1 + 1 = 2 = C = O(1)  
	 * </code>
	 * 
	 * @param index
	 * @return
	 */
	public static final int getParentIndex(int index) {
		if (index > 0)
			return (int) Math.floor((index - 1) / 2);
		return Integer.MIN_VALUE;
	}

	/**
	 * Returns the index of the left node for current node index using below
	 * equation.
	 * <p>
	 * <code>
	 * PARENT_INDEX = 2(CURRENT_INDEX) + 1; 
	 * <code> 	
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(getLeftIndex) = 1 + 1 = 2 = C = O(1)   
	 * </code>
	 * 
	 * @param index
	 * @return
	 */
	public static final int getLeftIndex(int index) {
		return 2 * index + 1;
	}

	/**
	 * Returns the index of the right node for current node index using below
	 * equation.
	 * <p>
	 * <code>
	 * PARENT_INDEX = 2(CURRENT_INDEX) + 2; 
	 * <code> 	
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(getRightIndex) = 1 + 1 = 2 = C = O(1)   
	 * </code>
	 * 
	 * @param index
	 * @return
	 */
	public static final int getRightIndex(int index) {
		return 2 * index + 2;
	}

	/**
	 * Default constructor by setting size to 0
	 */
	public BinaryHeapArray() {
		setSize(0);
	}

	/**
	 * Parameterized constructor to create the binary heap array
	 * 
	 * @param type input type of the binary heap {@link BinaryHeap.Type}
	 */
	public BinaryHeapArray(Type type) {
		this();
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tech.evelynn.collections.heap.Heap#size()
	 */
	@Override
	public int size() {
		return getSize();
	}

	/**
	 * Appends specific element to the heap. Insert operation is being perform using
	 * below algorithm.
	 * <p>
	 * <caption>ADD-LELEMENT-BINARY-HEAP-ARRAY(V)</caption>
	 * <ol>
	 * <li>Get the size of heap</li>
	 * <li>Check the size of heap if it exceeding the defined array length of heap
	 * then goto step - 3 otherwise step - 4</li>
	 * <li>Increase the array length using {@link #grow()} method</li>
	 * <li>Assign the {@code V} to the end of the heap</li>
	 * <li>Increase the size by 1</li>
	 * <li>heap-up to substitute the inserted element into heap</li>
	 * </ol>
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(getRightIndex) = 4 + O(log(n) 
	 * 					= O(log(n))
	 * </code>
	 * 
	 * @see tech.evelynn.collections.heap.Heap#add(java.lang.Object)
	 */
	@Override
	public boolean add(T value) {
		if (getSize() >= getArray().length)
			grow();
		getArray()[getSize()] = value;
		heapUp(size++);
		return true;
	}

	/**
	 * Removes specific element to the heap. Removal operation is being perform
	 * using below algorithm.
	 * <p>
	 * <caption>REMOVE-LELEMENT-BINARY-ARRAY-HEAP(V)</caption>
	 * <ol>
	 * <li>Check the length of the array of the heap</li>
	 * <li>Return <code>null</code> if array length is 0 (heap is empty)</li>
	 * <li>Find and remove the element using {@link #remove(int)} method</li>
	 * </ol>
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(getRightIndex) = 3 + 3n + O(log(n))
	 * 					= O(n log(n))
	 * </code>
	 * 
	 * @see tech.evelynn.collections.heap.Heap#remove(java.lang.Object)
	 */
	@Override
	public T remove(T value) {
		if (getArray().length == 0)
			return null;
		for (int i = 0; i < getSize(); i++) {
			T node = getArray()[i];
			if (node.equals(value))
				return remove(i);
		}
		return null;
	}

	/**
	 * Removes specific element to the heap from given index using below algorithm.
	 * <p>
	 * <caption>REMOVE-LELEMENT-BINARY-ARRAY-HEAP-INDEX(I)</caption>
	 * <ol>
	 * <li>Check the size of the heap</li>
	 * <li>If index is 0 or index is greater than size of the throw
	 * {@link NoSuchElementException} otherwise goto next step</li>
	 * <li>Get the element from the index</li>
	 * <li>Decrease the size of the heap</li>
	 * <li>Assign <code>null</code> value to the given index</li>
	 * <li>Heap-down to substitute the child element of the removed element from
	 * heap</li>
	 * <li>Check the size of the heap If size is the half or the array length then
	 * decrease the array length by using {@link #shrink()} method</li>
	 * <li>Return the removed element</li>
	 * </ol>
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(remove) = 4 + O(log(n)) + 2 + O(1) 
	 * 			 = 6 + O(log(n)) + O(1)
	 * 			 = C1 + O(log(n)) + C2
	 * 			 = O(log(n))
	 * </code>
	 * 
	 * @param index
	 * @return
	 */
	public T remove(int index) {
		if (index < 0 || index >= getSize())
			throw new NoSuchElementException();

		T t = getArray()[index];
		getArray()[index] = getArray()[setSize(getSize() - 1)];
		getArray()[getSize()] = null;

		heapDown(index);

		int shrinkSize = getArray().length >> 1;
		if (shrinkSize >= MINIMUM_SIZE && getSize() < shrinkSize)
			shrink();

		return t;
	}

	/**
	 * Heap up is the process which substitute the parent element of the current
	 * element in particular order(MIN/MAX).
	 * <p>
	 * <caption>HEAP-UP-BINARY-ARRAY-HEAP(I)</caption>
	 * <ol>
	 * <li>Check the value of the given INDEX if the value is null then return
	 * <code>null</code> otherwise store the element to check its value with its
	 * parent element</li>
	 * <li>Store index of the current element into NODE_INDEX</li>
	 * <li>Perform below steps 4 to 8 until NODE_INDEX reaches 0</li>
	 * <li>Get the PARENT_INDEX of the current element using
	 * {@link #getParentIndex(int)}</li>
	 * <li>If PARENT_INDEX is less then 0 return and do not perform any further
	 * steps</li>
	 * <li>Get the value of parent element from PARENT_INDEX</li>
	 * <li>If Heap type is {@link BinaryHeap.Type#MAX} and parent element value is
	 * lesser then current element or Heap type is {@link BinaryHeap.Type#MIN} and
	 * parent element value is greater then current element <code>swap</code> the
	 * current element value and it's parent element value.</li>
	 * <li>Assign PARENT_INDEX to the current NODE_INDEX and check the same scenario
	 * with it's own parent element</li>
	 * </ol>
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(heapUp) = 1 + 1 + 1 + 7log(n)
	 * 			 = 7log(n)+ 3 
	 * 			 = C1(log(n)) + C2
	 * 			 = O(log(n))
	 * </code>
	 * 
	 * @param idx
	 */
	protected void heapUp(int idx) {
		int nodeIndex = idx;
		T value = this.getArray()[nodeIndex];
		if (value == null)
			return;

		while (nodeIndex >= 0) {
			int parentIndex = getParentIndex(nodeIndex);
			if (parentIndex < 0)
				return;

			T parent = this.getArray()[parentIndex];

			if ((type == Type.MIN && value.compareTo(parent) < 0)
					|| (type == Type.MAX && value.compareTo(parent) > 0)) {
				// Node is greater/lesser than parent, switch node with parent
				this.getArray()[parentIndex] = value;
				this.getArray()[nodeIndex] = parent;
			} else {
				return;
			}
			nodeIndex = parentIndex;
		}
	}

	/**
	 * Heap down is the process which substitute the child element of the current
	 * element in particular order(MIN/MAX).
	 * <p>
	 * <caption>HEAP-DOWN-BINARY-ARRAY-HEAP(I)</caption>
	 * <ol>
	 * <li>Check the value of the given INDEX if the value is null then return
	 * <code>null</code> otherwise store the element to check its value with its
	 * parent element</li>
	 * <li>Store index of left child element of the current element into LEFT_INDEX
	 * using {@link #getLeftIndex(int)}</li>
	 * <li>Store index of right child element of the current element into
	 * RIGHT_INDEX using {@link #getRightIndex(int)}</li>
	 * <li>Store the value of the left child element and right child element for the
	 * Comparison with current.</li>
	 * <li>Check if both left and right child element is <code>null</code> then
	 * return and do not perform any steps</li>
	 * <li>Use below steps to find out while element should get swap with current
	 * parent element.</li>
	 * <li>Scenario 1 : If heap type is {@link BinaryHeap.Type#MIN}, both child are
	 * not <code>null</code> and value of both child is greater then current parent
	 * element</li>
	 * <li>Scenario 2 : If heap type is {@link BinaryHeap.Type#MAX}, both child are
	 * not <code>null</code> and value of both child is lesser than current parent
	 * element</li>
	 * <li>For scenario 1 and 2 perform below steps</li>
	 * <li>Scenario 1A : If heap type is {@link BinaryHeap.Type#MIN} and right child
	 * is greater then left child</li>
	 * <li>Scenario 2A : If heap type is {@link BinaryHeap.Type#MAX} and right child
	 * is lesser then left child</li>
	 * <li>For scenario 1A and 2A store value of the right child and index of the
	 * right child to swap with current parent element</li>
	 * <li>Scenario 1B : If heap type is {@link BinaryHeap.Type#MIN} and right child
	 * is lesser then left child</li>
	 * <li>Scenario 2B : If heap type is {@link BinaryHeap.Type#MAX} and right child
	 * is greater then left child</li>
	 * <li>For scenario 1B and 2B store value of the left child and index of the
	 * left child to swap with current parent element</li>
	 * <li>In case value of both child are same then we will consider right child to
	 * swap with the current parent node and will store value of the right child and
	 * index of the right child to swap with current parent element</li>
	 * <li>Scenario 3: If heap type is {@link BinaryHeap.Type#MIN} and right child
	 * is greater then current parent or If heap type is {@link BinaryHeap.Type#MAX}
	 * and right child is lesser then current parent</li>
	 * <li>For scenario 3 store value of the right child and index of the right
	 * child to swap with current parent element</li>
	 * <li>Scenario 4: If heap type is {@link BinaryHeap.Type#MIN} and left child is
	 * greater then current parent or If heap type is {@link BinaryHeap.Type#MAX}
	 * and left child is lesser then current parent</li>
	 * <li>For scenario 4 store value of the left child and index of the left child
	 * to swap with current parent element</li>
	 * <li>After checking all the scenario and In case we don't find any node to
	 * swap any left or right child element with current parent element then return
	 * and don't perform any steps.</li>
	 * <li>In case we get any node to swap it with current parent node then swap the
	 * nodes</li>
	 * <li>Do the same process for swapped node using recursion to check their own
	 * Child</li>
	 * </ol>
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(heapUp) = 22 * log(n)
	 * 			 = C1(log(n))
	 * 			 = O(log(n))
	 * </code>
	 * 
	 * @param index
	 */
	protected void heapDown(int index) {
		T value = this.getArray()[index];
		if (value == null)
			return;

		int leftIndex = getLeftIndex(index);
		int rightIndex = getRightIndex(index);
		T left = (leftIndex != Integer.MIN_VALUE && leftIndex < this.getSize()) ? this.getArray()[leftIndex] : null;
		T right = (rightIndex != Integer.MIN_VALUE && rightIndex < this.getSize()) ? this.getArray()[rightIndex] : null;

		if (left == null && right == null) {
			// Nothing to do here
			return;
		}

		T nodeToMove = null;
		int nodeToMoveIndex = -1;
		if ((type == Type.MIN && left != null && right != null && value.compareTo(left) > 0
				&& value.compareTo(right) > 0)
				|| (type == Type.MAX && left != null && right != null && value.compareTo(left) < 0
						&& value.compareTo(right) < 0)) {
			// Both children are greater/lesser than node
			if ((right != null) && ((type == Type.MIN && (right.compareTo(left) < 0))
					|| ((type == Type.MAX && right.compareTo(left) > 0)))) {
				// Right is greater/lesser than left
				nodeToMove = right;
				nodeToMoveIndex = rightIndex;
			} else if ((left != null) && ((type == Type.MIN && left.compareTo(right) < 0)
					|| (type == Type.MAX && left.compareTo(right) > 0))) {
				// Left is greater/lesser than right
				nodeToMove = left;
				nodeToMoveIndex = leftIndex;
			} else {
				// Both children are equal, use right
				nodeToMove = right;
				nodeToMoveIndex = rightIndex;
			}
		} else if ((type == Type.MIN && right != null && value.compareTo(right) > 0)
				|| (type == Type.MAX && right != null && value.compareTo(right) < 0)) {
			// Right is greater/lesser than node
			nodeToMove = right;
			nodeToMoveIndex = rightIndex;
		} else if ((type == Type.MIN && left != null && value.compareTo(left) > 0)
				|| (type == Type.MAX && left != null && value.compareTo(left) < 0)) {
			// Left is greater/lesser than node
			nodeToMove = left;
			nodeToMoveIndex = leftIndex;
		}
		// No node to move, stop recursion
		if (nodeToMove == null)
			return;

		// Re-factor heap sub-tree
		this.getArray()[nodeToMoveIndex] = value;
		this.getArray()[index] = nodeToMove;

		heapDown(nodeToMoveIndex);
	}

	/**
	 * Create new array with double size of current array copy all element from
	 * older array to new array
	 * 
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(grow) = 2 = C = O(1)
	 * </code>
	 * 
	 */
	private void grow() {
		int growSize = getSize() + (getSize() << 1);
		setArray(Arrays.copyOf(getArray(), growSize));
	}

	/**
	 * Create new array with half size of current array copy all element from older
	 * array to new array
	 * 
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(shrink) = 2 = C = O(1) 
	 * </code>
	 * 
	 */
	private void shrink() {
		int shrinkSize = getArray().length >> 1;
		setArray(Arrays.copyOf(getArray(), shrinkSize));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tech.evelynn.collections.heap.Heap#clear()
	 */
	@Override
	public void clear() {
		setSize(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tech.evelynn.collections.heap.Heap#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(T value) {
		if (getArray().length == 0)
			return false;
		for (int i = 0; i < getSize(); i++) {
			T t = getArray()[i];
			if (t.equals(value))
				return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tech.evelynn.collections.heap.BinaryHeap#getHeap()
	 */
	@Override
	public T[] getHeap() {
		T[] nodes = (T[]) new Comparable[getSize()];
		if (getArray().length == 0)
			return nodes;

		for (int i = 0; i < getSize(); i++) {
			T node = this.getArray()[i];
			nodes[i] = node;
		}
		return nodes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tech.evelynn.collections.heap.Heap#getHeadValue()
	 */
	@Override
	public T getHeadValue() {
		if (getSize() == 0 || getArray().length == 0)
			return null;
		return getArray()[0];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tech.evelynn.collections.heap.Heap#removeHead()
	 */
	@Override
	public T removeHead() {
		return remove(getHeadValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see tech.evelynn.collections.heap.Heap#toCollection()
	 */
	@Override
	public Collection<T> toCollection() {
		return (new JavaCompatibleBinaryHeapArray<T>(this));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return BinaryHeapArrayPrinter.getString(this);
	}

	public int getSize() {
		return size;
	}

	public int setSize(int size) {
		this.size = size;
		return size;
	}

	public T[] getArray() {
		return array;
	}

	public void setArray(T[] array) {
		this.array = array;
	}
}
