package tech.evelynn.collections.heap;

import java.util.AbstractCollection;

@SuppressWarnings("unchecked")
public class JavaCompatibleBinaryHeapArray<T extends Comparable<T>> extends AbstractCollection<T> {

	private BinaryHeapArray<T> heap = null;

	public JavaCompatibleBinaryHeapArray() {
		heap = new BinaryHeapArray<T>();
	}

	public JavaCompatibleBinaryHeapArray(BinaryHeapArray<T> heap) {
		this.heap = heap;
	}

	@Override
	public boolean add(T value) {
		return heap.add(value);
	}
	
	@Override
	public boolean remove(Object value) {
		return (heap.remove((T) value) != null);
	}

	@Override
	public boolean contains(Object value) {
		return heap.contains((T) value);
	}

	@Override
	public int size() {
		return heap.size();
	}

	@Override
	public java.util.Iterator<T> iterator() {
		return (new BinaryHeapArrayIterator<T>(this.heap));
	}

}
