package tech.evelynn.collections.heap;

/**
 * A delegate interface to provide binary heap based upon heap type. Heap is a
 * kind of binary tree with specific properties and limitation and this binary
 * tree can be programmed via nested linking nodes (Simply known as Tree) and
 * arrays.
 * <p>
 * Binary tree can be classified on the basis of the implementation and it is as
 * given below.
 * <ul>
 * <li>{@value BinaryHeap.Type#MIN}</li>
 * <li>{@value BinaryHeap.Type#MAX}</li>
 * </ul>
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <T> Type of Element going to be kept inside the heap
 */
public interface BinaryHeap<T extends Comparable<T>> extends Heap<T> {

	/**
	 * Binary tree classification based upon its implementation.
	 */
	public enum HeapType {
		Tree, Array
	}

	/**
	 * Binary tree classification based upon its storage type.
	 */
	public enum Type {
		MIN, MAX
	}

	/**
	 * Factory method to get the binary heap as per programmers need. Programmer can
	 * provide the {@link HeapType}(Tree/Array) and {@link Type}(Min/Max)
	 * 
	 * @param heapType Binary Heap implementation type
	 * @param type     Storage type of the heap.
	 * @return {@link BinaryHeapTree} Instance based upon the parameters
	 *         configuration, throw {@link RuntimeException} when parameters are not
	 *         configured properly
	 */
	public static <T extends Comparable<T>> BinaryHeap<T> getBinaryHeap(HeapType heapType, Type type) {
		switch (heapType) {
		case Tree:
			return new BinaryHeapTree<T>(type);
		case Array:
			return new BinaryHeapArray<T>(type);
		default:
			throw new RuntimeException();
		}
	}

	/**
	 * Returns the array transformation of current binary heap.
	 * 
	 * @return Array of the heap elements.
	 */
	public T[] getHeap();

}
