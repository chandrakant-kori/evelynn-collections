package tech.evelynn.collections.heap;

import java.util.Collection;

/**
 * Heap is a special case of balanced binary tree data structure where the
 * root-node key is compared with its children and arranged accordingly.
 * <p>
 * For children same case is being applied to arrange the data into the heap.
 * <p>
 * <table BORDER CELLPADDING=3 CELLSPACING=1> <caption> Summary of {@code Heap}
 * operations </caption>
 * <tr>
 * <td ALIGN=CENTER><em>Operation</em></td>
 * <td ALIGN=CENTER><em>Method Name</em></td>
 * </tr>
 * <tr>
 * <td><em>Insertion</em></td>
 * <td><em>{@link Heap#add(Object)}</em></td>
 * </tr>
 * <tr>
 * <td><em>Removal</em></td>
 * <td><em>{@link Heap#remove(Object)}</em></td>
 * </tr>
 * <tr>
 * <td><em>Examine Head</em></td>
 * <td><em>{@link Heap#getHeadValue()}</em></td>
 * </tr>
 * <tr>
 * <td><em>Head Removal</em></td>
 * <td><em>{@link Heap#removeHead()}</em></td>
 * </tr>
 * <tr>
 * <td><em>Existence</em></td>
 * <td><em>{@link Heap#contains(Object)}</em></td>
 * </tr>
 * </table>
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <T> Type of element going to be stored in the {@code Heap}.
 */
public interface Heap<T> {

	/**
	 * Appends specific element to the heap. Insert operation is being perform using
	 * below algorithm.
	 * <p>
	 * <caption>ADD-LELEMENT-HEAP(V)</caption>
	 * <ol>
	 * <li>Get the size of heap</li>
	 * <li>Assign the {@code V} to the end of the heap</li>
	 * <li>Increase the size by 1</li>
	 * <li>heap-up to substitute the inserted element into heap</li>
	 * </ol>
	 * 
	 * @param value Element to be inserted into the {@link Heap}
	 * @return <code>true</code> if case element is inserted successfully
	 */
	public boolean add(T value);

	/**
	 * Returns the <tt>Head</tt> of the heap. Either it could be maximum/minimum
	 * based upon the type of heap.
	 * 
	 * @return <tt>Head</tt> of the heap
	 */
	public T getHeadValue();

	/**
	 * Removes the <tt>Head</tt> of the heap, Head removal operation is being
	 * perform using below algorithm.
	 * <p>
	 * <caption>REMOVE-HEAD-LELEMENT-HEAP(V)</caption>
	 * <ol>
	 * <li>Use {@link #remove(Object)} method to remove the head</li>
	 * </ol>
	 * 
	 * @return <code>true</code> if case head is removed successfully
	 */
	public T removeHead();

	/**
	 * Removes specific element to the heap. Removal operation is being perform
	 * using below algorithm.
	 * <p>
	 * <caption>REMOVE-LELEMENT-HEAP(V)</caption>
	 * <ol>
	 * <li>Check the size of the heap</li>
	 * <li>Return <code>null</code> if size is 0 (heap is empty)</li>
	 * <li>Find and remove the element(Removal operation may be different for
	 * different type of heap)</li>
	 * <li>heap-down to balance the heap</li>
	 * </ol>
	 * 
	 * @param value Element to be remove from the heap.
	 * @return <code>true</code> if case Element is removed successfully
	 */
	public T remove(T value);

	/**
	 * Removes all of the elements from this list heap and assign <tt>Head</tt> of
	 * the heap along with <code>null</code> value.
	 */
	public void clear();

	/**
	 * Returns <tt>true</tt> if this heap contains the specified element.
	 * 
	 * @param value element whose presence in this heap is to be tested
	 * @return <tt>true</tt> if this heap contains the specified element
	 * 
	 * @throws NullPointerException if the specified element is null and this heap
	 *                              does not permit null elements
	 */
	public boolean contains(T value);

	/**
	 * Returns the number of elements in this heap. If this heap contains more than
	 * <tt>Integer.MAX_VALUE</tt> elements, returns <tt>Integer.MAX_VALUE</tt>.
	 *
	 * @return the number of elements in this heap
	 */
	public int size();

	/**
	 * Returns java compatible collection, which provide adaptability in between
	 * customized heap and JDK heap. It consider as bridge between <caption> evelynn
	 * collections <caption> and <caption> java collections <caption>
	 * 
	 * @return Java compatible collection
	 */
	public Collection<T> toCollection();

}
