package tech.evelynn.collections.heap;

import java.util.Iterator;

public class BinaryHeapArrayIterator<T extends Comparable<T>> implements Iterator<T> {

	private BinaryHeapArray<T> heap = null;
	private int last = -1;
	private int index = -1;

	protected BinaryHeapArrayIterator(BinaryHeapArray<T> heap) {
		this.heap = heap;
	}

	@Override
	public boolean hasNext() {
		if (index + 1 >= heap.getSize())
			return false;
		return (heap.getArray()[index + 1] != null);
	}

	@Override
	public T next() {
		if (++index >= heap.getSize())
			return null;
		last = index;
		return heap.getArray()[index];
	}

	@Override
	public void remove() {
		heap.remove(last);
	}
}
