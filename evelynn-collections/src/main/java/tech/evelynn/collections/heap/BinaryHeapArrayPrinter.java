package tech.evelynn.collections.heap;

import java.util.ArrayList;
import java.util.List;

public class BinaryHeapArrayPrinter {

	public static <T extends Comparable<T>> String getString(BinaryHeapArray<T> tree) {
        if (tree.getSize() == 0 || tree.getArray().length == 0)
            return "Tree has no nodes.";

        T root = tree.getArray()[0];
        if (root == null)
            return "Tree has no nodes.";
        return getString(tree, 0, "", true);
    }

	private static <T extends Comparable<T>> String getString(BinaryHeapArray<T> tree, int index, String prefix, boolean isTail) {
        StringBuilder builder = new StringBuilder();

        T value = tree.getArray()[index];
        builder.append(prefix + (isTail ? "└── " : "├── ") + value + "\n");
        List<Integer> children = null;
        int leftIndex = BinaryHeapArray.getLeftIndex(index);
        int rightIndex = BinaryHeapArray.getRightIndex(index);
        if (leftIndex != Integer.MIN_VALUE || rightIndex != Integer.MIN_VALUE) {
            children = new ArrayList<Integer>(2);
            if (leftIndex != Integer.MIN_VALUE && leftIndex < tree.getSize()) {
                children.add(leftIndex);
            }
            if (rightIndex != Integer.MIN_VALUE && rightIndex < tree.getSize()) {
                children.add(rightIndex);
            }
        }
        if (children != null) {
            for (int i = 0; i < children.size() - 1; i++) {
                builder.append(getString(tree, children.get(i), prefix + (isTail ? "    " : "│   "), false));
            }
            if (children.size() >= 1) {
                builder.append(getString(tree, children.get(children.size() - 1), prefix
                        + (isTail ? "    " : "│   "), true));
            }
        }

        return builder.toString();
    }
}
