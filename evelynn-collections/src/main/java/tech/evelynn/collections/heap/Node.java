package tech.evelynn.collections.heap;

public class Node<T extends Comparable<T>> {

	private T value = null;
	private Node<T> parent = null;
	private Node<T> left = null;
	private Node<T> right = null;

	public Node(Node<T> parent, T value) {
		this.setValue(value);
		this.setParent(parent);
	}

	@Override
	public String toString() {
		return "value=" + getValue() + " parent=" + ((getParent() != null) ? getParent().getValue() : "NULL") + " left="
				+ ((getLeft() != null) ? getLeft().getValue() : "NULL") + " right=" + ((getRight() != null) ? getRight().getValue() : "NULL");
	}

	public Node<T> getLeft() {
		return left;
	}

	public void setLeft(Node<T> left) {
		this.left = left;
	}

	public Node<T> getRight() {
		return right;
	}

	public void setRight(Node<T> right) {
		this.right = right;
	}

	public Node<T> getParent() {
		return parent;
	}

	public void setParent(Node<T> parent) {
		this.parent = parent;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}