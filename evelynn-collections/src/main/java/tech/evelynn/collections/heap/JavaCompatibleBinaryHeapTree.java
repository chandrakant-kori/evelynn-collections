package tech.evelynn.collections.heap;

import java.util.AbstractCollection;
import java.util.Iterator;

@SuppressWarnings("unchecked")
public class JavaCompatibleBinaryHeapTree<T extends Comparable<T>> extends AbstractCollection<T> {

	private BinaryHeapTree<T> heap = null;

    public JavaCompatibleBinaryHeapTree() {
        heap = new BinaryHeapTree<T>();
    }

    public JavaCompatibleBinaryHeapTree(BinaryHeapTree<T> heap) {
        this.heap = heap;
    }
    
    @Override
    public boolean add(T value) {
        return heap.add(value);
    }

    @Override
    public boolean remove(Object value) {
        return (heap.remove((T)value)!=null);
    }
    
	@Override
    public boolean contains(Object value) {
        return heap.contains((T)value);
    }

    @Override
    public int size() {
        return heap.size();
    }
    
    @Override
    public Iterator<T> iterator() {
        return (new BinaryHeapTreeIterator<T>(this.heap));
    }
}
