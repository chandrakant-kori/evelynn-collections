package tech.evelynn.collections.heap;

@SuppressWarnings("unchecked")
public class BinaryHeapTree<T extends Comparable<T>> implements BinaryHeap<T> {

	private Type type = Type.MIN;
	private int size = 0;
	private Node<T> root = null;

	public BinaryHeapTree() {
		setRoot(null);
		size = 0;
	}

	public BinaryHeapTree(Type type) {
		this();
		this.type = type;
	}

	@Override
	public int size() {
		return size;
	}

	private static int[] getDirections(int idx) {
		int index = idx;
		int directionsSize = (int) (Math.log10(index + 1) / Math.log10(2)) - 1;
		int[] directions = null;
		if (directionsSize > 0) {
			directions = new int[directionsSize];
			int i = directionsSize - 1;
			while (i >= 0) {
				index = (index - 1) / 2;
				directions[i--] = (index > 0 && index % 2 == 0) ? 1 : 0; // 0=left, 1=right
			}
		}
		return directions;
	}

	@Override
	public boolean add(T value) {
		return add(new Node<T>(null, value));
	}

	private boolean add(Node<T> newNode) {
		if (getRoot() == null) {
			setRoot(newNode);
			size++;
			return true;
		}

		Node<T> node = getRoot();
		int[] directions = getDirections(size); // size == index of new node
		if (directions != null && directions.length > 0) {
			for (int d : directions) {
				if (d == 0) {
					// Go left
					node = node.getLeft();
				} else {
					// Go right
					node = node.getRight();
				}
			}
		}
		if (node.getLeft() == null) {
			node.setLeft(newNode);
		} else {
			node.setRight(newNode);
		}
		newNode.setParent(node);

		size++;

		heapUp(newNode);

		return true;
	}
	
	private void removeRoot() {
        replaceNode(getRoot());
    }
	
	private Node<T> getLastNode() {
        // Find the last node
        int[] directions = getDirections(size-1); // Directions to the last node
        Node<T> lastNode = getRoot();
        if (directions != null && directions.length > 0) {
            for (int d : directions) {
                if (d == 0) {
                    // Go left
                    lastNode = lastNode.getLeft();
                } else {
                    // Go right
                    lastNode = lastNode.getRight();
                }
            }
        }
        if (lastNode.getRight() != null) {
            lastNode = lastNode.getRight();
        } else if (lastNode.getLeft() != null) {
            lastNode = lastNode.getLeft();
        }
        return lastNode;
    }
	
	public void replaceNode(Node<T> node) {
        Node<T> lastNode = getLastNode();

        // Remove lastNode from tree
        Node<T> lastNodeParent = lastNode.getParent();
        if (lastNodeParent!=null) {
            if (lastNodeParent.getRight() != null) {
                lastNodeParent.setRight(null);
            } else {
                lastNodeParent.setLeft(null);
            }
            lastNode.setParent(null);
        }

        if (node.getParent()!=null) {
            if (node.getParent().getLeft().equals(node)) {
                node.getParent().setLeft(lastNode);
            } else {
                node.getParent().setRight(lastNode);
            }
        }
        lastNode.setParent(node.getParent());

        lastNode.setLeft(node.getLeft());
        if (node.getLeft()!=null) node.getLeft().setParent(lastNode);

        lastNode.setRight(node.getRight());
        if (node.getRight()!=null) node.getRight().setParent(lastNode);

        if (node.equals(getRoot())) {
            if (!lastNode.equals(getRoot())) setRoot(lastNode);
            else setRoot(null);
        }

        size--;

        // Last node is the node to remove
        if (lastNode.equals(node)) return;

        if (lastNode.equals(getRoot())) {
            heapDown(lastNode);
        } else {
            heapDown(lastNode);
            heapUp(lastNode);
        }
    }

	private Node<T> getNode(Node<T> startingNode, T value) {
        Node<T> result = null;
        if (startingNode != null && startingNode.getValue().equals(value)) {
            result = startingNode;
        } else if (startingNode != null && !startingNode.getValue().equals(value)) {
            Node<T> left = startingNode.getLeft();
            Node<T> right = startingNode.getRight();
            if (left != null 
                && ((type==Type.MIN && left.getValue().compareTo(value)<=0)||(type==Type.MAX && left.getValue().compareTo(value)>=0))
            ) {
                result = getNode(left, value);
                if (result != null) return result;
            }
            if (right != null 
                && ((type==Type.MIN && right.getValue().compareTo(value)<=0)||(type==Type.MAX && right.getValue().compareTo(value)>=0))
            ) {
                result = getNode(right, value);
                if (result != null) return result;
            }
        }
        return result;
    }
	
	@Override
    public void clear() {
        setRoot(null);
        size = 0;
    }
	
	@Override
    public boolean contains(T value) {
        if (getRoot() == null) return false;
        Node<T> node = getNode(getRoot(),value);
        return (node != null);
    }
	
	@Override
    public T remove(T value) {
        if (getRoot() == null) return null;
        Node<T> node = getNode(getRoot(),value);
        if (node!=null) {
            T t = node.getValue();
            replaceNode(node);
            return t;
        }
        return null;
    }
	
	protected void heapUp(Node<T> nodeToHeapUp) {
        Node<T> node = nodeToHeapUp;
        while (node != null) {
            Node<T> heapNode = node;
            Node<T> parent = heapNode.getParent();

            if ((parent != null) && ((type == Type.MIN && node.getValue().compareTo(parent.getValue()) < 0) || (type == Type.MAX && node.getValue().compareTo(parent.getValue()) > 0))) {
                // Node is less than parent, switch node with parent
                Node<T> grandParent = parent.getParent();
                Node<T> parentLeft = parent.getLeft();
                Node<T> parentRight = parent.getRight();

                parent.setLeft(heapNode.getLeft());
                if (parent.getLeft() != null) parent.getLeft().setParent(parent);
                parent.setRight(heapNode.getRight());
                if (parent.getRight() != null) parent.getRight().setParent(parent);

                if (parentLeft != null && parentLeft.equals(node)) {
                    heapNode.setLeft(parent);
                    heapNode.setRight(parentRight);
                    if (parentRight != null) parentRight.setParent(heapNode);
                } else {
                    heapNode.setRight(parent);
                    heapNode.setLeft(parentLeft);
                    if (parentLeft != null) parentLeft.setParent(heapNode);
                }
                parent.setParent(heapNode);

                if (grandParent == null) {
                    // New root.
                    heapNode.setParent(null);
                    setRoot(heapNode);
                } else {
                    Node<T> grandLeft = grandParent.getLeft();
                    if (grandLeft != null && grandLeft.equals(parent)) {
                        grandParent.setLeft(heapNode);
                    } else {
                        grandParent.setRight(heapNode);
                    }
                    heapNode.setParent(grandParent);
                }
            } else {
                node = heapNode.getParent();
            }
        }
    }
	
	protected void heapDown(Node<T> nodeToHeapDown) {
        if (nodeToHeapDown==null) return;

        Node<T> node = nodeToHeapDown;
        Node<T> heapNode = node;
        Node<T> left = heapNode.getLeft();
        Node<T> right = heapNode.getRight();

        if (left == null && right == null) {
            // Nothing to do here
            return;
        }

        Node<T> nodeToMove = null;

        if ((left != null && right != null ) &&
            ((type == Type.MIN && node.getValue().compareTo(left.getValue()) > 0 && node.getValue().compareTo(right.getValue()) > 0)
            || (type == Type.MAX && node.getValue().compareTo(left.getValue()) < 0 && node.getValue().compareTo(right.getValue()) < 0))
         ) {
            // Both children are greater/lesser than node
            if ((type == Type.MIN && right.getValue().compareTo(left.getValue()) < 0) || (type == Type.MAX && right.getValue().compareTo(left.getValue()) > 0)) {
                // Right is greater/lesser than left
                nodeToMove = right;
            } else if ((type == Type.MIN && left.getValue().compareTo(right.getValue()) < 0) || (type == Type.MAX && left.getValue().compareTo(right.getValue()) > 0)) {
                // Left is greater/lesser than right
                nodeToMove = left;
            } else {
                // Both children are equal, use right
                nodeToMove = right;
            }
        } else if ((type == Type.MIN && right != null && node.getValue().compareTo(right.getValue()) > 0)
                   || (type == Type.MAX && right != null && node.getValue().compareTo(right.getValue()) < 0)) {
            // Right is greater than node
            nodeToMove = right;
        } else if ((type == Type.MIN && left != null && node.getValue().compareTo(left.getValue()) > 0)
                   || (type == Type.MAX && left != null && node.getValue().compareTo(left.getValue()) < 0)) {
            // Left is greater than node
            nodeToMove = left;
        }
        // No node to move, stop recursion
        if (nodeToMove == null) return;

        // Re-factor heap sub-tree
        Node<T> nodeParent = heapNode.getParent();
        if (nodeParent == null) {
            // heap down the root
            setRoot(nodeToMove);
            getRoot().setParent(null);
        } else {
            if (nodeParent.getLeft()!=null && nodeParent.getLeft().equals(node)) {
                // heap down a left
                nodeParent.setLeft(nodeToMove);
                nodeToMove.setParent(nodeParent);
            } else {
                // heap down a right
                nodeParent.setRight(nodeToMove);
                nodeToMove.setParent(nodeParent);
            }
        }

        Node<T> nodeLeft = heapNode.getLeft();
        Node<T> nodeRight = heapNode.getRight();
        Node<T> nodeToMoveLeft = nodeToMove.getLeft();
        Node<T> nodeToMoveRight = nodeToMove.getRight();
        if (nodeLeft!=null && nodeLeft.equals(nodeToMove)) {
            nodeToMove.setRight(nodeRight);
            if (nodeRight != null) nodeRight.setParent(nodeToMove);

            nodeToMove.setLeft(heapNode);
        } else {
            nodeToMove.setLeft(nodeLeft);
            if (nodeLeft != null) nodeLeft.setParent(nodeToMove);

            nodeToMove.setRight(heapNode);
        }
        heapNode.setParent(nodeToMove);

        heapNode.setLeft(nodeToMoveLeft);
        if (nodeToMoveLeft != null) nodeToMoveLeft.setParent(heapNode);

        heapNode.setRight(nodeToMoveRight);
        if (nodeToMoveRight != null) nodeToMoveRight.setParent(heapNode);

        heapDown(node);
    }

	private void getNodeValue(Node<T> node, int idx, T[] array) {
        int index = idx;
        array[index] = node.getValue();
        index = (index * 2) + 1;

        Node<T> left = node.getLeft();
        if (left != null)
            getNodeValue(left, index, array);
        Node<T> right = node.getRight();
        if (right != null)
            getNodeValue(right, index + 1, array);
    }
	
	@Override
    public T[] getHeap() {
		T[] nodes = (T[]) new Comparable[size];
        if (getRoot() != null)
            getNodeValue(getRoot(), 0, nodes);
        return nodes;
    }
	
	@Override
    public T getHeadValue() {
        T result = null;
        if (getRoot() != null)
            result = getRoot().getValue();
        return result;
    }
	
	@Override
    public T removeHead() {
        T result = null;
        if (getRoot() != null) {
            result = getRoot().getValue();
            removeRoot();
        }
        return result;
    }

	@Override
    public java.util.Collection<T> toCollection() {
        return (new JavaCompatibleBinaryHeapTree<T>(this));
    }
	
	@Override
    public String toString() {
        return BinaryHeapTreePrinter.getString(this);
    }

	public Node<T> getRoot() {
		return root;
	}

	public void setRoot(Node<T> root) {
		this.root = root;
	}
	
	
}
