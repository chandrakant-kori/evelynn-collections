package tech.evelynn.collections.misc;

import java.util.ArrayList;
import java.util.List;

public class LCANode<T> {

	private final List<LCANode<T>> ancestors = new ArrayList<LCANode<T>>();
	private final List<LCANode<T>> children = new ArrayList<LCANode<T>>();

	private T value = null;
	private int depth = 0;

	/**
	 * Creates tree with root only.
	 *
	 */
	public LCANode() {
	}

	/**
	 * Creates tree with root (storing value) only.
	 *
	 * @param value value to be stored in root
	 */
	public LCANode(T value) {
		this.value = value;
	}

	private LCANode(LCANode<T> parent) {
		parent.children.add(this);
		this.getAncestors().add(parent);
		this.setDepth(parent.getDepth() + 1);
		int dist = 0;
		while (true) {
			try {
				this.getAncestors().add(this.getAncestors().get(dist).getAncestors().get(dist));
				dist++;
			} catch (Exception e) {
				break;
			}
		}
	}

	public LCANode<T> setValue(T value) {
		this.value = value;
		return this;
	}

	/**
	 * Creates new child for this node and returns it.
	 *
	 * Complexity O(log depth)
	 *
	 * @return added child
	 */
	public LCANode<T> addChild() {
		return new LCANode<T>(this);
	}

	/**
	 * Creates new child (storing value) for this node and returns it.
	 *
	 * Complexity O(log depth)
	 *
	 * @param value value to be stored in new child
	 * @return added child
	 */
	public LCANode<T> addChild(T value) {
		return addChild().setValue(value);
	}

	/**
	 * Returns value stored in node.
	 *
	 * @return node's value.
	 */
	public T getValue() {
		return value;
	}

	/**
	 * Finds subtree with given value in the root.
	 *
	 * @param value value to be find
	 * @return subtree with given value in the root
	 */
	public LCANode<T> find(T value) {
		if (this.value == null) {
			if (value == null)
				return this;
		} else if (this.value.equals(value))
			return this;
		for (LCANode<T> child : children) {
			final LCANode<T> res = child.find(value);
			if (res != null)
				return res;
		}
		return null;
	}

	/**
	 * Returns true if tree contains a node with given value
	 *
	 * @param value to be checked
	 * @return true if tree contains node with given value, false otherwise
	 */
	public boolean contains(T value) {
		return find(value) != null;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public List<LCANode<T>> getAncestors() {
		return ancestors;
	}
}