package tech.evelynn.collections.misc;

public class LowestCommonAncestor<T> {

	public static <S> LCANode<S> lowestCommonAncestor(LCANode<S> node1, LCANode<S> node2)
			throws LCANodesNotInSameTreeException {
		if (node1 == node2)
			return node1;
		else if (node1.getDepth() < node2.getDepth())
			return lowestCommonAncestor(node2, node1);
		else if (node1.getDepth() > node2.getDepth()) {
			int diff = node1.getDepth() - node2.getDepth();
			int jump = 0;
			while (diff > 0) {
				if (diff % 2 == 1)
					node1 = node1.getAncestors().get(jump);
				jump++;
				diff /= 2;
			}
			return lowestCommonAncestor(node1, node2);
		} else {
			try {
				int step = 0;
				while (1 << (step + 1) <= node1.getDepth())
					step++;
				while (step >= 0) {
					if (step < node1.getAncestors().size() && node1.getAncestors().get(step) != node2.getAncestors().get(step)) {
						node1 = node1.getAncestors().get(step);
						node2 = node2.getAncestors().get(step);
					}
					step--;
				}
				return node1.getAncestors().get(0);
			} catch (Exception e) {
				throw new LCANodesNotInSameTreeException();
			}

		}
	}

}
