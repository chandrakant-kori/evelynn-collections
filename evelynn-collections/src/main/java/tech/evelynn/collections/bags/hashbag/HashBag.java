/**
 * 
 */
package tech.evelynn.collections.bags.hashbag;

import java.util.NoSuchElementException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BinaryOperator;

import tech.evelynn.collections.bags.Bag;
import tech.evelynn.collections.map.LinearProbingHashMap;
import tech.evelynn.collections.map.Pair;

/**
 * {@link HashBag} is the enhanced version of the {@link Bag} where it provides
 * the capability to mutate the item counts inside the bag. It uses
 * {@link LinearProbingHashMap} internally to store the items into the bag.
 * 
 * Insertion and removal operations are thread-safe here where it uses
 * re-enterant lock to provide the thread safety.<br>
 * <br>
 * <table BORDER CELLPADDING=3 CELLSPACING=1> <caption> Summary of
 * {@code HashBag} operations </caption>
 * <tr>
 * <td ALIGN=CENTER><em>Operation</em></td>
 * <td ALIGN=CENTER><em>Method Name</em></td>
 * <td ALIGN=CENTER><em>Time Complexity</em></td>
 * </tr>
 * <tr>
 * <td><em>Insertion</em></td>
 * <td><em>{@link HashBag#addOccurrences(Object)}</em></td>
 * <td ALIGN=CENTER><em>O(1)</em></td>
 * </tr>
 * <tr>
 * <td><em>Removal</em></td>
 * <td><em>{@link HashBag#removeOccurrences(Object)}</em></td>
 * <td ALIGN=CENTER><em>O(1)</em></td>
 * </tr>
 * <tr>
 * <td><em>Examine</em></td>
 * <td><em>{@link HashBag#occurrencesOf(Object)}</em></td>
 * <td ALIGN=CENTER><em>O(1)</em></td>
 * </tr>
 * <tr>
 * <td><em>Existence</em></td>
 * <td><em>{@link HashBag#contains(Object)}</em></td>
 * <td ALIGN=CENTER><em>O(1)</em></td>
 * </tr>
 * <tr>
 * <td><em>Maximum occurrences</em></td>
 * <td><em>{@link HashBag#topOccurrences()}</em></td>
 * <td ALIGN=CENTER><em>O(n)</em></td>
 * </tr>
 * <tr>
 * <td><em>Minmum occurrences</em></td>
 * <td><em>{@link HashBag#leastOccurrences()}</em></td>
 * <td ALIGN=CENTER><em>O(n)</em></td>
 * </tr>
 * </table>
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 * @version 1.0
 *
 */
public class HashBag<K> {

	private LinearProbingHashMap<K, Integer> hashBag = new LinearProbingHashMap<>();
	private Lock lock = new ReentrantLock();

	/**
	 * Used to check the count of the item present in the {@link HashBag}. In case
	 * item doesn't present in the bag it returns 0.
	 * <p>
	 * <code>
	 * Time Complexity  
	 * T(occurrencesOf) =  1 + 1 = 2 = C = O(1)
	 * </code>
	 * 
	 * @param key item going to check the occurrences
	 * @return number of occurrences
	 */
	public Integer occurrencesOf(K key) {
		if (hashBag.contains(key))
			return hashBag.get(key);
		return 0;
	}

	/**
	 * Performs insert operation in the {@link HashBag}. When given item is present
	 * in the {@link HashBag} it will increase the count of the item by 1, otherwise
	 * it will insert the item in the bag with initial count of 1. <br>
	 * <br>
	 * Insert operation acquires lock on the {@link HashBag} while performing
	 * insertion and releases lock once the operation is completed.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(addOccurrences) =  5 = C = O(1) 
	 * </code>
	 * 
	 * @param key item for the insertion
	 * @return count of the item post insertion
	 */
	public Integer addOccurrences(K key) {
		lock.lock();
		if (hashBag.contains(key))
			return hashBag.put(key, hashBag.get(key) + 1);
		hashBag.put(key, 1);
		lock.unlock();
		return 1;
	}

	/**
	 * Performs removal operation in the {@link HashBag}. When given item is present
	 * in the {@link HashBag} it will decreases the count of the item by 1,
	 * otherwise it throws {@link NoSuchElementException}
	 * <p>
	 * Remove operation acquires lock on the {@link HashBag} while performing
	 * removal and releases lock once the operation is completed.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(removeOccurrences) =  5 = C = O(1) 
	 * </code>
	 * 
	 * @param key item for the removal
	 * @return count of the item post removal
	 */
	public Integer removeOccurrences(K key) {
		lock.lock();
		if (hashBag.contains(key)) {
			if (hashBag.get(key) > 1)
				return hashBag.put(key, hashBag.get(key) - 1);
			hashBag.remove(key);
		}
		lock.lock();
		throw new NoSuchElementException();
	}

	/**
	 * It uses {@link HashBag#aggregate(Pair[], int, int, BinaryOperator)} function
	 * to find the item having maximum count value.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(topOccurrences)=  1 + T(aggregate) 
	 * 					=  C0 + C1(n) + C2
	 * 					=  O(n)
	 * </code>
	 * 
	 * @return item having maximum count value.
	 */
	public K topOccurrences() {
		BinaryOperator<Pair<K, Integer>> max = (x, y) -> x.getValue() > y.getValue() ? x : y;
		return aggregate(hashBag.getArray(), 0, hashBag.getArray().length - 1, max).getKey();
	}

	/**
	 * It uses {@link HashBag#aggregate(Pair[], int, int, BinaryOperator)} function
	 * to find the item having minimum count value.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(topOccurrences)=  1 + T(aggregate) 
	 * = C0 + C1(n) + C2
	 * = O(n)
	 * </code>
	 * 
	 * @return item having minimun count value.
	 */
	public K leastOccurrences() {
		BinaryOperator<Pair<K, Integer>> min = (x, y) -> x.getValue() < y.getValue() ? x : y;
		return aggregate(hashBag.getArray(), 0, hashBag.getArray().length - 1, min).getKey();
	}

	/**
	 * A common aggregate function which used to run with divide and conquer
	 * approach to execute any aggregate function on any given array.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * 	T(aggregate)= 1 + 1 + T(n/2) + T(n/2) + 1  + 1 
	 *  			= 2 * T(n/2) + 4 
	 *  			= T(n) + 4
	 * 	T(aggregate)=  C1(n) + C2
	 * 	T(aggregate)=  O(n)
	 * </code>
	 * 
	 * @param array Input array to perform aggregate function.
	 * @param low   starting index of the array
	 * @param high  ending index of the array
	 * @param op    aggregate function {@link BinaryOperator}
	 * @return result
	 */
	public Pair<K, Integer> aggregate(Pair<K, Integer>[] array, int low, int high,
			BinaryOperator<Pair<K, Integer>> op) {
		if (low == high)
			return array[low];
		int mid = (low + high) / 2;
		Pair<K, Integer> x = aggregate(array, low, mid, op);
		Pair<K, Integer> y = aggregate(array, mid + 1, high, op);
		return op.apply(x, y);
	}

	/**
	 * Used to print the summary of {@link HashBag}. In the summary it shows item
	 * with count.
	 * <p>
	 * <code>
	 * Time Complexity  
	 * T(Summary) = 1 + 1 + 1 + n + 1
	 * 			  = n + 4 
	 * 			  = n + C 
	 * 			  = O(n)
	 * </code>
	 * 
	 */
	public void Summary() {
		StringBuilder builder = new StringBuilder();
		builder.append("Statistics of Bag").append(System.lineSeparator());
		builder.append("-----------------").append(System.lineSeparator());
		hashBag.toMap().forEach(
				(key, value) -> builder.append(key.toString() + " -> " + value).append(System.lineSeparator()));
		builder.append("-----------------").append(System.lineSeparator());
	}

	/**
	 * Used to clear the {@link HashBag}
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(clear) =  1 = C = O(1)
	 * </code>
	 * 
	 */
	public void clear() {
		hashBag.clear();
	}

	/**
	 * Used to check the availability of the item inside the {@link HashBag}.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(contains) = 1 = C = O(1)
	 * </code>
	 * 
	 * @param key item going to be checked for availability.
	 * @return existance of the item.
	 */
	public boolean contains(K key) {
		return hashBag.contains(key);
	}

	/**
	 * This method is used to get the number of items inside the {@link HashBag}.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(size) = 1 = C = O(1)
	 * </code>
	 * 
	 * @return size of the {@link HashBag}
	 */
	public int size() {
		return hashBag.size();
	}

}
