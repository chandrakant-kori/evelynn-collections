package tech.evelynn.collections.bags;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator for bag that allows the programmer to traverse the bag item in
 * forward direction and obtain the iterator's current position in the list. A
 * {@code BagIterator} has no current element; An iterator for a bag of length
 * {@code n} has {@code n+1} possible cursor positions, as illustrated by the
 * carets ({@code ^}) below:
 * 
 * <PRE>
 *                     Element(0)   Element(1)   Element(2)   ... Element(n-1)
 * cursor positions:  ^          ^            ^            ^                  ^
 * </PRE>
 * 
 * <table BORDER CELLPADDING=3 CELLSPACING=1><caption> Summary of Bag Iterator
 * operations </caption>
 * <tr>
 * <td ALIGN=CENTER><em>Operation</em></td>
 * <td ALIGN=CENTER><em>Method Name</em></td>
 * <td ALIGN=CENTER><em>Time Complexity</em></td>
 * </tr>
 * <tr>
 * <td><em>Cursor check for remaining items</em></td>
 * <td><em>{@link BagIterator#hasNext()}</em></td>
 * <td ALIGN=CENTER><em>O(1)</em></td>
 * </tr>
 * <tr>
 * <td><em>Move cursor to the next item</em></td>
 * <td><em>{@link BagIterator#next()}</em></td>
 * <td ALIGN=CENTER><em>O(1)</em></td>
 * </tr>
 * <tr>
 * <td><em>Removal</em></td>
 * <td><em>Not Allowed</em></td>
 * <td ALIGN=CENTER><em>NA</em></td>
 * </tr>
 * </table>
 * <br>
 * Note that the {@link #remove} operation isn't allowed while iterating the
 * bag. <br>
 * <br>
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 * @version 1.0
 * @param <E> Type of element going iterate.
 */
public class BagIterator<E> implements Iterator<E> {

	private BagNode<E> currentElement;

	/**
	 * {@link Bag} constructor sets default values as given below <br>
	 * <br>
	 * <table BORDER CELLPADDING=3 CELLSPACING=1> <caption> Initial status of the
	 * bag iterator</caption>
	 * <tr>
	 * <td ALIGN=CENTER><em>Name</em></td>
	 * <td ALIGN=CENTER><em>Initial value</em></td>
	 * </tr>
	 * <tr>
	 * <td><em>{@link BagIterator#currentElement}</em></td>
	 * <td><em><code>element from the parameter</code></em></td>
	 * </tr>
	 * </table>
	 * 
	 * @param firstElement first element of the {@link Bag}
	 */
	public BagIterator(BagNode<E> firstElement) {
		currentElement = firstElement;
	}

	/**
	 * This method is being used to check the is there any element remaining while
	 * iterating through cursor.
	 * <p>
	 * <code> Time Complexity T(hasNext) = 1 = C = O(1) <code>
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	public boolean hasNext() {
		return currentElement != null;
	}

	/**
	 * Remove operation isn't allowed in bag, Hence it will throw
	 * {@link UnsupportedOperationException}
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(remove) = 1 = C = O(1)
	 * </code>
	 * 
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

	/**
	 * This method checks the remaining elements through cursor. In case no elements
	 * are there to iterate then it throws {@link NoSuchElementException} otherwise
	 * it moves cursor to the next item of bag.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(next) = 1 + 1 + 1 + 1 = 4 = C = O(1)
	 * </code>
	 * 
	 * @see java.util.Iterator#next()
	 */
	@Override
	public E next() {
		if (!hasNext())
			throw new NoSuchElementException();
		E element = currentElement.getContent();
		currentElement = currentElement.getNextElement();
		return element;
	}
}
