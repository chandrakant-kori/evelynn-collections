package tech.evelynn.collections.bags;

import java.util.Iterator;

/**
 * A Bag is similar to your shopping bag or grocery bag, wherein you can have
 * one or more occurrences or a particular item. In the bag, order is being
 * maintained and it can be iterated as per insertion order. A bag is immutable
 * always. <br>
 * <br>
 * <table BORDER CELLPADDING=3 CELLSPACING=1> <caption> Summary of Bag
 * operations </caption>
 * <tr>
 * <td ALIGN=CENTER><em>Operation</em></td>
 * <td ALIGN=CENTER><em>Method Name</em></td>
 * <td ALIGN=CENTER><em>Time Complexity</em></td>
 * </tr>
 * <tr>
 * <td><em>Insertion</em></td>
 * <td><em>{@link Bag#add(Object)}</em></td>
 * <td ALIGN=CENTER><em>O(1)</em></td>
 * </tr>
 * <tr>
 * <td><em>Removal</em></td>
 * <td><em>Not Allowed</em></td>
 * <td ALIGN=CENTER><em>NA</em></td>
 * </tr>
 * <tr>
 * <td><em>Examine</em></td>
 * <td><em>Not Allowed</em></td>
 * <td ALIGN=CENTER><em>NA</em></td>
 * </tr>
 * <tr>
 * <td><em>Existence</em></td>
 * <td><em>{@link Bag#contains(Object)}</em></td>
 * <td ALIGN=CENTER><em>O(n)</em></td>
 * </tr>
 * </table>
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 * @version 1.0
 * @param <E> Type of element going to be kept inside the bag.
 * 
 */
public class Bag<E> implements Iterable<E> {

	private BagNode<E> firstElement; // first element of the bag
	private int size; // size of bag

	/**
	 * {@link Bag} constructor sets default values as given below <br>
	 * <br>
	 * <table BORDER CELLPADDING=3 CELLSPACING=1> <caption> Initial status of the
	 * bag</caption>
	 * <tr>
	 * <td ALIGN=CENTER><em>Name</em></td>
	 * <td ALIGN=CENTER><em>Initial value</em></td>
	 * </tr>
	 * <tr>
	 * <td><em>{@link Bag#firstElement}</em></td>
	 * <td><em><code>null</code></em></td>
	 * </tr>
	 * <tr>
	 * <td><em>{@link Bag#size}</em></td>
	 * <td><em><code>0</code></em></td>
	 * </tr>
	 * </table>
	 * 
	 */
	public Bag() {
		firstElement = null;
		size = 0;
	}

	/**
	 * This method is being used to check the status of bag.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(isEmpty) = 1 = C = O(1)
	 * </code>
	 * 
	 * @return either bag is empty or not empty.
	 */
	public boolean isEmpty() {
		return firstElement == null;
	}

	/**
	 * This method is used to get the number of items inside the bag.
	 * <p>
	 * <code> Time Complexity T(size) = 1 = C = O(1)
	 * </pre>
	 * 
	 * @return size of the bag
	 */
	public int size() {
		return size;
	}

	/**
	 * This method is used to add the item inside the bag.
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(add) = 1 + 1 + 1 + 1 + 1 = 5 = C = O(1)
	 * </code>
	 * 
	 * @param element added in the bag
	 */
	public void add(E element) {
		BagNode<E> oldfirst = firstElement;
		firstElement = new BagNode<>();
		firstElement.setContent(element);
		firstElement.setNextElement(oldfirst);
		size++;
	}

	/**
	 * This method is used to check the availability of the item inside the bag.
	 * <p>
	 * <code>
	 * Time Complexity 
	 * T(contains) = 1 + 1*(n+1) + 1*(n) + 1 = 2(n) + 3 = C1(n) + C2 = O(n)
	 * </code>
	 * 
	 * @param element going to be checked for availability
	 * @return existence of the item
	 */
	public boolean contains(E element) {
		Iterator<E> iterator = this.iterator();
		while (iterator.hasNext()) {
			if (iterator.next().equals(element)) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	public Iterator<E> iterator() {
		return new BagIterator<E>(firstElement);
	}

}
