package tech.evelynn.collections.bags;

/**
 * Bag Node that stores items
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <E> Type of element going to be stored within {@link BagNode}
 */
public class BagNode<E> {

	private E content;
	private BagNode<E> nextElement;

	public E getContent() {
		return content;
	}

	public void setContent(E content) {
		this.content = content;
	}

	public BagNode<E> getNextElement() {
		return nextElement;
	}

	public void setNextElement(BagNode<E> nextElement) {
		this.nextElement = nextElement;
	}

}
