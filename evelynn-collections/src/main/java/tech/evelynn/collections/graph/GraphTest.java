package tech.evelynn.collections.graph;

import java.util.Arrays;

public class GraphTest {

	public static void main(String[] args) {

		Vertex<Integer> node1 = new Vertex<>(10,4);
		Vertex<Integer> node2 = new Vertex<>(20,7);
		Vertex<Integer> node3 = new Vertex<>(30,6);
		Vertex<Integer> node4 = new Vertex<>(40,10);

		Edge<Integer> edge1 = new Edge<>(12, node1, node2);
		Edge<Integer> edge2 = new Edge<>(5, node1, node3);
		Edge<Integer> edge3 = new Edge<>(24, node2, node3);
		Edge<Integer> edge4 = new Edge<>(40, node3, node4);
		Edge<Integer> edge5 = new Edge<>(45, node3, node4);

		Graph<Integer> graph = new Graph<>(Graph.TYPE.DIRECTED,Arrays.asList(node1, node2, node3, node4),Arrays.asList(edge1, edge2, edge3, edge4, edge5));
		System.out.println(graph);
	}
}
