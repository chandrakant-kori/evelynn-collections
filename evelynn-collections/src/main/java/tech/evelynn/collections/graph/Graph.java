package tech.evelynn.collections.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * A graph is a pair G = (V, E), where V is a set whose elements are called
 * vertices (singular: {@code Vertex}), and E is a set of paired vertices, whose
 * elements are called {@code Edge}
 * <p>
 * The vertices x and y of an edge {x, y} are called the end-points of the edge.
 * The edge is said to join x and y and to be incident on x and y. A vertex may
 * belong to no edge, in which case it is not joined to any other vertex.
 * <p>
 * Programmer can create below type of {@code Graph}
 * <ul>
 * <li>{@code Graph.TYPE#DIRECTED}</li>
 * <li>{@code Graph.TYPE#UNDIRECTED}</li>
 * </ul>
 * <br>
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <T> Type of element going to be kept in vertices of graph
 */
@SuppressWarnings("unchecked")
public class Graph<T extends Comparable<T>> {

	private List<Vertex<T>> allVertices = new ArrayList<Vertex<T>>();
	private List<Edge<T>> allEdges = new ArrayList<Edge<T>>();

	public enum TYPE {
		DIRECTED, UNDIRECTED
	}

	/** Defaulted to undirected */
	private TYPE type = TYPE.UNDIRECTED;

	public Graph() {
	}

	/**
	 * Parameterized constructor to provide {@code Graph.TYPE} while creating graph
	 * 
	 * @param type {@code Type} of Graph
	 */
	public Graph(TYPE type) {
		this.type = type;
	}

	/**
	 * Copy constructor to create graph from existing graph
	 * <p>
	 * <code> 
	 * Time Complexity 
	 * T(Graph) = n + n^2 
	 * 			= O(n^2)
	 * </code>
	 * 
	 * @param g existing graph to make it copy
	 */
	public Graph(Graph<T> g) {
		type = g.getType();

		// Copy the vertices which also copies the edges
		for (Vertex<T> v : g.getVertices())
			this.allVertices.add(new Vertex<T>(v));

		for (Vertex<T> v : this.getVertices()) {
			for (Edge<T> e : v.getEdges()) {
				this.allEdges.add(e);
			}
		}
	}

	/**
	 * Parameterized constructor to create {@code Graph.TYPE#UNDIRECTED} graph with
	 * given set of vertices and edges.
	 * <p>
	 * <code> 
	 * Time Complexity 
	 * T(Graph) = 1 + 1 + 7n 
	 * 			= 7n + 2 
	 * 			= C1n + C2 
	 * 			= O(n)
	 * </code>
	 * 
	 * @param vertices set of vertices
	 * @param edges    set of edges
	 */
	public Graph(Collection<Vertex<T>> vertices, Collection<Edge<T>> edges) {
		this(TYPE.UNDIRECTED, vertices, edges);
	}

	/**
	 * Parameterized constructor to create given set of vertices and edges and
	 * specific {@link TYPE}
	 * <p>
	 * <code> 
	 * Time Complexity 
	 * T(Graph) = 1 + 1 + 7n 
	 * 			= 7n + 2 
	 * 			= C1n + C2 
	 * 			= O(n)
	 * </code>
	 * 
	 * @param type     Type of {@code Graph}
	 * @param vertices set of vertices
	 * @param edges    set of edge
	 */
	public Graph(TYPE type, Collection<Vertex<T>> vertices, Collection<Edge<T>> edges) {
		this(type);

		this.allVertices.addAll(vertices);
		this.allEdges.addAll(edges);

		for (Edge<T> e : edges) {
			final Vertex<T> from = e.getFrom();
			final Vertex<T> to = e.getTo();

			if (!this.allVertices.contains(from) || !this.allVertices.contains(to))
				continue;

			from.addEdge(e);
			if (this.type == TYPE.UNDIRECTED) {
				Edge<T> reciprical = new Edge<T>(e.getCost(), to, from);
				to.addEdge(reciprical);
				this.allEdges.add(reciprical);
			}
		}
	}

	/**
	 * it could be {@link Graph.TYPE#UNDIRECTED} or {@link Graph.TYPE#DIRECTED}
	 * 
	 * @return the type of {@link Graph}.
	 */
	public TYPE getType() {
		return type;
	}

	/**
	 * Returns the list of {@link Vertex} for the corresponding graph
	 * 
	 * @return list of {@link Vertex}
	 */
	public List<Vertex<T>> getVertices() {
		return allVertices;
	}

	/**
	 * Returns the list of {@link Edge) for the corresponding graph
	 * 
	 * @return list of {@link Edge}
	 */
	public List<Edge<T>> getEdges() {
		return allEdges;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int code = this.type.hashCode() + this.allVertices.size() + this.allEdges.size();
		for (Vertex<T> v : allVertices)
			code *= v.hashCode();
		for (Edge<T> e : allEdges)
			code *= e.hashCode();
		return 31 * code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object g1) {
		if (!(g1 instanceof Graph))
			return false;

		final Graph<T> g = (Graph<T>) g1;

		final boolean typeEquals = this.type == g.type;
		if (!typeEquals)
			return false;

		final boolean verticesSizeEquals = this.allVertices.size() == g.allVertices.size();
		if (!verticesSizeEquals)
			return false;

		final boolean edgesSizeEquals = this.allEdges.size() == g.allEdges.size();
		if (!edgesSizeEquals)
			return false;

		// Vertices can contain duplicates and appear in different order but both arrays
		// should contain the same elements
		final Object[] ov1 = this.allVertices.toArray();
		Arrays.sort(ov1);
		final Object[] ov2 = g.allVertices.toArray();
		Arrays.sort(ov2);
		for (int i = 0; i < ov1.length; i++) {
			final Vertex<T> v1 = (Vertex<T>) ov1[i];
			final Vertex<T> v2 = (Vertex<T>) ov2[i];
			if (!v1.equals(v2))
				return false;
		}

		// Edges can contain duplicates and appear in different order but both arrays
		// should contain the same elements
		final Object[] oe1 = this.allEdges.toArray();
		Arrays.sort(oe1);
		final Object[] oe2 = g.allEdges.toArray();
		Arrays.sort(oe2);
		for (int i = 0; i < oe1.length; i++) {
			final Edge<T> e1 = (Edge<T>) oe1[i];
			final Edge<T> e2 = (Edge<T>) oe2[i];
			if (!e1.equals(e2))
				return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		for (Vertex<T> v : allVertices)
			builder.append(v.toString());
		return builder.toString();
	}
}
