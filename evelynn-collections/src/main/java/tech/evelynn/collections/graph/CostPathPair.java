package tech.evelynn.collections.graph;

import java.util.Iterator;
import java.util.List;

/**
 * A cost-path pair is the primary element of the graph to store cost of path
 * exist in the graph. it can be considered as super-set of cost-vertex pair.
 * All edge are being kept inside the edge list and the sum of all the edge will
 * be accumulated by this cost-path pair.
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <T> Type of {@link Edge} for cost-path pair
 */
@SuppressWarnings("unchecked")
public class CostPathPair<T extends Comparable<T>> {

	private int cost = 0;
	private List<Edge<T>> path = null;

	/**
	 * Parameterized constructor to create a cost-path pair.
	 * 
	 * @param cost initial cost of the cost-path pair
	 * @param path list of all edge which are considered as path.
	 */
	public CostPathPair(int cost, List<Edge<T>> path) {
		if (path == null)
			throw (new NullPointerException("path cannot be NULL."));
		this.cost = cost;
		this.path = path;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public List<Edge<T>> getPath() {
		return path;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int hash = this.cost;
		for (Edge<T> e : path)
			hash *= e.getCost();
		return 31 * hash;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof CostPathPair))
			return false;

		final CostPathPair<?> pair = (CostPathPair<?>) obj;
		if (this.cost != pair.cost)
			return false;

		final Iterator<?> iter1 = this.getPath().iterator();
		final Iterator<?> iter2 = pair.getPath().iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			Edge<T> e1 = (Edge<T>) iter1.next();
			Edge<T> e2 = (Edge<T>) iter2.next();
			if (!e1.equals(e2))
				return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Cost = ").append(cost).append("\n");
		for (Edge<T> e : path)
			builder.append("\t").append(e);
		return builder.toString();
	}
}
