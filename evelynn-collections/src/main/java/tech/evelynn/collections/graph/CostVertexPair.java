package tech.evelynn.collections.graph;

/**
 * A cost-vertex pair is the primary element of the graph to store a significant
 * cost(or time) is taken to reach any particular {@code Vertex}
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <T> Type of {@link Vertex}
 */
public class CostVertexPair<T extends Comparable<T>> implements Comparable<CostVertexPair<T>> {

	private int cost = Integer.MAX_VALUE;
	private Vertex<T> vertex = null;

	/**
	 * Parameterized constructor with initial cost and <code>non-null</code> vertex.
	 * In case {@link Vertex} is null the it will throw {@link NullPointerException}
	 * 
	 * @param cost   initial cost of the {@link Vertex}
	 * @param vertex vertex to be assign with the initial cost
	 */
	public CostVertexPair(int cost, Vertex<T> vertex) {
		if (vertex == null)
			throw (new NullPointerException("vertex cannot be NULL."));

		this.cost = cost;
		this.vertex = vertex;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Vertex<T> getVertex() {
		return vertex;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return 31 * (this.cost * ((this.vertex != null) ? this.vertex.hashCode() : 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object e1) {
		if (!(e1 instanceof CostVertexPair))
			return false;

		final CostVertexPair<?> pair = (CostVertexPair<?>) e1;
		if (this.cost != pair.cost)
			return false;

		if (!this.vertex.equals(pair.vertex))
			return false;

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CostVertexPair<T> p) {
		if (p == null)
			throw new NullPointerException("CostVertexPair 'p' must be non-NULL.");

		if (this.cost < p.cost)
			return -1;
		if (this.cost > p.cost)
			return 1;
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(vertex.getValue()).append(" (").append(vertex.getWeight()).append(") ").append(" cost=")
				.append(cost).append("\n");
		return builder.toString();
	}
}
