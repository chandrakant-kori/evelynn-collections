package tech.evelynn.collections.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A vertex (plural vertices) is the fundamental unit of which graphs are
 * formed, From the point of view of graph theory, vertices are treated as
 * featureless and indivisible objects, although they may have additional
 * structure depending on the application from which the graph arises.
 * 
 * <table BORDER CELLPADDING=3 CELLSPACING=1> <caption> Summary of
 * {@code Vertex} operations </caption>
 * <tr>
 * <td ALIGN=CENTER><em>Operation</em></td>
 * <td ALIGN=CENTER><em>Method Name</em></td>
 * <td ALIGN=CENTER><em>Time Complexity</em></td>
 * </tr>
 * <tr>
 * <td><em>Path existence</em></td>
 * <td><em>{@link Vertex#pathTo(Vertex)}</em></td>
 * <td ALIGN=CENTER><em>O(n)</em></td>
 * </tr>
 * <tr>
 * <td><em>Getting edge to reach another {@code Vertex}</em></td>
 * <td><em>{@link Vertex#getEdge(Vertex)}</em></td>
 * <td ALIGN=CENTER><em>O(n)</em></td>
 * </tr>
 * </table>
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 * @version 1.0
 *
 * @param <T> Type of data will be kept inside the graph
 */
@SuppressWarnings("unchecked")
public class Vertex<T extends Comparable<T>> implements Comparable<Vertex<T>> {

	private T value = null;
	private int weight = 0;
	private List<Edge<T>> edges = new ArrayList<Edge<T>>();

	/**
	 * Parameterized Constructor with initial data.
	 * 
	 * @param value initial data of the vertex.
	 */
	public Vertex(T value) {
		this.value = value;
	}

	/**
	 * Parameterized Constructor with initial data and weight.
	 * 
	 * @param value  initial data of the vertex.
	 * @param weight initial weight of the vertex.
	 */
	public Vertex(T value, int weight) {
		this(value);
		this.weight = weight;
	}

	/**
	 * Copy constructor to create vertex from existing vertex
	 * 
	 * @param vertex existing copy vertex
	 */
	public Vertex(Vertex<T> vertex) {
		this(vertex.value, vertex.weight);
		this.edges.addAll(vertex.edges);
	}

	public T getValue() {
		return value;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public void addEdge(Edge<T> e) {
		edges.add(e);
	}

	/**
	 * Provides list of edges for the corresponding {@link Vertex}
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(getEdges) = 1 = C = O(1)
	 * </code>
	 * 
	 * @return List of {@link Edge}
	 */
	public List<Edge<T>> getEdges() {
		return edges;
	}

	/**
	 * Used to return {@link Edge} to reach given {@link Vertex}
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(getEdge) = 1 + (n+1) = n + 2 = n + C = O(n)
	 * </code>
	 * 
	 * @param v Vertex to find the first reaching edge
	 * @return First {@link Edge} reaching to the given vertex
	 */
	public Edge<T> getEdge(Vertex<T> v) {
		for (Edge<T> e : edges) {
			if (e.getTo().equals(v))
				return e;
		}
		return null;
	}

	/**
	 * Used to check is there any path existing to reach given {@link Vertex}
	 * <p>
	 * <code>
	 * Time Complexity
	 * T(pathTo) = 1 + (n+1) = n + 2 = n + C = O(n)
	 * </code>
	 * 
	 * @param v {@link Vertex} going to check for path existence
	 * @return existence of path in form of boolean
	 */
	public boolean pathTo(Vertex<T> v) {
		for (Edge<T> e : edges) {
			if (e.getTo().equals(v))
				return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int code = this.value.hashCode() + this.weight + this.edges.size();
		return 31 * code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object v1) {
		if (!(v1 instanceof Vertex))
			return false;

		final Vertex<T> v = (Vertex<T>) v1;

		final boolean weightEquals = this.weight == v.weight;
		if (!weightEquals)
			return false;

		final boolean edgesSizeEquals = this.edges.size() == v.edges.size();
		if (!edgesSizeEquals)
			return false;

		final boolean valuesEquals = this.value.equals(v.value);
		if (!valuesEquals)
			return false;

		final Iterator<Edge<T>> iter1 = this.edges.iterator();
		final Iterator<Edge<T>> iter2 = v.edges.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			// Only checking the cost
			final Edge<T> e1 = iter1.next();
			final Edge<T> e2 = iter2.next();
			if (e1.getCost() != e2.getCost())
				return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Vertex<T> v) {
		final int valueComp = this.value.compareTo(v.value);
		if (valueComp != 0)
			return valueComp;

		if (this.weight < v.weight)
			return -1;
		if (this.weight > v.weight)
			return 1;

		if (this.edges.size() < v.edges.size())
			return -1;
		if (this.edges.size() > v.edges.size())
			return 1;

		final Iterator<Edge<T>> iter1 = this.edges.iterator();
		final Iterator<Edge<T>> iter2 = v.edges.iterator();
		while (iter1.hasNext() && iter2.hasNext()) {
			// Only checking the cost
			final Edge<T> e1 = iter1.next();
			final Edge<T> e2 = iter2.next();
			if (e1.getCost() < e2.getCost())
				return -1;
			if (e1.getCost() > e2.getCost())
				return 1;
		}

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Value=").append(value).append(" weight=").append(weight).append("\n");
		for (Edge<T> e : edges)
			builder.append("\t").append(e.toString());
		return builder.toString();
	}
}
