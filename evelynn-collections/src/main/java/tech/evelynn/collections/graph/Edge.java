package tech.evelynn.collections.graph;

/**
 * An edge is (together with vertices) one of the two basic units out of which
 * graphs are constructed. Each edge has two (or in hyper-graphs, more) vertices
 * to which it is attached, called its end-points. Edges may be directed or
 * undirected; undirected edges are also called lines and directed edges are
 * also called arcs or arrows. In an undirected simple graph, an edge may be
 * represented as the set of its vertices, and in a directed simple graph it may
 * be represented as an ordered pair of its vertices. An edge that connects
 * vertices x and y is sometimes written xy..
 * 
 * @author <a target="_new" href=
 *         "https://www.linkedin.com/in/chandrakant-kori/">Chandrakant Kori</a>
 *
 * @version 1.0
 * @param <T> Type of {@code Edge}
 */
@SuppressWarnings("unchecked")
public class Edge<T extends Comparable<T>> implements Comparable<Edge<T>> {

	private Vertex<T> from = null;
	private Vertex<T> to = null;
	private int cost = 0;

	/**
	 * Parameterized constructor to create an edge in between one {@code Vertex} to
	 * another {@code Vertex}
	 * 
	 * @param cost Cost (or time) taken to reach from source {@code Vertex} to
	 *             destination {@code Vertex}
	 * 
	 * @param from source {@code Vertex}
	 * @param to   destination {@code Vertex}
	 */
	public Edge(int cost, Vertex<T> from, Vertex<T> to) {
		if (from == null || to == null)
			throw (new NullPointerException("Both 'to' and 'from' vertices need to be non-NULL."));

		this.setCost(cost);
		this.setFrom(from);
		this.setTo(to);
	}

	/**
	 * Copy constructor to create an edge from existing {@code Edge}
	 * 
	 * @param e existing edge 
	 */ 
	public Edge(Edge<T> e) {
		this(e.getCost(), e.getFrom(), e.getTo());
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Vertex<T> getFromVertex() {
		return getFrom();
	}

	public Vertex<T> getToVertex() {
		return getTo();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int cost = (this.cost * (this.getFromVertex().hashCode() * this.getToVertex().hashCode()));
		return 31 * cost;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object e1) {
		if (!(e1 instanceof Edge))
			return false;

		final Edge<T> e = (Edge<T>) e1;

		final boolean costs = this.cost == e.cost;
		if (!costs)
			return false;

		final boolean from = this.getFrom().equals(e.getFrom());
		if (!from)
			return false;

		final boolean to = this.getTo().equals(e.getTo());
		if (!to)
			return false;

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Edge<T> e) {
		if (this.cost < e.cost)
			return -1;
		if (this.cost > e.cost)
			return 1;

		final int from = this.getFrom().compareTo(e.getFrom());
		if (from != 0)
			return from;

		final int to = this.getTo().compareTo(e.getTo());
		if (to != 0)
			return to;

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("[ ").append(getFrom().getValue()).append("(").append(getFrom().getWeight()).append(") ")
				.append("]").append(" -> ").append("[ ").append(getTo().getValue()).append("(")
				.append(getTo().getWeight()).append(") ").append("]").append(" = ").append(cost).append("\n");
		return builder.toString();
	}

	public Vertex<T> getTo() {
		return to;
	}

	public void setTo(Vertex<T> to) {
		this.to = to;
	}

	public Vertex<T> getFrom() {
		return from;
	}

	public void setFrom(Vertex<T> from) {
		this.from = from;
	}
}
