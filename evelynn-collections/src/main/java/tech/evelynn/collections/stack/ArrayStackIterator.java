package tech.evelynn.collections.stack;

import java.util.Iterator;

public class ArrayStackIterator<T> implements Iterator<T> {

	private ArrayStack<T> stack = null;
	private int last = -1;
	private int index = 0;

	public ArrayStackIterator(ArrayStack<T> stack) {
		this.stack = stack;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		return (index + 1 <= stack.getSize());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T next() {
		if (index >= stack.getSize())
			return null;
		last = index;
		return stack.getArray()[index++];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		stack.remove(last);
	}
}
