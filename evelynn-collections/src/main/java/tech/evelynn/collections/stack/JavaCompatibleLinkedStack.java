package tech.evelynn.collections.stack;

import java.util.AbstractQueue;
import java.util.Iterator;

@SuppressWarnings("unchecked")
public class JavaCompatibleLinkedStack<T> extends AbstractQueue<T> {

	private LinkedStack<T> stack = null;

	public JavaCompatibleLinkedStack(LinkedStack<T> stack) {
		this.stack = stack;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean add(T value) {
		return stack.push(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean remove(Object value) {
		return stack.remove((T) value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean contains(Object value) {
		return stack.contains((T) value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean offer(T value) {
		return stack.push(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T peek() {
		return stack.peek();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T poll() {
		return stack.pop();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int size() {
		return stack.size();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This iterator is NOT thread safe and is invalid when the data structure is
	 * modified.
	 */
	@Override
	public Iterator<T> iterator() {
		return (new LinkedStackIterator<T>(stack));
	}

}
