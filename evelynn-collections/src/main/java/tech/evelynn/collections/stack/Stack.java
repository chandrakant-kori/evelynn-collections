package tech.evelynn.collections.stack;

public interface Stack<T> {

	public boolean push(T value);

	public T pop();

	public T peek();

	public boolean remove(T value);

	public void clear();

	public boolean contains(T value);

	public int size();

	public java.util.Queue<T> toLifoQueue();

	public java.util.Collection<T> toCollection();
}
