package tech.evelynn.collections.stack;

import java.util.AbstractQueue;

@SuppressWarnings("unchecked")
public class JavaCompatibleArrayStack<T> extends AbstractQueue<T> {

	private ArrayStack<T> stack = null;

    public JavaCompatibleArrayStack(ArrayStack<T> stack) {
        this.stack = stack;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(T value) {
        return stack.push(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(Object value) {
        return stack.remove((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return stack.contains((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean offer(T value) {
        return stack.push(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peek() {
        return stack.peek();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T poll() {
        return stack.pop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return stack.size();
    }

    /**
     * {@inheritDoc}
     * 
     * This iterator is NOT thread safe and is invalid when the data structure is modified.
     */
    @Override
    public java.util.Iterator<T> iterator() {
        return (new ArrayStackIterator<T>(this.stack));
    }
}
