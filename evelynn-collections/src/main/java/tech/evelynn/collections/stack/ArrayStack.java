package tech.evelynn.collections.stack;

import java.util.Arrays;

@SuppressWarnings("unchecked")
public class ArrayStack<T> implements Stack<T> {

	private static final int MINIMUM_SIZE = 1024;

	private T[] array = (T[]) new Object[MINIMUM_SIZE];
	private int size = 0;

	@Override
	public boolean push(T value) {
		if (getSize() >= getArray().length)
			grow();
		getArray()[size++] = value;
		return true;
	}

	@Override
	public T pop() {
		if (getSize() <= 0)
			return null;

		T t = getArray()[setSize(getSize() - 1)];
		getArray()[getSize()] = null;

		int shrinkSize = getArray().length >> 1;
		if (shrinkSize >= MINIMUM_SIZE && getSize() < shrinkSize)
			shrink();

		return t;
	}

	@Override
	public T peek() {
		if (getSize() <= 0)
			return null;

		T t = getArray()[setSize(getSize() - 1)];
		return t;
	}

	public T get(int index) {
		if (index >= 0 && index < getSize())
			return getArray()[index];
		return null;
	}

	@Override
	public boolean remove(T value) {
		for (int i = 0; i < getSize(); i++) {
			T obj = getArray()[i];
			if (obj.equals(value)) {
				return (remove(i));
			}
		}
		return false;
	}

	public boolean remove(int index) {
		if (index != setSize(getSize() - 1)) {
			// Shift the array down one spot
			System.arraycopy(getArray(), index + 1, getArray(), index, getSize() - index);
		}
		getArray()[getSize()] = null;

		int shrinkSize = getArray().length >> 1;
		if (shrinkSize >= MINIMUM_SIZE && getSize() < shrinkSize)
			shrink();

		return true;
	}

	private void grow() {
		int growSize = getSize() + (getSize() << 1);
		setArray(Arrays.copyOf(getArray(), growSize));
	}

	private void shrink() {
		int shrinkSize = getArray().length >> 1;
		setArray(Arrays.copyOf(getArray(), shrinkSize));
	}

	@Override
	public void clear() {
		setSize(0);
	}

	@Override
	public boolean contains(T value) {
		for (int i = 0; i < getSize(); i++) {
			T obj = getArray()[i];
			if (obj.equals(value))
				return true;
		}
		return false;
	}

	@Override
	public int size() {
		return getSize();
	}

	@Override
    public java.util.Queue<T> toLifoQueue() {
        return (new JavaCompatibleArrayStack<T>(this));
    }
	
	@Override
    public java.util.Collection<T> toCollection() {
        return (new JavaCompatibleArrayStack<T>(this));
    }
	
	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = getSize() - 1; i >= 0; i--) {
            builder.append(getArray()[i]).append(", ");
        }
        return builder.toString();
    }

	public int getSize() {
		return size;
	}

	public int setSize(int size) {
		this.size = size;
		return size;
	}

	public T[] getArray() {
		return array;
	}

	public void setArray(T[] array) {
		this.array = array;
	}
}
