package tech.evelynn.collections.stack;

public class LinkedStack<T> implements Stack<T> {

	private LinkedStackNode<T> top = null;
    private int size = 0;

    public LinkedStack() {
        setTop(null);
        size = 0;
    }

    @Override
    public boolean push(T value) {
        return push(new LinkedStackNode<T>(value));
    }
    
    private boolean push(LinkedStackNode<T> node) {
        if (getTop() == null) {
            setTop(node);
        } else {
        	LinkedStackNode<T> oldTop = getTop();
            setTop(node);
            getTop().setBelow(oldTop);
            oldTop.setAbove(getTop());
        }
        size++;
        return true;
    }

    @Override
    public T pop() {
        if (getTop()==null) return null;

        LinkedStackNode<T> nodeToRemove = getTop();
        setTop(nodeToRemove.getBelow());
        if (getTop() != null) getTop().setAbove(null);

        T value = nodeToRemove.getValue();
        size--;
        return value;
    }
    
    @Override
    public T peek() {
        return (getTop()!=null)?getTop().getValue():null;
    }
    
    public T get(int index) {
    	LinkedStackNode<T> current = getTop();
        for (int i=0; i<index; i++) {
            if (current==null) break;
            current = current.getBelow();
        }
        return (current!=null)?current.getValue():null;
    }
    
    @Override
    public boolean remove(T value) {
        // Find the node
    	LinkedStackNode<T> node = getTop();
        while (node != null && (!node.getValue().equals(value))) {
            node = node.getBelow();
        }
        if (node == null) return false;
        return remove(node);
    }
    
    public boolean remove(LinkedStackNode<T> node) {
    	LinkedStackNode<T> above = node.getAbove();
    	LinkedStackNode<T> below = node.getBelow();
        if (above != null && below != null) {
            above.setBelow(below);
            below.setAbove(above);
        } else if (above != null && below == null) {
            above.setBelow(null);
        } else if (above == null && below != null) {
            // Node is the top
            below.setAbove(null);
            setTop(below);
        } else {
            // prev==null && next==null
            setTop(null);
        }
        size--;
        return true;
    }
    
    @Override
    public void clear() {
        setTop(null);
        size = 0;
    }
    
    @Override
    public boolean contains(T value) {
        if (getTop() == null) return false;
        LinkedStackNode<T> node = getTop();
        while (node != null) {
            if (node.getValue().equals(value)) return true;
            node = node.getBelow();
        }
        return false;
    }
    
    @Override
    public int size() {
        return size;
    }
    
    @Override
    public java.util.Queue<T> toLifoQueue() {
        return (new JavaCompatibleLinkedStack<T>(this));
    }
    
    @Override
    public java.util.Collection<T> toCollection() {
        return (new JavaCompatibleLinkedStack<T>(this));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        LinkedStackNode<T> node = getTop();
        while (node != null) {
            builder.append(node.getValue()).append(", ");
            node = node.getBelow();
        }
        return builder.toString();
    }

	public LinkedStackNode<T> getTop() {
		return top;
	}

	public void setTop(LinkedStackNode<T> top) {
		this.top = top;
	}
    
}
