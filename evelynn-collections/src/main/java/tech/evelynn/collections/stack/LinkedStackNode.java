package tech.evelynn.collections.stack;

public class LinkedStackNode<T> {

	private T value = null;
	private LinkedStackNode<T> above = null;
	private LinkedStackNode<T> below = null;

	public LinkedStackNode(T value) {
		this.setValue(value);
	}

	@Override
	public String toString() {
		return "value=" + getValue() + " above=" + ((getAbove() != null) ? getAbove().getValue() : "NULL") + " below="
				+ ((getBelow() != null) ? getBelow().getValue() : "NULL");
	}

	public LinkedStackNode<T> getBelow() {
		return below;
	}

	public void setBelow(LinkedStackNode<T> below) {
		this.below = below;
	}

	public LinkedStackNode<T> getAbove() {
		return above;
	}

	public void setAbove(LinkedStackNode<T> above) {
		this.above = above;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}