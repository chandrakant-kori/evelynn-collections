package tech.evelynn.collections.stack;

import java.util.Iterator;

public class LinkedStackIterator<T> implements Iterator<T> {

	private LinkedStack<T> stack = null;
    private LinkedStackNode<T> lastNode = null;
    private LinkedStackNode<T> nextNode = null;

    public LinkedStackIterator(LinkedStack<T> stack) {
        this.stack = stack;
        LinkedStackNode<T> current = stack.getTop();
        while (current!=null && current.getBelow()!=null) current = current.getBelow();
        this.nextNode = current;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext() {
        return (nextNode!=null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next() {
        LinkedStackNode<T> current = nextNode;
        if (current!=null) {
            lastNode = nextNode;
            nextNode = current.getAbove();
            return current.getValue();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove() {
        stack.remove(lastNode);
    }

}
