package tech.evelynn.collections.queue;

@SuppressWarnings("unchecked")
public class ArrayQueue<T> implements Queue<T> {

	private static final int MINIMUM_SIZE = 1024;

    private T[] array = (T[]) new Object[MINIMUM_SIZE];
    private int lastIndex = 0;
    private int firstIndex = 0;
    
    @Override
    public boolean offer(T value) {
        if (size() >= getArray().length)
            grow(size());

        getArray()[getLastIndex() % getArray().length] = value;
        setLastIndex(getLastIndex() + 1);
        return true;
    }
    
    @Override
    public T poll() {
        int size = getLastIndex() - getFirstIndex();
        if (size < 0) return null;

        T t = getArray()[getFirstIndex() % getArray().length];
        getArray()[getFirstIndex() % getArray().length] = null;
        setFirstIndex(getFirstIndex() + 1);

        size = getLastIndex() - getFirstIndex();
        if (size <= 0) {
            // Removed last element
            setLastIndex(0);
            setFirstIndex(0);
        }

        int shrinkSize = getArray().length>>1;
        if (shrinkSize >= MINIMUM_SIZE && size < shrinkSize)
            shrink();

        return t;
    }
    
    @Override
    public T peek() {
        return getArray()[getFirstIndex() % getArray().length];
    }
    
    @Override
    public boolean remove(T value) {
        for (int i=0; i < getArray().length; i++) {
            T obj = getArray()[i];
            // if obj is null, it should return false (not NPE)
            if (value.equals(obj)) return remove(i);
        }
        return false;
    }
    
    public boolean remove(int index) {
        if (index<0 || index >= getArray().length) return false;
        if (index==getFirstIndex()) return (poll()!=null);

        int adjIndex = index % getArray().length;
        int adjLastIndex = (getLastIndex()-1) % getArray().length;
        if (adjIndex != adjLastIndex) {
            // Shift the array down one spot
            System.arraycopy(getArray(), index+1, getArray(), index, (getArray().length - (index+1)));
            if (adjLastIndex < getFirstIndex()) {
            	//Wrapped around array
            	getArray()[getArray().length-1] = getArray()[0];
            	System.arraycopy(getArray(), 1, getArray(), 0, getFirstIndex()-1);
            }
        }
        getArray()[adjLastIndex] = null;

        int shrinkSize = getArray().length>>1;
        if (shrinkSize >= MINIMUM_SIZE && size() < shrinkSize)
            shrink();

        setLastIndex(getLastIndex() - 1);
        return true;
    }
    
    private void grow(int size) {
        int growSize = (size + (size<<1));
		T[] temp = (T[]) new Object[growSize];
        // Since the array can wrap around, make sure you grab the first chunk 
        int adjLast = getLastIndex() % getArray().length;
        if (adjLast > 0 && adjLast <= getFirstIndex()) {
            System.arraycopy(getArray(), 0, temp, getArray().length-adjLast, adjLast);
        }
        // Copy the remaining
        System.arraycopy(getArray(), getFirstIndex(), temp, 0, getArray().length - getFirstIndex());
        setArray(null);
        setArray(temp);
        setLastIndex((getLastIndex() - getFirstIndex()));
        setFirstIndex(0);
    }
    
    private void shrink() {
        int shrinkSize = getArray().length>>1;
        T[] temp = (T[]) new Object[shrinkSize];
        // Since the array can wrap around, make sure you grab the first chunk 
        int adjLast = getLastIndex() % getArray().length;
        int endIndex = (getLastIndex()>getArray().length) ? getArray().length : getLastIndex();
        if (adjLast <= getFirstIndex()) {
            System.arraycopy(getArray(), 0, temp, getArray().length - getFirstIndex(), adjLast);
        }
        // Copy the remaining
        System.arraycopy(getArray(), getFirstIndex(), temp, 0, endIndex-getFirstIndex());
        setArray(null);
        setArray(temp);
        setLastIndex((getLastIndex() - getFirstIndex()));
        setFirstIndex(0);
    }

    
    @Override
    public void clear() {
        setFirstIndex(0);
        setLastIndex(0);
    }
    
    @Override
    public boolean contains(T value) {
        for (int i=0; i < getArray().length; i++) {
            T obj = getArray()[i];
            // if obj is null, it should return false (not NPE)
            if (value.equals(obj)) return true;
        }
        return false;
    }
    
    @Override
    public int size() {
        return getLastIndex() - getFirstIndex();
    }
    
    
    @Override
    public java.util.Queue<T> toQueue() {
        return (new JavaCompatibleArrayQueue<T>(this));
    }
    
    @Override
    public java.util.Collection<T> toCollection() {
        return (new JavaCompatibleArrayQueue<T>(this));
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (int i = getLastIndex() - 1; i >= getFirstIndex(); i--) {
            builder.append(getArray()[i%getArray().length]).append(", ");
        }
        return builder.toString();
    }

	public int getFirstIndex() {
		return firstIndex;
	}

	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}

	public int getLastIndex() {
		return lastIndex;
	}

	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}

	public T[] getArray() {
		return array;
	}

	public void setArray(T[] array) {
		this.array = array;
	}
}
