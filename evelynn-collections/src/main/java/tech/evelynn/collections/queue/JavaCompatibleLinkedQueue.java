package tech.evelynn.collections.queue;

import java.util.AbstractQueue;

@SuppressWarnings("unchecked")
public class JavaCompatibleLinkedQueue<T> extends AbstractQueue<T>{

	private LinkedQueue<T> queue = null;

    public JavaCompatibleLinkedQueue(LinkedQueue<T> queue) {
        this.queue = queue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(T value) {
        return queue.offer(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(Object value) {
        return queue.remove((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return queue.contains((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean offer(T value) {
        return queue.offer(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peek() {
        return queue.peek();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T poll() {
        return queue.poll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return queue.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public java.util.Iterator<T> iterator() {
        return (new LinkedQueueIterator<T>(queue));
    }
}
