package tech.evelynn.collections.queue;

import java.util.AbstractQueue;

@SuppressWarnings("unchecked")
public class JavaCompatibleArrayQueue<T> extends AbstractQueue<T> {

	private ArrayQueue<T> queue = null;

    public JavaCompatibleArrayQueue(ArrayQueue<T> queue) {
        this.queue = queue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(T value) {
        return queue.offer(value);
    }

    /**
     * {@inheritDoc}
     */
	@Override
    public boolean remove(Object value) {
        return queue.remove((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return queue.contains((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean offer(T value) {
        return queue.offer(value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T peek() {
        return queue.peek();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T poll() {
        return queue.poll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return queue.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public java.util.Iterator<T> iterator() {
        return (new ArrayQueueIterator<T>(queue));
    }


}
