package tech.evelynn.collections.queue;

public class LinkedQueue<T> implements Queue<T> {

	private QueueNode<T> head = null;
	private QueueNode<T> tail = null;
	private int size = 0;

	public LinkedQueue() {
		head = null;
		setTail(null);
		size = 0;
	}

	@Override
	public boolean offer(T value) {
		return add(new QueueNode<T>(value));
	}

	private boolean add(QueueNode<T> node) {
		if (head == null) {
			head = node;
			setTail(node);
		} else {
			QueueNode<T> oldHead = head;
			head = node;
			node.setNext(oldHead);
			oldHead.setPrev(node);
		}
		size++;
		return true;
	}

	@Override
	public T poll() {
		T result = null;
		if (getTail() != null) {
			result = getTail().getValue();

			QueueNode<T> prev = getTail().getPrev();
			if (prev != null) {
				prev.setNext(null);
				setTail(prev);
			} else {
				head = null;
				setTail(null);
			}
			size--;
		}
		return result;
	}

	@Override
	public T peek() {
		return (getTail() != null) ? getTail().getValue() : null;
	}

	@Override
	public boolean remove(T value) {
		// Find the node
		QueueNode<T> node = head;
		while (node != null && (!node.getValue().equals(value))) {
			node = node.getNext();
		}
		if (node == null)
			return false;
		return remove(node);
	}

	public boolean remove(QueueNode<T> node) {
		// Update the tail, if needed
		if (node.equals(getTail()))
			setTail(node.getPrev());

		QueueNode<T> prev = node.getPrev();
		QueueNode<T> next = node.getNext();
		if (prev != null && next != null) {
			prev.setNext(next);
			next.setPrev(prev);
		} else if (prev != null && next == null) {
			prev.setNext(null);
		} else if (prev == null && next != null) {
			// Node is the head
			next.setPrev(null);
			head = next;
		} else {
			// prev==null && next==null
			head = null;
		}
		size--;
		return true;
	}

	@Override
	public void clear() {
		head = null;
		size = 0;
	}

	@Override
	public boolean contains(T value) {
		if (head == null)
			return false;

		QueueNode<T> node = head;
		while (node != null) {
			if (node.getValue().equals(value))
				return true;
			node = node.getNext();
		}
		return false;
	}
	
	@Override
    public int size() {
        return size;
    }
	
	@Override
    public java.util.Queue<T> toQueue() {
        return (new JavaCompatibleLinkedQueue<T>(this));
    }
	
	@Override
    public java.util.Collection<T> toCollection() {
        return (new JavaCompatibleLinkedQueue<T>(this));
    }
	
	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        QueueNode<T> node = head;
        while (node != null) {
            builder.append(node.getValue()).append(", ");
            node = node.getNext();
        }
        return builder.toString();
    }

	public QueueNode<T> getTail() {
		return tail;
	}

	public void setTail(QueueNode<T> tail) {
		this.tail = tail;
	}

	
}
