package tech.evelynn.collections.queue;

public class QueueNode<T> {

	private T value = null;
	private QueueNode<T> prev = null;
	private QueueNode<T> next = null;

	public QueueNode(T value) {
		this.setValue(value);
	}

	@Override
	public String toString() {
		return "value=" + getValue() + " previous=" + ((getPrev() != null) ? getPrev().getValue() : "NULL") + " next="
				+ ((getNext() != null) ? getNext().getValue() : "NULL");
	}

	public QueueNode<T> getNext() {
		return next;
	}

	public void setNext(QueueNode<T> next) {
		this.next = next;
	}

	public QueueNode<T> getPrev() {
		return prev;
	}

	public void setPrev(QueueNode<T> prev) {
		this.prev = prev;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}
}
