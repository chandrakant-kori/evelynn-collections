package tech.evelynn.collections.queue;

import java.util.Iterator;

public class LinkedQueueIterator<T> implements Iterator<T> {

	private LinkedQueue<T> queue = null;
	private QueueNode<T> lastNode = null;
	private QueueNode<T> nextNode = null;

	public LinkedQueueIterator(LinkedQueue<T> queue) {
		this.queue = queue;
		this.nextNode = queue.getTail();
	}

	@Override
	public boolean hasNext() {
		return (nextNode != null);
	}

	@Override
	public T next() {
		QueueNode<T> current = nextNode;
		lastNode = current;
		if (current != null) {
			nextNode = current.getPrev();
			return current.getValue();
		}
		return null;
	}

	@Override
	public void remove() {
		queue.remove(lastNode);
	}

}
