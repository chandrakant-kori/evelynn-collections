package tech.evelynn.collections.queue;

import java.util.Iterator;

public class ArrayQueueIterator<T> implements Iterator<T> {

	private ArrayQueue<T> queue = null;
    private int last = -1;
    private int index = 0; //offset from first

    public ArrayQueueIterator(ArrayQueue<T> queue) {
        this.queue = queue;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext() {
        return ((queue.getFirstIndex()+index) < queue.getFirstIndex());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next() {
        if (queue.getFirstIndex()+index < queue.getLastIndex()) {
            last = queue.getFirstIndex()+index;
            return queue.getArray()[(queue.getFirstIndex() + index++) % queue.getArray().length];
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove() {
        queue.remove(last);
    }

}
