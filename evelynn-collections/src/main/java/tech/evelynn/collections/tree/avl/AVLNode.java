package tech.evelynn.collections.tree.avl;

import tech.evelynn.collections.tree.bst.BSTNode;

public class AVLNode<T extends Comparable<T>> extends BSTNode<T> {

	protected int height = 1;

	protected AVLNode(BSTNode<T> parent, T value) {
		super(parent, value);
	}

	protected boolean isLeaf() {
		return ((getLesser() == null) && (getGreater() == null));
	}

	protected int updateHeight() {
		int lesserHeight = 0;
		if (getLesser() != null) {
			AVLNode<T> lesserAVLNode = (AVLNode<T>) getLesser();
			lesserHeight = lesserAVLNode.height;
		}
		int greaterHeight = 0;
		if (getGreater() != null) {
			AVLNode<T> greaterAVLNode = (AVLNode<T>) getGreater();
			greaterHeight = greaterAVLNode.height;
		}

		if (lesserHeight > greaterHeight) {
			height = lesserHeight + 1;
		} else {
			height = greaterHeight + 1;
		}
		return height;
	}

	protected int getBalanceFactor() {
		int lesserHeight = 0;
		if (getLesser() != null) {
			AVLNode<T> lesserAVLNode = (AVLNode<T>) getLesser();
			lesserHeight = lesserAVLNode.height;
		}
		int greaterHeight = 0;
		if (getGreater() != null) {
			AVLNode<T> greaterAVLNode = (AVLNode<T>) getGreater();
			greaterHeight = greaterAVLNode.height;
		}
		return greaterHeight - lesserHeight;
	}

	@Override
	public String toString() {
		return "value=" + getId() + " height=" + height + " parent=" + ((getParent() != null) ? getParent().getId() : "NULL") + " lesser="
				+ ((getLesser() != null) ? getLesser().getId() : "NULL") + " greater=" + ((getGreater() != null) ? getGreater().getId() : "NULL");
	}
}
