package tech.evelynn.collections.tree.tst;

public class TSTNode {

	private final TSTNode parent;
	private final char character;

	private boolean isWord;

	protected TSTNode loKid;
	protected TSTNode kid;
	protected TSTNode hiKid;

	protected TSTNode(TSTNode parent, char character, boolean isWord) {
		this.parent = parent;
		this.character = character;
		this.setWord(isWord);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("char=").append(this.getCharacter()).append('\n');
		if (this.loKid != null)
			builder.append('\t').append("lo=").append(this.loKid.getCharacter()).append('\n');
		if (this.kid != null)
			builder.append('\t').append("eq=").append(this.kid.getCharacter()).append('\n');
		if (this.hiKid != null)
			builder.append('\t').append("hi=").append(this.hiKid.getCharacter()).append('\n');
		return builder.toString();
	}

	public char getCharacter() {
		return character;
	}

	public boolean isWord() {
		return isWord;
	}

	public void setWord(boolean isWord) {
		this.isWord = isWord;
	}

	public TSTNode getParent() {
		return parent;
	}
}
