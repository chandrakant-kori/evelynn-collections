package tech.evelynn.collections.tree.interval;

import java.util.Comparator;

public class IntervalConstants {

	public static final Comparator<IntervalData<?>> START_COMPARATOR = new Comparator<IntervalData<?>>() {
		@Override
		public int compare(IntervalData<?> arg0, IntervalData<?> arg1) {
			// Compare start first
			if (arg0.getStart() < arg1.getStart())
				return -1;
			if (arg1.getStart() < arg0.getStart())
				return 1;
			return 0;
		}
	};

	public static final Comparator<IntervalData<?>> END_COMPARATOR = new Comparator<IntervalData<?>>() {
		@Override
		public int compare(IntervalData<?> arg0, IntervalData<?> arg1) {
			// Compare end first
			if (arg0.getEnd() > arg1.getEnd())
				return -1;
			if (arg1.getEnd() > arg0.getEnd())
				return 1;
			return 0;
		}
	};

}
