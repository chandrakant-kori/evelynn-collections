package tech.evelynn.collections.tree.kd;

import java.util.Comparator;

public class KDEuclideanComparator<K extends KDNode> implements Comparator<K> {

	private final KDXYZPoint point;

	public KDEuclideanComparator(KDXYZPoint point) {
		this.point = point;
	}

	@Override
	public int compare(K o1, K o2) {
		Double d1 = point.euclideanDistance(o1.getId());
		Double d2 = point.euclideanDistance(o2.getId());
		if (d1.compareTo(d2) < 0)
			return -1;
		else if (d2.compareTo(d1) < 0)
			return 1;
		return o1.getId().compareTo(o2.getId());
	}
}
