package tech.evelynn.collections.tree.segment;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unchecked")
public class FlatSegmentTree<D extends SegmentTreeData> extends SegmentTree<D> {

	public FlatSegmentTree(List<D> data) {
		this(data, 1);
	}

	public FlatSegmentTree(List<D> data, int minLength) {
		if (data.size() <= 0)
			throw new InvalidParameterException("Segments list is empty.");

		Collections.sort(data); // Make sure they are sorted

		// Make sure they don't overlap
		if (data.size() >= 2) {
			for (int i = 0; i < (data.size() - 2); i++) {
				SegmentTreeData s1 = data.get(i);
				SegmentTreeData s2 = data.get(i + 1);
				if (s1.end > s2.start)
					throw new InvalidParameterException("Segments are overlapping.");
			}
		}

		// Check for gaps
		final List<NonOverlappingSegment<D>> segments = new ArrayList<NonOverlappingSegment<D>>();
		for (int i = 0; i < data.size(); i++) {
			if (i < data.size() - 1) {
				final SegmentTreeData d1 = data.get(i);
				final NonOverlappingSegment<D> s1 = new NonOverlappingSegment<D>(minLength, d1.start, d1.end, (D) d1);
				segments.add(s1);
				final SegmentTreeData d2 = data.get(i + 1);
				if (d2.start - d1.end > 1) {
					final SegmentTreeData d3 = d1.copy();
					d3.clear();
					d3.start = d1.end + 1;
					d3.end = d2.start - 1;
					final NonOverlappingSegment<D> s3 = new NonOverlappingSegment<D>(minLength, d3.start, d3.end,
							(D) d3);
					segments.add(s3);
				}
			} else {
				final SegmentTreeData d1 = data.get(i);
				final NonOverlappingSegment<D> s1 = new NonOverlappingSegment<D>(minLength, d1.start, d1.end, (D) d1);
				segments.add(s1);
			}
		}

		final long start = segments.get(0).start;
		final long end = segments.get(segments.size() - 1).end;
		final int length = (int) (end - start) + 1;
		root = NonOverlappingSegment.createFromList(minLength, segments, start, length);
	}

	@Override
	public D query(long index) {
		return this.query(index, index);
	}

	@Override
	public D query(long startOfQuery, long endOfQuery) {
		if (root == null)
			return null;

		long s = startOfQuery;
		long e = endOfQuery;
		if (s < root.start)
			s = root.start;
		if (e > root.end)
			e = root.end;

		return root.query(s, e);
	}

}
