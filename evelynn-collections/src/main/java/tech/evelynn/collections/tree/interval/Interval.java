package tech.evelynn.collections.tree.interval;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Interval<O> {

	private long center = Long.MIN_VALUE;
	private Interval<O> left = null;
	private Interval<O> right = null;
	private List<IntervalData<O>> overlap = new ArrayList<IntervalData<O>>();

	public void add(IntervalData<O> data) {
		overlap.add(data);
	}

	/**
	 * Stabbing query
	 * 
	 * @param index to query for.
	 * @return data at index.
	 */
	public IntervalData<O> query(long index) {
		IntervalData<O> results = null;
		if (index < getCenter()) {
			// overlap is sorted by start point
			Collections.sort(overlap, IntervalConstants.START_COMPARATOR);
			for (IntervalData<O> data : overlap) {
				if (data.getStart() > index)
					break;

				IntervalData<O> temp = data.query(index);
				if (results == null && temp != null)
					results = temp;
				else if (results != null && temp != null)
					results.combined(temp);
			}
		} else if (index >= getCenter()) {
			// overlap is reverse sorted by end point
			Collections.sort(overlap, IntervalConstants.END_COMPARATOR);
			for (IntervalData<O> data : overlap) {
				if (data.getEnd() < index)
					break;

				IntervalData<O> temp = data.query(index);
				if (results == null && temp != null)
					results = temp;
				else if (results != null && temp != null)
					results.combined(temp);
			}
		}

		if (index < getCenter()) {
			if (getLeft() != null) {
				IntervalData<O> temp = getLeft().query(index);
				if (results == null && temp != null)
					results = temp;
				else if (results != null && temp != null)
					results.combined(temp);
			}
		} else if (index >= getCenter()) {
			if (getRight() != null) {
				IntervalData<O> temp = getRight().query(index);
				if (results == null && temp != null)
					results = temp;
				else if (results != null && temp != null)
					results.combined(temp);
			}
		}
		return results;
	}

	/**
	 * Range query
	 * 
	 * @param start of range to query for.
	 * @param end   of range to query for.
	 * @return data for range.
	 */
	public IntervalData<O> query(long start, long end) {
		IntervalData<O> results = null;
		for (IntervalData<O> data : overlap) {
			if (data.getStart() > end)
				break;
			IntervalData<O> temp = data.query(start, end);
			if (results == null && temp != null)
				results = temp;
			else if (results != null && temp != null)
				results.combined(temp);
		}

		if (getLeft() != null && start < getCenter()) {
			IntervalData<O> temp = getLeft().query(start, end);
			if (temp != null && results == null)
				results = temp;
			else if (results != null && temp != null)
				results.combined(temp);
		}

		if (getRight() != null && end >= getCenter()) {
			IntervalData<O> temp = getRight().query(start, end);
			if (temp != null && results == null)
				results = temp;
			else if (results != null && temp != null)
				results.combined(temp);
		}

		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Center=").append(getCenter());
		builder.append(" Set=").append(overlap);
		return builder.toString();
	}

	public long getCenter() {
		return center;
	}

	public void setCenter(long center) {
		this.center = center;
	}

	public Interval<O> getLeft() {
		return left;
	}

	public void setLeft(Interval<O> left) {
		this.left = left;
	}

	public Interval<O> getRight() {
		return right;
	}

	public void setRight(Interval<O> right) {
		this.right = right;
	}
}