package tech.evelynn.collections.tree.quad;

import java.util.Comparator;

public class QuadTreeConstants {

	public static final Comparator<XYPoint> X_COMPARATOR = new Comparator<XYPoint>() {

		@Override
		public int compare(XYPoint o1, XYPoint o2) {
			if (o1.x < o2.x)
				return -1;
			if (o1.x > o2.x)
				return 1;
			return 0;
		}
	};

	public static final Comparator<XYPoint> Y_COMPARATOR = new Comparator<XYPoint>() {

		@Override
		public int compare(XYPoint o1, XYPoint o2) {
			if (o1.y < o2.y)
				return -1;
			if (o1.y > o2.y)
				return 1;
			return 0;
		}
	};
}
