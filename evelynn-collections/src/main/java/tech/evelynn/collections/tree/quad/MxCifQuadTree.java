package tech.evelynn.collections.tree.quad;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unchecked")
public class MxCifQuadTree<B extends AxisAlignedBoundingBox> extends QuadTree<B> {

	private static final XYPoint XY_POINT = new XYPoint();
	private static final AxisAlignedBoundingBox RANGE = new AxisAlignedBoundingBox();

	private MxCifQuadNode<B> root = null;

	public MxCifQuadTree(double x, double y, double width, double height) {
		this(x, y, width, height, 0, 0);
	}

	public MxCifQuadTree(double x, double y, double width, double height, double minWidth, double minHeight) {
		XYPoint xyPoint = new XYPoint(x, y);
		AxisAlignedBoundingBox aabb = new AxisAlignedBoundingBox(xyPoint, width, height);
		MxCifQuadNode.minWidth = minWidth;
		MxCifQuadNode.minHeight = minHeight;
		root = new MxCifQuadNode<B>(aabb);
	}

	@Override
	public QuadNode<B> getRoot() {
		return root;
	}

	@Override
	public boolean insert(double x, double y) {
		return insert(x, y, 1, 1);
	}

	public boolean insert(double x, double y, double width, double height) {
		XYPoint xyPoint = new XYPoint(x, y);
		AxisAlignedBoundingBox range = new AxisAlignedBoundingBox(xyPoint, width, height);

		return root.insert((B) range);
	}

	@Override
	public boolean remove(double x, double y) {
		return remove(x, y, 1, 1);
	}

	public boolean remove(double x, double y, double width, double height) {
		XY_POINT.set(x, y);
		RANGE.set(XY_POINT, width, height);

		return root.remove((B) RANGE);
	}

	@Override
	public Collection<B> queryRange(double x, double y, double width, double height) {
		if (root == null)
			return Collections.EMPTY_LIST;

		XY_POINT.set(x, y);
		RANGE.set(XY_POINT, width, height);

		List<B> geometricObjectsInRange = new LinkedList<B>();
		root.queryRange(RANGE, geometricObjectsInRange);
		return geometricObjectsInRange;
	}

}
