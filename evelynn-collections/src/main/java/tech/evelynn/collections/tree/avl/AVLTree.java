package tech.evelynn.collections.tree.avl;

import tech.evelynn.collections.tree.bst.BSTNode;
import tech.evelynn.collections.tree.bst.BSTNodeCreator;
import tech.evelynn.collections.tree.bst.BinarySearchTree;

public class AVLTree<T extends Comparable<T>> extends BinarySearchTree<T> {

	private enum Balance {
		LEFT_LEFT, LEFT_RIGHT, RIGHT_LEFT, RIGHT_RIGHT
	}

	public AVLTree() {
		this.creator = new BSTNodeCreator<T>() {
			@Override
			public BSTNode<T> createNewNode(BSTNode<T> parent, T id) {
				return (new AVLNode<T>(parent, id));
			}
		};
	}

	public AVLTree(BSTNodeCreator<T> creator) {
		super(creator);
	}

	@Override
	public BSTNode<T> addValue(T id) {
		BSTNode<T> nodeToReturn = super.addValue(id);
		AVLNode<T> nodeAdded = (AVLNode<T>) nodeToReturn;
		nodeAdded.updateHeight();
		balanceAfterInsert(nodeAdded);

		nodeAdded = (AVLNode<T>) nodeAdded.getParent();
		while (nodeAdded != null) {
			int h1 = nodeAdded.height;

			nodeAdded.updateHeight();
			balanceAfterInsert(nodeAdded);

			// If height before and after balance is the same, stop going up the tree
			int h2 = nodeAdded.height;
			if (h1 == h2)
				break;

			nodeAdded = (AVLNode<T>) nodeAdded.getParent();
		}
		return nodeToReturn;
	}

	private void balanceAfterInsert(AVLNode<T> node) {
		int balanceFactor = node.getBalanceFactor();
		if (balanceFactor > 1 || balanceFactor < -1) {
			AVLNode<T> child = null;
			Balance balance = null;
			if (balanceFactor < 0) {
				child = (AVLNode<T>) node.getLesser();
				balanceFactor = child.getBalanceFactor();
				if (balanceFactor < 0)
					balance = Balance.LEFT_LEFT;
				else
					balance = Balance.LEFT_RIGHT;
			} else {
				child = (AVLNode<T>) node.getGreater();
				balanceFactor = child.getBalanceFactor();
				if (balanceFactor < 0)
					balance = Balance.RIGHT_LEFT;
				else
					balance = Balance.RIGHT_RIGHT;
			}

			if (balance == Balance.LEFT_RIGHT) {
				// Left-Right (Left rotation, right rotation)
				rotateLeft(child);
				rotateRight(node);
			} else if (balance == Balance.RIGHT_LEFT) {
				// Right-Left (Right rotation, left rotation)
				rotateRight(child);
				rotateLeft(node);
			} else if (balance == Balance.LEFT_LEFT) {
				// Left-Left (Right rotation)
				rotateRight(node);
			} else {
				// Right-Right (Left rotation)
				rotateLeft(node);
			}

			child.updateHeight();
			node.updateHeight();
		}
	}

	@Override
	public BSTNode<T> removeValue(T value) {
		// Find node to remove
		BSTNode<T> nodeToRemoved = this.getNode(value);
		if (nodeToRemoved == null)
			return null;

		// Find the replacement node
		BSTNode<T> replacementNode = this.getReplacementNode(nodeToRemoved);

		// Find the parent of the replacement node to re-factor the height/balance of
		// the tree
		AVLNode<T> nodeToRefactor = null;
		if (replacementNode != null)
			nodeToRefactor = (AVLNode<T>) replacementNode.getParent();
		if (nodeToRefactor == null)
			nodeToRefactor = (AVLNode<T>) nodeToRemoved.getParent();
		if (nodeToRefactor != null && nodeToRefactor == nodeToRemoved)
			nodeToRefactor = (AVLNode<T>) replacementNode;

		// Replace the node
		replaceNodeWithNode(nodeToRemoved, replacementNode);

		// Re-balance the tree all the way up the tree
		while (nodeToRefactor != null) {
			nodeToRefactor.updateHeight();
			balanceAfterDelete(nodeToRefactor);

			nodeToRefactor = (AVLNode<T>) nodeToRefactor.getParent();
		}

		return nodeToRemoved;
	}

	private void balanceAfterDelete(AVLNode<T> node) {
		int balanceFactor = node.getBalanceFactor();
		if (balanceFactor == -2 || balanceFactor == 2) {
			if (balanceFactor == -2) {
				AVLNode<T> ll = (AVLNode<T>) node.getLesser().getLesser();
				int lesser = (ll != null) ? ll.height : 0;
				AVLNode<T> lr = (AVLNode<T>) node.getLesser().getGreater();
				int greater = (lr != null) ? lr.height : 0;
				if (lesser >= greater) {
					rotateRight(node);
					node.updateHeight();
					if (node.getParent() != null)
						((AVLNode<T>) node.getParent()).updateHeight();
				} else {
					rotateLeft(node.getLesser());
					rotateRight(node);

					AVLNode<T> p = (AVLNode<T>) node.getParent();
					if (p.getLesser() != null)
						((AVLNode<T>) p.getLesser()).updateHeight();
					if (p.getGreater() != null)
						((AVLNode<T>) p.getGreater()).updateHeight();
					p.updateHeight();
				}
			} else if (balanceFactor == 2) {
				AVLNode<T> rr = (AVLNode<T>) node.getGreater().getGreater();
				int greater = (rr != null) ? rr.height : 0;
				AVLNode<T> rl = (AVLNode<T>) node.getGreater().getLesser();
				int lesser = (rl != null) ? rl.height : 0;
				if (greater >= lesser) {
					rotateLeft(node);
					node.updateHeight();
					if (node.getParent() != null)
						((AVLNode<T>) node.getParent()).updateHeight();
				} else {
					rotateRight(node.getGreater());
					rotateLeft(node);

					AVLNode<T> p = (AVLNode<T>) node.getParent();
					if (p.getLesser() != null)
						((AVLNode<T>) p.getLesser()).updateHeight();
					if (p.getGreater() != null)
						((AVLNode<T>) p.getGreater()).updateHeight();
					p.updateHeight();
				}
			}
		}
	}

	@Override
	public String toString() {
		return AVLTreePrinter.getString(this);
	}

}
