package tech.evelynn.collections.tree.segment;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("unchecked")
public final class NonOverlappingSegment<D extends SegmentTreeData> extends Segment<D> {

	private Set<Segment<D>> set = new TreeSet<Segment<D>>();

	public NonOverlappingSegment(int minLength) {
		super(minLength);
	}

	public NonOverlappingSegment(int minLength, D data) {
		this(minLength, data.start, data.end, data);
	}

	public NonOverlappingSegment(int minLength, long start, long end, D data) {
		super(minLength);

		this.start = start;
		this.end = end;
		this.length = ((int) (end - start)) + 1;
		if (data == null)
			return;
		this.data = ((D) data.copy());
	}

	protected static <D extends SegmentTreeData> Segment<D> createFromList(int minLength,
			List<NonOverlappingSegment<D>> segments, long start, int length) {
		final NonOverlappingSegment<D> segment = new NonOverlappingSegment<D>(minLength);
		segment.start = start;
		segment.end = start + (length - 1);
		segment.length = length;

		for (Segment<D> s : segments) {
			if (segment.data == null)
				segment.data = ((D) s.data.copy());
			else
				segment.data.combined(s.data); // Update our data to reflect all children's data
		}

		// If segment is greater or equal to two, split data into children
		if (segment.length >= 2 && segment.length >= minLength) {
			segment.half = segment.length / 2;

			final List<NonOverlappingSegment<D>> s1 = new ArrayList<NonOverlappingSegment<D>>();
			final List<NonOverlappingSegment<D>> s2 = new ArrayList<NonOverlappingSegment<D>>();
			for (int i = 0; i < segments.size(); i++) {
				final NonOverlappingSegment<D> s = segments.get(i);
				final long middle = segment.start + segment.half;
				if (s.end < middle) {
					s1.add(s);
				} else if (s.start >= middle) {
					s2.add(s);
				} else {
					// Need to split across middle
					final NonOverlappingSegment<D> ss1 = new NonOverlappingSegment<D>(minLength, s.start, middle - 1,
							s.data);
					s1.add(ss1);

					final NonOverlappingSegment<D> ss2 = new NonOverlappingSegment<D>(minLength, middle, s.end, s.data);
					s2.add(ss2);
				}
			}

			final Segment<D> sub1 = createFromList(minLength, s1, segment.start, segment.half);
			final Segment<D> sub2 = createFromList(minLength, s2, segment.start + segment.half,
					segment.length - segment.half);
			segment.segments = new Segment[] { sub1, sub2 };
		} else if (segment.length <= minLength) {
			for (Segment<D> s : segments) {
				segment.set.add(s);
			}
		}

		return segment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public D query(long startOfQuery, long endOfQuery) {
		if (startOfQuery == this.start && endOfQuery == this.end) {
			if (this.data == null)
				return null;
			final D dataToReturn = ((D) this.data.query(startOfQuery, endOfQuery));
			return dataToReturn;
		}

		if (!this.hasChildren()) {
			if (endOfQuery < this.start || startOfQuery > this.end) {
				// Ignore
			} else {
				D dataToReturn = null;
				if (this.set.size() == 0)
					return dataToReturn;
				for (Segment<D> s : this.set) {
					if (s.start >= startOfQuery && s.end <= endOfQuery) {
						if (dataToReturn == null)
							dataToReturn = (D) s.data.query(startOfQuery, endOfQuery);
						else
							dataToReturn.combined(s.data);
					} else if (s.start <= startOfQuery && s.end >= endOfQuery) {
						if (dataToReturn == null)
							dataToReturn = (D) s.data.query(startOfQuery, endOfQuery);
						else
							dataToReturn.combined(s.data);
					}
				}
				return dataToReturn;
			}
		}

		if (this.hasChildren()) {
			if (startOfQuery <= this.getLeftChild().end && endOfQuery > this.getLeftChild().end) {
				final SegmentTreeData q1 = this.getLeftChild().query(startOfQuery, getLeftChild().end);
				final SegmentTreeData q2 = this.getRightChild().query(getRightChild().start, endOfQuery);
				if (q1 == null && q2 == null)
					return null;
				if (q1 != null && q2 == null)
					return (D) q1;
				if (q1 == null && q2 != null)
					return (D) q2;
				if (q1 != null && q2 != null)
					return ((D) q1.combined(q2));
			} else if (startOfQuery <= this.getLeftChild().end && endOfQuery <= this.getLeftChild().end) {
				return this.getLeftChild().query(startOfQuery, endOfQuery);
			}
			return this.getRightChild().query(startOfQuery, endOfQuery);
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append(" ");
		builder.append("Set=").append(set);
		return builder.toString();
	}
}
