package tech.evelynn.collections.tree.segment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@SuppressWarnings("unchecked")
public final class OverlappingSegment<D extends SegmentTreeData> extends Segment<D> {

	protected Set<Segment<D>> range = new HashSet<Segment<D>>();

	public OverlappingSegment(int minLength) {
		super(minLength);
	}

	public OverlappingSegment(int minLength, long start, long end, D data) {
		super(minLength);

		this.start = start;
		this.end = end;
		this.length = ((int) (end - start)) + 1;
		if (data == null)
			return;
		this.data = ((D) data.copy());
	}

	protected static <D extends SegmentTreeData> Segment<D> createFromList(int minLength,
			List<OverlappingSegment<D>> segments, long start, int length) {
		final OverlappingSegment<D> segment = new OverlappingSegment<D>(minLength);
		segment.start = start;
		segment.end = start + (length - 1);
		segment.length = length;

		for (Segment<D> s : segments) {
			if (s.data == null)
				continue;
			if (s.end < segment.start || s.start > segment.end) {
				// Ignore
			} else {
				segment.range.add(s);
			}
			if (s.start == segment.start && s.end == segment.end) {
				if (segment.data == null)
					segment.data = ((D) s.data.copy());
				else
					segment.data.combined(s.data); // Update our data to reflect all children's data
			} else if (!segment.hasChildren() && s.start >= segment.start && s.end <= segment.end) {
				if (segment.data == null)
					segment.data = ((D) s.data.copy());
				else
					segment.data.combined(s.data); // Update our data to reflect all children's data
			}
		}

		// If segment is greater or equal to two, split data into children
		if (segment.length >= 2 && segment.length >= minLength) {
			segment.half = segment.length / 2;

			final List<OverlappingSegment<D>> s1 = new ArrayList<OverlappingSegment<D>>();
			final List<OverlappingSegment<D>> s2 = new ArrayList<OverlappingSegment<D>>();
			for (int i = 0; i < segments.size(); i++) {
				final OverlappingSegment<D> s = segments.get(i);
				final long middle = segment.start + segment.half;
				if (s.end < middle) {
					s1.add(s);
				} else if (s.start >= middle) {
					s2.add(s);
				} else {
					// Need to split across middle
					final OverlappingSegment<D> ss1 = new OverlappingSegment<D>(minLength, s.start, middle - 1, s.data);
					s1.add(ss1);

					final OverlappingSegment<D> ss2 = new OverlappingSegment<D>(minLength, middle, s.end, s.data);
					s2.add(ss2);
				}
			}

			final Segment<D> sub1 = createFromList(minLength, s1, segment.start, segment.half);
			final Segment<D> sub2 = createFromList(minLength, s2, segment.start + segment.half,
					segment.length - segment.half);
			segment.segments = new Segment[] { sub1, sub2 };
		}

		return segment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public D query(long startOfQuery, long endOfQuery) {
		D result = null;

		// Use the range data to make range queries faster
		if (startOfQuery == this.start && endOfQuery == this.end) {
			for (Segment<D> s : this.range) {
				final D temp = (D) s.data.query(startOfQuery, endOfQuery);
				if (temp != null) {
					if (result == null)
						result = (D) temp.copy();
					else
						result.combined(temp);
				}
			}
		} else if (!this.hasChildren()) {
			if (endOfQuery < this.start || startOfQuery > this.end) {
				// Ignore
			} else {
				for (Segment<D> s : this.range) {
					if (endOfQuery < s.start || startOfQuery > s.end) {
						// Ignore
					} else {
						final D temp = (D) s.data.query(startOfQuery, endOfQuery);
						if (temp != null) {
							if (result == null)
								result = (D) temp.copy();
							else
								result.combined(temp);
						}
					}
				}
			}
		} else {
			final long middle = this.start + this.half;
			D temp = null;
			if (startOfQuery < middle && endOfQuery >= middle) {
				temp = this.getLeftChild().query(startOfQuery, middle - 1);
				D temp2 = this.getRightChild().query(middle, endOfQuery);
				if (temp2 != null) {
					if (temp == null)
						temp = (D) temp2.copy();
					else
						temp.combined(temp2);
				}
			} else if (endOfQuery < middle) {
				temp = this.getLeftChild().query(startOfQuery, endOfQuery);
			} else if (startOfQuery >= middle) {
				temp = this.getRightChild().query(startOfQuery, endOfQuery);
			}
			if (temp != null)
				result = (D) temp.copy();
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append(" ");
		builder.append("Range=").append(range);
		return builder.toString();
	}
}
