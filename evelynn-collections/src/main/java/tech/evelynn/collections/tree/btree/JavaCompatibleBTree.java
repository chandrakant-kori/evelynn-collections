package tech.evelynn.collections.tree.btree;

import java.util.AbstractCollection;

@SuppressWarnings("unchecked")
public class JavaCompatibleBTree<T extends Comparable<T>> extends AbstractCollection<T> {

	private BTree<T> tree = null;

	public JavaCompatibleBTree(BTree<T> tree) {
		this.tree = tree;
	}

	@Override
	public boolean add(T value) {
		return tree.add(value);
	}

	@Override
	public boolean remove(Object value) {
		return (tree.remove((T) value) != null);
	}

	@Override
	public boolean contains(Object value) {
		return tree.contains((T) value);
	}

	@Override
	public int size() {
		return tree.size();
	}

	@Override
	public java.util.Iterator<T> iterator() {
		return (new BTreeIterator<T>(this.tree));
	}

}
