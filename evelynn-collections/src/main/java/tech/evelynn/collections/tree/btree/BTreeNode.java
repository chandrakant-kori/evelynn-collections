package tech.evelynn.collections.tree.btree;

import java.util.Arrays;
import java.util.Comparator;

@SuppressWarnings("unchecked")
public class BTreeNode<T extends Comparable<T>> {
	private T[] keys = null;
	private int keysSize = 0;
	private BTreeNode<T>[] children = null;
	private int childrenSize = 0;

	private Comparator<BTreeNode<T>> comparator = new Comparator<BTreeNode<T>>() {
		@Override
		public int compare(BTreeNode<T> arg0, BTreeNode<T> arg1) {
			return arg0.getKey(0).compareTo(arg1.getKey(0));
		}
	};

	protected BTreeNode<T> parent = null;

	public BTreeNode(BTreeNode<T> parent, int maxKeySize, int maxChildrenSize) {
		this.parent = parent;
		this.keys = (T[]) new Comparable[maxKeySize + 1];
		this.setKeysSize(0);
		this.setChildren(new BTreeNode[maxChildrenSize + 1]);
		this.setChildrenSize(0);
	}

	public T getKey(int index) {
		return keys[index];
	}

	public int indexOf(T value) {
		for (int i = 0; i < getKeysSize(); i++) {
			if (keys[i].equals(value))
				return i;
		}
		return -1;
	}

	public void addKey(T value) {
		keys[keysSize++] = value;
		Arrays.sort(keys, 0, getKeysSize());
	}

	public T removeKey(T value) {
		T removed = null;
		boolean found = false;
		if (getKeysSize() == 0)
			return null;
		for (int i = 0; i < getKeysSize(); i++) {
			if (keys[i].equals(value)) {
				found = true;
				removed = keys[i];
			} else if (found) {
				// shift the rest of the keys down
				keys[i - 1] = keys[i];
			}
		}
		if (found) {
			setKeysSize(getKeysSize() - 1);
			keys[getKeysSize()] = null;
		}
		return removed;
	}

	public T removeKey(int index) {
		if (index >= getKeysSize())
			return null;
		T value = keys[index];
		for (int i = index + 1; i < getKeysSize(); i++) {
			// shift the rest of the keys down
			keys[i - 1] = keys[i];
		}
		setKeysSize(getKeysSize() - 1);
		keys[getKeysSize()] = null;
		return value;
	}

	public int numberOfKeys() {
		return getKeysSize();
	}

	public BTreeNode<T> getChild(int index) {
		if (index >= getChildrenSize())
			return null;
		return getChildren()[index];
	}

	public int indexOf(BTreeNode<T> child) {
		for (int i = 0; i < getChildrenSize(); i++) {
			if (getChildren()[i].equals(child))
				return i;
		}
		return -1;
	}

	public boolean addChild(BTreeNode<T> child) {
		child.parent = this;
		getChildren()[childrenSize++] = child;
		Arrays.sort(getChildren(), 0, getChildrenSize(), comparator);
		return true;
	}

	public boolean removeChild(BTreeNode<T> child) {
		boolean found = false;
		if (getChildrenSize() == 0)
			return found;
		for (int i = 0; i < getChildrenSize(); i++) {
			if (getChildren()[i].equals(child)) {
				found = true;
			} else if (found) {
				// shift the rest of the keys down
				getChildren()[i - 1] = getChildren()[i];
			}
		}
		if (found) {
			setChildrenSize(getChildrenSize() - 1);
			getChildren()[getChildrenSize()] = null;
		}
		return found;
	}

	public BTreeNode<T> removeChild(int index) {
		if (index >= getChildrenSize())
			return null;
		BTreeNode<T> value = getChildren()[index];
		getChildren()[index] = null;
		for (int i = index + 1; i < getChildrenSize(); i++) {
			// shift the rest of the keys down
			getChildren()[i - 1] = getChildren()[i];
		}
		setChildrenSize(getChildrenSize() - 1);
		getChildren()[getChildrenSize()] = null;
		return value;
	}

	public int numberOfChildren() {
		return getChildrenSize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();

		builder.append("keys=[");
		for (int i = 0; i < numberOfKeys(); i++) {
			T value = getKey(i);
			builder.append(value);
			if (i < numberOfKeys() - 1)
				builder.append(", ");
		}
		builder.append("]\n");

		if (parent != null) {
			builder.append("parent=[");
			for (int i = 0; i < parent.numberOfKeys(); i++) {
				T value = parent.getKey(i);
				builder.append(value);
				if (i < parent.numberOfKeys() - 1)
					builder.append(", ");
			}
			builder.append("]\n");
		}

		if (getChildren() != null) {
			builder.append("keySize=").append(numberOfKeys()).append(" children=").append(numberOfChildren())
					.append("\n");
		}

		return builder.toString();
	}

	public int getKeysSize() {
		return keysSize;
	}

	public void setKeysSize(int keysSize) {
		this.keysSize = keysSize;
	}

	public int getChildrenSize() {
		return childrenSize;
	}

	public void setChildrenSize(int childrenSize) {
		this.childrenSize = childrenSize;
	}

	public BTreeNode<T>[] getChildren() {
		return children;
	}

	public void setChildren(BTreeNode<T>[] children) {
		this.children = children;
	}

}
