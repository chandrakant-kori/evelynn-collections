package tech.evelynn.collections.tree.quad;

import java.util.Collection;

public abstract class QuadTree<G extends XYPoint> {

	protected abstract QuadNode<G> getRoot();
	
	public abstract Collection<G> queryRange(double x, double y, double width, double height);
	
	public abstract boolean insert(double x, double y);
	
	public abstract boolean remove(double x, double y);
	
	@Override
    public String toString() {
        return QuadTreePrinter.getString(this);
    }
	
	
	
}
