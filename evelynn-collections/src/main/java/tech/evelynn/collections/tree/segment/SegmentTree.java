package tech.evelynn.collections.tree.segment;

public abstract class SegmentTree<D extends SegmentTreeData> {
	
	protected Segment<D> root = null;
	
	public abstract D query(long index);
	
	public abstract D query(long start, long end);

	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(SegmentTreePrinter.getString(this));
        return builder.toString();
    }
	
}
