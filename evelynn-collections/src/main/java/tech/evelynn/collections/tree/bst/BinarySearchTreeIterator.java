package tech.evelynn.collections.tree.bst;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class BinarySearchTreeIterator<C extends Comparable<C>> implements Iterator<C> {

	private BinarySearchTree<C> tree = null;
	private BSTNode<C> last = null;
	private Deque<BSTNode<C>> toVisit = new ArrayDeque<BSTNode<C>>();

	protected BinarySearchTreeIterator(BinarySearchTree<C> tree) {
		this.tree = tree;
		if (tree.getRoot() != null)
			toVisit.add(tree.getRoot());
	}

	@Override
	public boolean hasNext() {
		if (toVisit.size() > 0)
			return true;
		return false;
	}

	@Override
	public C next() {
		while (toVisit.size() > 0) {
			// Go thru the current nodes
			BSTNode<C> n = toVisit.pop();

			// Add non-null children
			if (n.getLesser() != null)
				toVisit.add(n.getLesser());
			if (n.getGreater() != null)
				toVisit.add(n.getGreater());

			// Update last node (used in remove method)
			last = n;
			return n.getId();
		}
		return null;
	}

	@Override
	public void remove() {
		tree.removeNode(last);
	}
}
