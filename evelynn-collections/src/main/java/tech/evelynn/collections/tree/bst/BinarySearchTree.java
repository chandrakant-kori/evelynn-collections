package tech.evelynn.collections.tree.bst;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.Collection;
import java.util.HashSet;
import java.util.Queue;
import java.util.Random;
import java.util.Set;

import tech.evelynn.collections.tree.Tree;

@SuppressWarnings("unchecked")
public class BinarySearchTree<T extends Comparable<T>> implements Tree<T> {

	private int modifications = 0;

	protected static final Random RANDOM = new Random();

	private BSTNode<T> root = null;
	protected int size = 0;
	protected BSTNodeCreator<T> creator = null;

	public enum DepthFirstSearchOrder {
		IN_ORDER, PRE_ORDER, POST_ORDER
	}

	public BinarySearchTree() {
		this.creator = new BSTNodeCreator<T>() {
			@Override
			public BSTNode<T> createNewNode(BSTNode<T> parent, T id) {
				return (new BSTNode<T>(parent, id));
			}
		};
	}

	public BinarySearchTree(BSTNodeCreator<T> creator) {
		this.creator = creator;
	}

	@Override
	public boolean add(T value) {
		BSTNode<T> nodeAdded = this.addValue(value);
		return (nodeAdded != null);
	}

	protected BSTNode<T> addValue(T value) {
		BSTNode<T> newNode = this.creator.createNewNode(null, value);

		// If root is null, assign
		if (getRoot() == null) {
			setRoot(newNode);
			size++;
			return newNode;
		}

		BSTNode<T> node = getRoot();
		while (node != null) {
			if (newNode.getId().compareTo(node.getId()) <= 0) {
				// Less than or equal to goes left
				if (node.getLesser() == null) {
					// New left node
					node.setLesser(newNode);
					newNode.setParent(node);
					size++;
					return newNode;
				}
				node = node.getLesser();
			} else {
				// Greater than goes right
				if (node.getGreater() == null) {
					// New right node
					node.setGreater(newNode);
					newNode.setParent(node);
					size++;
					return newNode;
				}
				node = node.getGreater();
			}
		}

		return newNode;
	}

	@Override
	public boolean contains(T value) {
		BSTNode<T> node = getNode(value);
		return (node != null);
	}

	public BSTNode<T> getNode(T value) {
		BSTNode<T> node = getRoot();
		while (node != null && node.getId() != null) {
			if (value.compareTo(node.getId()) < 0) {
				node = node.getLesser();
			} else if (value.compareTo(node.getId()) > 0) {
				node = node.getGreater();
			} else if (value.compareTo(node.getId()) == 0) {
				return node;
			}
		}
		return null;
	}

	protected void rotateLeft(BSTNode<T> node) {
		BSTNode<T> parent = node.getParent();
		BSTNode<T> greater = node.getGreater();
		BSTNode<T> lesser = greater.getLesser();

		greater.setLesser(node);
		node.setParent(greater);

		node.setGreater(lesser);

		if (lesser != null)
			lesser.setParent(node);

		if (parent != null) {
			if (node == parent.getLesser()) {
				parent.setLesser(greater);
			} else if (node == parent.getGreater()) {
				parent.setGreater(greater);
			} else {
				throw new RuntimeException("Yikes! I'm not related to my parent. " + node.toString());
			}
			greater.setParent(parent);
		} else {
			setRoot(greater);
			getRoot().setParent(null);
		}
	}

	protected void rotateRight(BSTNode<T> node) {
		BSTNode<T> parent = node.getParent();
		BSTNode<T> lesser = node.getLesser();
		BSTNode<T> greater = lesser.getGreater();

		lesser.setGreater(node);
		node.setParent(lesser);

		node.setLesser(greater);

		if (greater != null)
			greater.setParent(node);

		if (parent != null) {
			if (node == parent.getLesser()) {
				parent.setLesser(lesser);
			} else if (node == parent.getGreater()) {
				parent.setGreater(lesser);
			} else {
				throw new RuntimeException("Yikes! I'm not related to my parent. " + node.toString());
			}
			lesser.setParent(parent);
		} else {
			setRoot(lesser);
			getRoot().setParent(null);
		}
	}

	protected BSTNode<T> getGreatest(BSTNode<T> startingNode) {
		if (startingNode == null)
			return null;

		BSTNode<T> greater = startingNode.getGreater();
		while (greater != null && greater.getId() != null) {
			BSTNode<T> node = greater.getGreater();
			if (node != null && node.getId() != null)
				greater = node;
			else
				break;
		}
		return greater;
	}

	protected BSTNode<T> getLeast(BSTNode<T> startingNode) {
		if (startingNode == null)
			return null;

		BSTNode<T> lesser = startingNode.getLesser();
		while (lesser != null && lesser.getId() != null) {
			BSTNode<T> node = lesser.getLesser();
			if (node != null && node.getId() != null)
				lesser = node;
			else
				break;
		}
		return lesser;
	}

	@Override
	public T remove(T value) {
		BSTNode<T> nodeToRemove = this.removeValue(value);
		return ((nodeToRemove != null) ? nodeToRemove.getId() : null);
	}

	protected BSTNode<T> removeValue(T value) {
		BSTNode<T> nodeToRemoved = this.getNode(value);
		if (nodeToRemoved != null)
			nodeToRemoved = removeNode(nodeToRemoved);
		return nodeToRemoved;
	}

	protected BSTNode<T> removeNode(BSTNode<T> nodeToRemoved) {
		if (nodeToRemoved != null) {
			BSTNode<T> replacementNode = this.getReplacementNode(nodeToRemoved);
			replaceNodeWithNode(nodeToRemoved, replacementNode);
		}
		return nodeToRemoved;
	}

	protected BSTNode<T> getReplacementNode(BSTNode<T> nodeToRemoved) {
		BSTNode<T> replacement = null;
		if (nodeToRemoved.getGreater() != null && nodeToRemoved.getLesser() != null) {
			// Two children.
			// Add some randomness to deletions, so we don't always use the
			// greatest/least on deletion
			if (modifications % 2 != 0) {
				replacement = this.getGreatest(nodeToRemoved.getLesser());
				if (replacement == null)
					replacement = nodeToRemoved.getLesser();
			} else {
				replacement = this.getLeast(nodeToRemoved.getGreater());
				if (replacement == null)
					replacement = nodeToRemoved.getGreater();
			}
			modifications++;
		} else if (nodeToRemoved.getLesser() != null && nodeToRemoved.getGreater() == null) {
			// Using the less subtree
			replacement = nodeToRemoved.getLesser();
		} else if (nodeToRemoved.getGreater() != null && nodeToRemoved.getLesser() == null) {
			// Using the greater subtree (there is no lesser subtree, no refactoring)
			replacement = nodeToRemoved.getGreater();
		}
		return replacement;
	}

	protected void replaceNodeWithNode(BSTNode<T> nodeToRemoved, BSTNode<T> replacementNode) {
		if (replacementNode != null) {
			// Save for later
			BSTNode<T> replacementNodeLesser = replacementNode.getLesser();
			BSTNode<T> replacementNodeGreater = replacementNode.getGreater();

			// Replace replacementNode's branches with nodeToRemove's branches
			BSTNode<T> nodeToRemoveLesser = nodeToRemoved.getLesser();
			if (nodeToRemoveLesser != null && nodeToRemoveLesser != replacementNode) {
				replacementNode.setLesser(nodeToRemoveLesser);
				nodeToRemoveLesser.setParent(replacementNode);
			}
			BSTNode<T> nodeToRemoveGreater = nodeToRemoved.getGreater();
			if (nodeToRemoveGreater != null && nodeToRemoveGreater != replacementNode) {
				replacementNode.setGreater(nodeToRemoveGreater);
				nodeToRemoveGreater.setParent(replacementNode);
			}

			// Remove link from replacementNode's parent to replacement
			BSTNode<T> replacementParent = replacementNode.getParent();
			if (replacementParent != null && replacementParent != nodeToRemoved) {
				BSTNode<T> replacementParentLesser = replacementParent.getLesser();
				BSTNode<T> replacementParentGreater = replacementParent.getGreater();
				if (replacementParentLesser != null && replacementParentLesser == replacementNode) {
					replacementParent.setLesser(replacementNodeGreater);
					if (replacementNodeGreater != null)
						replacementNodeGreater.setParent(replacementParent);
				} else if (replacementParentGreater != null && replacementParentGreater == replacementNode) {
					replacementParent.setGreater(replacementNodeLesser);
					if (replacementNodeLesser != null)
						replacementNodeLesser.setParent(replacementParent);
				}
			}
		}

		// Update the link in the tree from the nodeToRemoved to the
		// replacementNode
		BSTNode<T> parent = nodeToRemoved.getParent();
		if (parent == null) {
			// Replacing the root node
			setRoot(replacementNode);
			if (getRoot() != null)
				getRoot().setParent(null);
		} else if (parent.getLesser() != null && (parent.getLesser().getId().compareTo(nodeToRemoved.getId()) == 0)) {
			parent.setLesser(replacementNode);
			if (replacementNode != null)
				replacementNode.setParent(parent);
		} else if (parent.getGreater() != null && (parent.getGreater().getId().compareTo(nodeToRemoved.getId()) == 0)) {
			parent.setGreater(replacementNode);
			if (replacementNode != null)
				replacementNode.setParent(parent);
		}
		size--;
	}

	@Override
	public void clear() {
		setRoot(null);
		size = 0;
	}

	@Override
	public int size() {
		return size;
	}

	public T[] getBFS() {
		return getBFS(this.getRoot(), this.size);
	}

	public static <T extends Comparable<T>> T[] getBFS(BSTNode<T> start, int size) {
		final Queue<BSTNode<T>> queue = new ArrayDeque<BSTNode<T>>();
		final T[] values = (T[]) Array.newInstance(start.getId().getClass(), size);
		int count = 0;
		BSTNode<T> node = start;
		while (node != null) {
			values[count++] = node.getId();
			if (node.getLesser() != null)
				queue.add(node.getLesser());
			if (node.getGreater() != null)
				queue.add(node.getGreater());
			if (!queue.isEmpty())
				node = queue.remove();
			else
				node = null;
		}
		return values;
	}

	public T[] getLevelOrder() {
		return getBFS();
	}

	public T[] getDFS(DepthFirstSearchOrder order) {
		return getDFS(order, this.getRoot(), this.size);
	}

	public static <T extends Comparable<T>> T[] getDFS(DepthFirstSearchOrder order, BSTNode<T> start, int size) {
		final Set<BSTNode<T>> added = new HashSet<BSTNode<T>>(2);
		final T[] nodes = (T[]) Array.newInstance(start.getId().getClass(), size);
		int index = 0;
		BSTNode<T> node = start;
		while (index < size && node != null) {
			BSTNode<T> parent = node.getParent();
			BSTNode<T> lesser = (node.getLesser() != null && !added.contains(node.getLesser())) ? node.getLesser() : null;
			BSTNode<T> greater = (node.getGreater() != null && !added.contains(node.getGreater())) ? node.getGreater() : null;

			if (parent == null && lesser == null && greater == null) {
				if (!added.contains(node))
					nodes[index++] = node.getId();
				break;
			}

			if (order == DepthFirstSearchOrder.IN_ORDER) {
				if (lesser != null) {
					node = lesser;
				} else {
					if (!added.contains(node)) {
						nodes[index++] = node.getId();
						added.add(node);
					}
					if (greater != null) {
						node = greater;
					} else if (added.contains(node)) {
						node = parent;
					} else {
						// We should not get here. Stop the loop!
						node = null;
					}
				}
			} else if (order == DepthFirstSearchOrder.PRE_ORDER) {
				if (!added.contains(node)) {
					nodes[index++] = node.getId();
					added.add(node);
				}
				if (lesser != null) {
					node = lesser;
				} else if (greater != null) {
					node = greater;
				} else if (added.contains(node)) {
					node = parent;
				} else {
					// We should not get here. Stop the loop!
					node = null;
				}
			} else {
				// post-Order
				if (lesser != null) {
					node = lesser;
				} else {
					if (greater != null) {
						node = greater;
					} else {
						// lesser==null && greater==null
						nodes[index++] = node.getId();
						added.add(node);
						node = parent;
					}
				}
			}
		}
		return nodes;
	}

	public T[] getSorted() {
		// Depth first search to traverse the tree in-order sorted.
		return getDFS(DepthFirstSearchOrder.IN_ORDER);
	}

	@Override
	public Collection<T> toCollection() {
		return (new JavaCompatibleBinarySearchTree<T>(this));
	}

	@Override
	public String toString() {
		return BinaryTreePrinter.getString(this);
	}

	public BSTNode<T> getRoot() {
		return root;
	}

	public void setRoot(BSTNode<T> root) {
		this.root = root;
	}

}
