package tech.evelynn.collections.tree.segment;

import java.util.Comparator;

public class SegmentTreeConstants {

	public static final Comparator<OverlappingSegment<?>> START_COMPARATOR = new Comparator<OverlappingSegment<?>>() {

		@Override
		public int compare(OverlappingSegment<?> arg0, OverlappingSegment<?> arg1) {
			if (arg0.start < arg1.start)
				return -1;
			if (arg1.start < arg0.start)
				return 1;
			return 0;
		}
	};

	public static final Comparator<OverlappingSegment<?>> END_COMPARATOR = new Comparator<OverlappingSegment<?>>() {

		@Override
		public int compare(OverlappingSegment<?> arg0, OverlappingSegment<?> arg1) {
			if (arg0.end < arg1.end)
				return -1;
			if (arg1.end < arg0.end)
				return 1;
			return 0;
		}
	};

}
