package tech.evelynn.collections.tree.tst;

import tech.evelynn.collections.tree.Tree;

public class TernarySearchTree<C extends CharSequence> implements Tree<C> {
	
	protected TSTNodeCreator creator;
    protected TSTNode root;

    private int size = 0;

    public TernarySearchTree() {
        this.creator = new TSTNodeCreator() {
            @Override
            public TSTNode createNewNode(TSTNode parent, Character character, boolean isWord) {
                return (new TSTNode(parent, character, isWord));
            }
        };
    }

    public TernarySearchTree(TSTNodeCreator creator) {
        this.creator = creator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(C value) {
        if (value == null)
            return false;

        final int before = getSize();
        if (root == null)
            this.root = insert(null, null, value, 0);
        else
            insert(null, root, value, 0);
        final int after = getSize();

        // Successful if the size has increased by one
        return (before==(after-1));
    }

    private TSTNode insert(TSTNode parent, TSTNode node, C value, int idx) {
        if (idx >= value.length())
            return null;

        final char c = value.charAt(idx);
        final boolean isWord = (idx==(value.length()-1));

        if (node == null) {
            // Create new node
            node = this.creator.createNewNode(parent, c, isWord);

            // If new node represents a "word", increase the size
            if (isWord)
                setSize(getSize() + 1);
        } else if (c==node.getCharacter() && isWord && !node.isWord()) {
            // Changing an existing node into a "word" node
            node.setWord(true);
            // Increase the size
            setSize(getSize() + 1);
        }

        if (c < node.getCharacter()) {
            node.loKid = insert(node, node.loKid, value, idx);
        } else if (c > node.getCharacter()) {
            node.hiKid = insert(node, node.hiKid, value, idx);
        } else if (idx < (value.length()-1)) {
            // Not done with whole string yet
            node.kid = insert(node, node.kid, value, ++idx);
        }
        return node;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public C remove(C value) {
        if (value == null || root == null)
            return null;

        // Find the node
        final TSTNode node = search(root, value, 0);

        // If node was not found or the node is not a "word"
        if (node == null || !node.isWord())
            return null;

        // TSTNode was found, remove from tree if possible
        node.setWord(false);
        remove(node);
        setSize(getSize() - 1);
        return value;
    }

    public void remove(TSTNode node) {
        // If node is a "word", stop the recursive pruning
        if (node.isWord())
            return;

        // If node has at least one child, we cannot prune it.
        if (node.loKid!=null || node.kid!=null || node.hiKid!=null) 
            return;

        // TSTNode has no children, prune the node
        final TSTNode parent = node.getParent();
        if (parent != null) {
            // Remove node from parent
            if (parent.loKid==node) {
                parent.loKid = null;
            } else if (parent.hiKid==node) {
                parent.hiKid = null;
            } else if (parent.kid==node) {
                parent.kid = null;
            }

            // If parent is not a "word" node, go up the tree and prune if possible
            if (!node.isWord())
                remove(parent);
        } else {
            // If node doesn't have a parent, it's root.
            this.root = null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        root = null;
        setSize(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(C value) {
        if (value == null)
            return false;

        // Find the node
        final TSTNode node = search(root, value, 0);

        // If node isn't null and it represents a "word" then the tree contains the value
        return (node!=null && node.isWord());
    }

    private TSTNode search(TSTNode node, C value, int idx) {
        if (node == null || idx>=value.length())
            return null;

        final char c = value.charAt(idx);

        if (c < node.getCharacter())
            return search(node.loKid, value, idx);
        if (c > node.getCharacter())
            return search(node.hiKid, value, idx);
        if (idx < (value.length()-1)) {
            // c == node.char and there is still some characters left in the search string
            return search(node.kid, value, ++idx);
        }
        return node;
    }

    @Override
    public int size() {
        return getSize();
    }
    
    @Override
    public java.util.Collection<C> toCollection() {
        return (new JavaCompatibleTernaryTree<C>(this));
    }

    @Override
    public String toString() {
        return TernarySearchTreePrinter.getString(this);
    }

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
