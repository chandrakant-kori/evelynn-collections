package tech.evelynn.collections.tree.bst.treap;

import java.util.Random;

import tech.evelynn.collections.tree.bst.BSTNode;

public class TreapNode<T extends Comparable<T>> extends BSTNode<T> {

	private static int randomSeed = Integer.MAX_VALUE; // This should be at least twice the number of Nodes
	private int priority = Integer.MIN_VALUE;

	private TreapNode(BSTNode<T> parent, int priority, T value) {
		super(parent, value);
		this.setPriority(priority);
	}

	public TreapNode(BSTNode<T> parent, T value) {
		this(parent, new Random().nextInt(randomSeed), value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("priorty=").append(getPriority()).append(" value=").append(getId());
		if (getParent() != null)
			builder.append(" parent=").append(getParent().getId());
		builder.append("\n");
		if (getLesser() != null)
			builder.append("left=").append(getLesser().toString()).append("\n");
		if (getGreater() != null)
			builder.append("right=").append(getGreater().toString()).append("\n");
		return builder.toString();
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

}
