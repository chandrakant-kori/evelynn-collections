package tech.evelynn.collections.tree.bst;

import java.util.AbstractCollection;
import java.util.Iterator;

@SuppressWarnings("unchecked")
public class JavaCompatibleBinarySearchTree<T extends Comparable<T>> extends AbstractCollection<T> {

	private BinarySearchTree<T> tree = null;

	public JavaCompatibleBinarySearchTree(BinarySearchTree<T> tree) {
		this.tree = tree;
	}

	@Override
	public boolean add(T value) {
		return tree.add(value);
	}

	@Override
	public boolean remove(Object value) {
		return (tree.remove((T) value) != null);
	}

	@Override
	public boolean contains(Object value) {
		return tree.contains((T) value);
	}

	@Override
	public int size() {
		return tree.size();
	}

	@Override
	public Iterator<T> iterator() {
		return (new BinarySearchTreeIterator<T>(this.tree));
	}
}
