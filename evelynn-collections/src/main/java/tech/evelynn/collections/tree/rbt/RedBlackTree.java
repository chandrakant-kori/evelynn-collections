package tech.evelynn.collections.tree.rbt;

import tech.evelynn.collections.tree.bst.BSTNode;
import tech.evelynn.collections.tree.bst.BSTNodeCreator;
import tech.evelynn.collections.tree.bst.BinarySearchTree;

public class RedBlackTree<T extends Comparable<T>> extends BinarySearchTree<T> {

	public RedBlackTree() {
		this.creator = new BSTNodeCreator<T>() {
			@Override
			public BSTNode<T> createNewNode(BSTNode<T> parent, T id) {
				return (new RedBlackNode<T>(parent, id, RedBlackTreeConstants.BLACK));
			}
		};
	}

	public RedBlackTree(BSTNodeCreator<T> creator) {
		super(creator);
	}

	@Override
	protected BSTNode<T> addValue(T id) {
		if (getRoot() == null) {
			// Case 1 - The current node is at the root of the tree.

			// Defaulted to black in our creator
			setRoot(this.creator.createNewNode(null, id));
			getRoot().setLesser(this.creator.createNewNode(getRoot(), null));
			getRoot().setGreater(this.creator.createNewNode(getRoot(), null));

			size++;
			return getRoot();
		}

		RedBlackNode<T> nodeAdded = null;
		// Insert node like a BST would
		BSTNode<T> node = getRoot();
		while (node != null) {
			if (node.getId() == null) {
				node.setId(id);
				((RedBlackNode<T>) node).color = RedBlackTreeConstants.RED;

				// Defaulted to black in our creator
				node.setLesser(this.creator.createNewNode(node, null));
				node.setGreater(this.creator.createNewNode(node, null));

				nodeAdded = (RedBlackNode<T>) node;
				break;
			} else if (id.compareTo(node.getId()) <= 0) {
				node = node.getLesser();
			} else {
				node = node.getGreater();
			}
		}

		if (nodeAdded != null)
			balanceAfterInsert(nodeAdded);

		size++;
		return nodeAdded;
	}

	private void balanceAfterInsert(RedBlackNode<T> begin) {
		RedBlackNode<T> node = begin;
		RedBlackNode<T> parent = (RedBlackNode<T>) node.getParent();

		if (parent == null) {
			// Case 1 - The current node is at the root of the tree.
			node.color = RedBlackTreeConstants.BLACK;
			return;
		}

		if (parent.color == RedBlackTreeConstants.BLACK) {
			// Case 2 - The current node's parent is black, so property 4 (both
			// children of every red node are black) is not invalidated.
			return;
		}

		RedBlackNode<T> grandParent = node.getGrandParent();
		RedBlackNode<T> uncle = node.getUncle(grandParent);
		if (parent.color == RedBlackTreeConstants.RED && uncle.color == RedBlackTreeConstants.RED) {
			// Case 3 - If both the parent and the uncle are red, then both of
			// them can be repainted black and the grandparent becomes
			// red (to maintain property 5 (all paths from any given node to its
			// leaf nodes contain the same number of black nodes)).
			parent.color = RedBlackTreeConstants.BLACK;
			uncle.color = RedBlackTreeConstants.BLACK;
			if (grandParent != null) {
				grandParent.color = RedBlackTreeConstants.RED;
				balanceAfterInsert(grandParent);
			}
			return;
		}

		if (parent.color == RedBlackTreeConstants.RED && uncle.color == RedBlackTreeConstants.BLACK) {
			// Case 4 - The parent is red but the uncle is black; also, the
			// current node is the right child of parent, and parent in turn
			// is the left child of its parent grandparent.
			if (node == parent.getGreater() && parent == grandParent.getLesser()) {
				// right-left
				rotateLeft(parent);

				node = (RedBlackNode<T>) node.getLesser();
				parent = (RedBlackNode<T>) node.getParent();
				grandParent = node.getGrandParent();
				uncle = node.getUncle(grandParent);
			} else if (node == parent.getLesser() && parent == grandParent.getGreater()) {
				// left-right
				rotateRight(parent);

				node = (RedBlackNode<T>) node.getGreater();
				parent = (RedBlackNode<T>) node.getParent();
				grandParent = node.getGrandParent();
				uncle = node.getUncle(grandParent);
			}
		}

		if (parent.color == RedBlackTreeConstants.RED && uncle.color == RedBlackTreeConstants.BLACK) {
			// Case 5 - The parent is red but the uncle is black, the
			// current node is the left child of parent, and parent is the
			// left child of its parent G.
			parent.color = RedBlackTreeConstants.BLACK;
			grandParent.color = RedBlackTreeConstants.RED;
			if (node == parent.getLesser() && parent == grandParent.getLesser()) {
				// left-left
				rotateRight(grandParent);
			} else if (node == parent.getGreater() && parent == grandParent.getGreater()) {
				// right-right
				rotateLeft(grandParent);
			}
		}
	}

	@Override
	protected BSTNode<T> removeNode(BSTNode<T> node) {
		if (node == null)
			return node;

		RedBlackNode<T> nodeToRemoved = (RedBlackNode<T>) node;

		if (nodeToRemoved.isLeaf()) {
			// No children
			nodeToRemoved.setId(null);
			if (nodeToRemoved == getRoot()) {
				setRoot(null);
			} else {
				nodeToRemoved.setId(null);
				nodeToRemoved.color = RedBlackTreeConstants.BLACK;
				nodeToRemoved.setLesser(null);
				nodeToRemoved.setGreater(null);
			}

			size--;
			return nodeToRemoved;
		}

		// At least one child

		// Keep the id and assign it to the replacement node
		T id = nodeToRemoved.getId();
		RedBlackNode<T> lesser = (RedBlackNode<T>) nodeToRemoved.getLesser();
		RedBlackNode<T> greater = (RedBlackNode<T>) nodeToRemoved.getGreater();
		if (lesser.getId() != null && greater.getId() != null) {
			// Two children
			RedBlackNode<T> greatestInLesser = (RedBlackNode<T>) this.getGreatest(lesser);
			if (greatestInLesser == null || greatestInLesser.getId() == null)
				greatestInLesser = lesser;

			// Replace node with greatest in his lesser tree, which leaves us with only one
			// child
			replaceValueOnly(nodeToRemoved, greatestInLesser);
			nodeToRemoved = greatestInLesser;
			lesser = (RedBlackNode<T>) nodeToRemoved.getLesser();
			greater = (RedBlackNode<T>) nodeToRemoved.getGreater();
		}

		// Handle one child
		RedBlackNode<T> child = (RedBlackNode<T>) ((lesser.getId() != null) ? lesser : greater);
		if (nodeToRemoved.color == RedBlackTreeConstants.BLACK) {
			if (child.color == RedBlackTreeConstants.BLACK)
				nodeToRemoved.color = RedBlackTreeConstants.RED;
			boolean result = balanceAfterDelete(nodeToRemoved);
			if (!result)
				return nodeToRemoved;
		}

		// Replacing node with child
		replaceWithChild(nodeToRemoved, child);
		// Add the id to the child because it represents the node that was removed.
		child.setId(id);
		if (getRoot() == nodeToRemoved) {
			getRoot().setParent(null);
			((RedBlackNode<T>) getRoot()).color = RedBlackTreeConstants.BLACK;
			// If we replaced the root with a leaf, just null out root
			if (nodeToRemoved.isLeaf())
				setRoot(null);
		}
		nodeToRemoved = child;

		size--;
		return nodeToRemoved;
	}

	private void replaceValueOnly(RedBlackNode<T> nodeToReplace, RedBlackNode<T> nodeToReplaceWith) {
		nodeToReplace.setId(nodeToReplaceWith.getId());
		nodeToReplaceWith.setId(null);
	}

	private void replaceWithChild(RedBlackNode<T> nodeToReplace, RedBlackNode<T> nodeToReplaceWith) {
		nodeToReplace.setId(nodeToReplaceWith.getId());
		nodeToReplace.color = nodeToReplaceWith.color;

		nodeToReplace.setLesser(nodeToReplaceWith.getLesser());
		if (nodeToReplace.getLesser() != null)
			nodeToReplace.getLesser().setParent(nodeToReplace);

		nodeToReplace.setGreater(nodeToReplaceWith.getGreater());
		if (nodeToReplace.getGreater() != null)
			nodeToReplace.getGreater().setParent(nodeToReplace);
	}

	private boolean balanceAfterDelete(RedBlackNode<T> node) {
		if (node.getParent() == null) {
			// Case 1 - node is the new root.
			return true;
		}

		RedBlackNode<T> parent = (RedBlackNode<T>) node.getParent();
		RedBlackNode<T> sibling = node.getSibling();
		if (sibling.color == RedBlackTreeConstants.RED) {
			// Case 2 - sibling is red.
			parent.color = RedBlackTreeConstants.RED;
			sibling.color = RedBlackTreeConstants.BLACK;
			if (node == parent.getLesser()) {
				rotateLeft(parent);

				// Rotation, need to update parent/sibling
				parent = (RedBlackNode<T>) node.getParent();
				sibling = node.getSibling();
			} else if (node == parent.getGreater()) {
				rotateRight(parent);

				// Rotation, need to update parent/sibling
				parent = (RedBlackNode<T>) node.getParent();
				sibling = node.getSibling();
			} else {
				throw new RuntimeException("Yikes! I'm not related to my parent. " + node.toString());
			}
		}

		if (parent.color == RedBlackTreeConstants.BLACK && sibling.color == RedBlackTreeConstants.BLACK
				&& ((RedBlackNode<T>) sibling.getLesser()).color == RedBlackTreeConstants.BLACK
				&& ((RedBlackNode<T>) sibling.getGreater()).color == RedBlackTreeConstants.BLACK) {
			// Case 3 - parent, sibling, and sibling's children are black.
			sibling.color = RedBlackTreeConstants.RED;
			return balanceAfterDelete(parent);
		}

		if (parent.color == RedBlackTreeConstants.RED && sibling.color == RedBlackTreeConstants.BLACK
				&& ((RedBlackNode<T>) sibling.getLesser()).color == RedBlackTreeConstants.BLACK
				&& ((RedBlackNode<T>) sibling.getGreater()).color == RedBlackTreeConstants.BLACK) {
			// Case 4 - sibling and sibling's children are black, but parent is red.
			sibling.color = RedBlackTreeConstants.RED;
			parent.color = RedBlackTreeConstants.BLACK;
			return true;
		}

		if (sibling.color == RedBlackTreeConstants.BLACK) {
			// Case 5 - sibling is black, sibling's left child is red,
			// sibling's right child is black, and node is the left child of
			// its parent.
			if (node == parent.getLesser() && ((RedBlackNode<T>) sibling.getLesser()).color == RedBlackTreeConstants.RED
					&& ((RedBlackNode<T>) sibling.getGreater()).color == RedBlackTreeConstants.BLACK) {
				sibling.color = RedBlackTreeConstants.RED;
				((RedBlackNode<T>) sibling.getLesser()).color = RedBlackTreeConstants.RED;

				rotateRight(sibling);

				// Rotation, need to update parent/sibling
				parent = (RedBlackNode<T>) node.getParent();
				sibling = node.getSibling();
			} else if (node == parent.getGreater()
					&& ((RedBlackNode<T>) sibling.getLesser()).color == RedBlackTreeConstants.BLACK
					&& ((RedBlackNode<T>) sibling.getGreater()).color == RedBlackTreeConstants.RED) {
				sibling.color = RedBlackTreeConstants.RED;
				((RedBlackNode<T>) sibling.getGreater()).color = RedBlackTreeConstants.RED;

				rotateLeft(sibling);

				// Rotation, need to update parent/sibling
				parent = (RedBlackNode<T>) node.getParent();
				sibling = node.getSibling();
			}
		}

		// Case 6 - sibling is black, sibling's right child is red, and node
		// is the left child of its parent.
		sibling.color = parent.color;
		parent.color = RedBlackTreeConstants.BLACK;
		if (node == parent.getLesser()) {
			((RedBlackNode<T>) sibling.getGreater()).color = RedBlackTreeConstants.BLACK;
			rotateLeft(node.getParent());
		} else if (node == parent.getGreater()) {
			((RedBlackNode<T>) sibling.getLesser()).color = RedBlackTreeConstants.BLACK;
			rotateRight(node.getParent());
		} else {
			throw new RuntimeException("Yikes! I'm not related to my parent. " + node.toString());
		}

		return true;
	}

	@Override
	public java.util.Collection<T> toCollection() {
		return (new JavaCompatibleRedBlackTree<T>(this));
	}

	@Override
	public String toString() {
		return RedBlackTreePrinter.getString(this);
	}
}
