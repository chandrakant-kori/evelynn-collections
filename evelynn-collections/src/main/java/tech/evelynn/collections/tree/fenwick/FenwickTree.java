package tech.evelynn.collections.tree.fenwick;

import tech.evelynn.collections.list.List;

@SuppressWarnings("unchecked")
public class FenwickTree<D extends FenwickData> {

	private Object[] array;

    public FenwickTree(List<D> data) {
        // Find the largest index
        int n = 0;
        for (FenwickData d : data.toList())
            if (d.index > n)
                n = d.index;
        n = next(n+1);
        setArray(new Object[n]);

        // Add the data
        for (D d : data.toList())
            update(d.index, d);
    }
    
    public D query(int index) {
        return query(index, index);
    }
    
    public D query(int start, int end) {
        final D e = lookup(end);
        final D s = lookup(start-1);
		final D c = (D) e.copy();
        if (s != null)
            c.separate(s);
        return c;
    }
    
    private D lookup(int index) {
        index++; // tree index is 1 based
        index = Math.min(getArray().length - 1, index);
        if (index <= 0)
            return null;

        D res = null;
        while (index > 0) {
            if (res == null) {
                final D data = (D) getArray()[index];
                if (data != null)
                    res = (D) data.copy();
            } else{ 
                res.combined((D) getArray()[index]);
            }
            index = prev(index);
        }
        return res;
    }

    private void update(int index, D value) {
        index++; // tree index is 1 based
        while (index < getArray().length) {
            D data = (D) getArray()[index];
            if (data == null) {
                data = (D) value.copy();
                data.index = index;
                getArray()[index] = data;
            } else {
                data.combined(value);
            }
            index = next(index);
        }
    }
    
    public static final int prev(int x) {
        return x & (x - 1);
    }

    public static final int next(int x) {
        return 2 * x - prev(x);
    }
    
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(FenwickTreePrinter.getString(this));
        return builder.toString();
    }

	public Object[] getArray() {
		return array;
	}

	public void setArray(Object[] array) {
		this.array = array;
	}
    
    
    
}
