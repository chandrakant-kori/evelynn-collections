package tech.evelynn.collections.tree.suffix;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class SuffixTree<C extends CharSequence> implements ISuffixTree<C>  {

	private static final char DEFAULT_END_SEQ_CHAR = '$';

    private final char endSeqChar;

    private Map<Integer, SuffixTreeLink> linksMap = new HashMap<Integer, SuffixTreeLink>();
    private Map<Integer, SuffixTreeEdge<C>> edgeMap = new TreeMap<Integer, SuffixTreeEdge<C>>();

    private int currentNode = 0;
    private int firstCharIndex = 0;
    private int lastCharIndex = -1;

    private String string;
    private char[] characters;
    
    public SuffixTree(C seq) {
        this(seq, DEFAULT_END_SEQ_CHAR);
    }
    
    public SuffixTree(C seq, char endSeq) {
        endSeqChar = endSeq;
        StringBuilder builder = new StringBuilder(seq);
        if (builder.indexOf(String.valueOf(getEndSeqChar())) < 0)
            builder.append(getEndSeqChar());
        setString(builder.toString());
        int length = getString().length();
        setCharacters(new char[length]);
        for (int i = 0; i < length; i++) {
            char c = getString().charAt(i);
            getCharacters()[i] = c;
        }

        for (int j = 0; j < length; j++) {
            addPrefix(j);
        }
    }

    @Override
    public boolean doesSubStringExist(C sub) {
        char[] chars = new char[sub.length()];
        for (int i = 0; i < sub.length(); i++) {
            chars[i] = sub.charAt(i);
        }
        int[] indices = searchEdges(chars);
        int start = indices[0];
        int end = indices[1];
        int length = end - start;
        if (length == (chars.length - 1))
            return true;
        return false;
    }

    @Override
    public Set<String> getSuffixes() {
        Set<String> set = getSuffixes(0);
        return set;
    }

    private Set<String> getSuffixes(int start) {
        Set<String> set = new TreeSet<String>();
        for (int key : getEdgeMap().keySet()) {
            SuffixTreeEdge<C> e = getEdgeMap().get(key);
            if (e == null)
                continue;
            if (e.getStartNode() != start)
                continue;

            String s = (getString().substring(e.getFirstCharIndex(), e.getLastCharIndex() + 1));
            SuffixTreeLink n = getLinksMap().get(e.getEndNode());
            if (n == null) {
                int index = s.indexOf(getEndSeqChar());
                if (index >= 0)
                    s = s.substring(0, index);
                set.add(s);
            } else {
                Set<String> set2 = getSuffixes(e.getEndNode());
                for (String s2 : set2) {
                    int index = s2.indexOf(getEndSeqChar());
                    if (index >= 0)
                        s2 = s2.substring(0, index);
                    set.add(s + s2);
                }
            }
        }
        return set;
    }
	
    private void addPrefix(int index) {
        int parentNodeIndex = 0;
        int lastParentIndex = -1;

        while (true) {
            SuffixTreeEdge<C> edge = null;
            parentNodeIndex = currentNode;
            if (isExplicit()) {
                edge = SuffixTreeEdge.find(this, currentNode, getCharacters()[index]);
                if (edge != null) {
                    // SuffixTreeEdge already exists
                    break;
                }
            } else {
                // Implicit node, a little more complicated
                edge = SuffixTreeEdge.find(this, currentNode, getCharacters()[firstCharIndex]);
                int span = lastCharIndex - firstCharIndex;
                if (getCharacters()[edge.getFirstCharIndex() + span + 1] == getCharacters()[index]) {
                    // If the edge is the last char, don't split
                    break;
                }
                parentNodeIndex = edge.split(currentNode, firstCharIndex, lastCharIndex);
            }
            edge = new SuffixTreeEdge<C>(this, index, getCharacters().length - 1, parentNodeIndex);
            if (lastParentIndex > 0) {
                // Last parent is not root, create a link.
                getLinksMap().get(lastParentIndex).setSuffixNode(parentNodeIndex);
            }
            lastParentIndex = parentNodeIndex;
            if (currentNode == 0) {
                firstCharIndex++;
            } else {
                // Current node is not root, follow link
                currentNode = getLinksMap().get(currentNode).getSuffixNode();
            }
            if (!isExplicit())
                canonize();
        }
        if (lastParentIndex > 0) {
            // Last parent is not root, create a link.
            getLinksMap().get(lastParentIndex).setSuffixNode(parentNodeIndex);
        }
        lastParentIndex = parentNodeIndex;
        lastCharIndex++; // Now the endpoint is the next active point
        if (!isExplicit())
            canonize();
    }

    /**
     * Is the tree explicit
     * 
     * @return True if explicit.
     */
    private boolean isExplicit() {
        return firstCharIndex > lastCharIndex;
    }

    /**
     * Canonize the tree.
     */
    private void canonize() {
        SuffixTreeEdge<C> edge = SuffixTreeEdge.find(this, currentNode, getCharacters()[firstCharIndex]);
        int edgeSpan = edge.getLastCharIndex() - edge.getFirstCharIndex();
        while (edgeSpan <= (lastCharIndex - firstCharIndex)) {
            firstCharIndex = firstCharIndex + edgeSpan + 1;
            currentNode = edge.getEndNode();
            if (firstCharIndex <= lastCharIndex) {
                edge = SuffixTreeEdge.find(this, edge.getEndNode(), getCharacters()[firstCharIndex]);
                edgeSpan = edge.getLastCharIndex() - edge.getFirstCharIndex();
            }
        }
    }

    /**
     * Returns a two element int array who's 0th index is the start index and
     * 1th is the end index.
     */
    private int[] searchEdges(char[] query) {
        int startNode = 0;
        int queryPosition = 0;
        int startIndex = -1;
        int endIndex = -1;
        boolean stop = false;

        while (!stop && queryPosition < query.length) {
            SuffixTreeEdge<C> edge = SuffixTreeEdge.find(this, startNode, query[queryPosition]);
            if (edge == null) {
                stop = true;
                break;
            }
            if (startNode == 0)
                startIndex = edge.getFirstCharIndex();
            for (int i = edge.getFirstCharIndex(); i <= edge.getLastCharIndex(); i++) {
                if (queryPosition >= query.length) {
                    stop = true;
                    break;
                } else if (query[queryPosition] == getCharacters()[i]) {
                    queryPosition++;
                    endIndex = i;
                } else {
                    stop = true;
                    break;
                }
            }
            if (!stop) { // proceed with next node
                startNode = edge.getEndNode();
                if (startNode == -1)
                    stop = true;
            }
        }
        return (new int[] { startIndex, endIndex });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("String = ").append(this.getString()).append("\n");
        builder.append("End of word character = ").append(getEndSeqChar()).append("\n");
        builder.append(SuffixTreePrinter.getString(this));
        return builder.toString();
    }

	public String getString() {
		return string;
	}

	public void setString(String string) {
		this.string = string;
	}

	public char getEndSeqChar() {
		return endSeqChar;
	}

	public Map<Integer, SuffixTreeEdge<C>> getEdgeMap() {
		return edgeMap;
	}

	public void setEdgeMap(Map<Integer, SuffixTreeEdge<C>> edgeMap) {
		this.edgeMap = edgeMap;
	}

	public char[] getCharacters() {
		return characters;
	}

	public void setCharacters(char[] characters) {
		this.characters = characters;
	}

	public Map<Integer, SuffixTreeLink> getLinksMap() {
		return linksMap;
	}

	public void setLinksMap(Map<Integer, SuffixTreeLink> linksMap) {
		this.linksMap = linksMap;
	}
}
