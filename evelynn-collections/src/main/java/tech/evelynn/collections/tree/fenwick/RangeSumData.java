package tech.evelynn.collections.tree.fenwick;

import java.math.BigDecimal;
import java.math.BigInteger;

@SuppressWarnings("unchecked")
public class RangeSumData<N extends Number> extends FenwickData {

	public N sum = null;

	public RangeSumData(int index, N number) {
		super(index);
		this.sum = number;
	}

	@Override
	public void clear() {
		super.clear();
		sum = null;
	}

	@Override
	public FenwickData combined(FenwickData data) {
		RangeSumData<N> q = null;
		if (data instanceof RangeSumData) {
			q = (RangeSumData<N>) data;
			this.combined(q);
		}
		return this;
	}

	@Override
	public FenwickData separate(FenwickData data) {
		RangeSumData<N> q = null;
		if (data instanceof RangeSumData) {
			q = (RangeSumData<N>) data;
			this.separate(q);
		}
		return this;
	}

	private void combined(RangeSumData<N> data) {
		if (this.sum == null && data.sum == null)
			return;
		else if (this.sum != null && data.sum == null)
			return;
		else if (this.sum == null && data.sum != null)
			this.sum = data.sum;
		else {
			/* TODO: This is ugly and how to handle number overflow? */
			if (this.sum instanceof BigDecimal || data.sum instanceof BigDecimal) {
				BigDecimal result = ((BigDecimal) this.sum).add((BigDecimal) data.sum);
				this.sum = (N) result;
			} else if (this.sum instanceof BigInteger || data.sum instanceof BigInteger) {
				BigInteger result = ((BigInteger) this.sum).add((BigInteger) data.sum);
				this.sum = (N) result;
			} else if (this.sum instanceof Long || data.sum instanceof Long) {
				Long result = (this.sum.longValue() + data.sum.longValue());
				this.sum = (N) result;
			} else if (this.sum instanceof Double || data.sum instanceof Double) {
				Double result = (this.sum.doubleValue() + data.sum.doubleValue());
				this.sum = (N) result;
			} else if (this.sum instanceof Float || data.sum instanceof Float) {
				Float result = (this.sum.floatValue() + data.sum.floatValue());
				this.sum = (N) result;
			} else {
				// Integer
				Integer result = (this.sum.intValue() + data.sum.intValue());
				this.sum = (N) result;
			}
		}
	}

	private void separate(RangeSumData<N> data) {
		if (this.sum == null && data.sum == null)
			return;
		else if (this.sum != null && data.sum == null)
			return;
		else if (this.sum == null && data.sum != null)
			this.sum = data.sum;
		else {
			/* TODO: This is ugly and how to handle number overflow? */
			if (this.sum instanceof BigDecimal || data.sum instanceof BigDecimal) {
				BigDecimal result = ((BigDecimal) this.sum).subtract((BigDecimal) data.sum);
				this.sum = (N) result;
			} else if (this.sum instanceof BigInteger || data.sum instanceof BigInteger) {
				BigInteger result = ((BigInteger) this.sum).subtract((BigInteger) data.sum);
				this.sum = (N) result;
			} else if (this.sum instanceof Long || data.sum instanceof Long) {
				Long result = (this.sum.longValue() - data.sum.longValue());
				this.sum = (N) result;
			} else if (this.sum instanceof Double || data.sum instanceof Double) {
				Double result = (this.sum.doubleValue() - data.sum.doubleValue());
				this.sum = (N) result;
			} else if (this.sum instanceof Float || data.sum instanceof Float) {
				Float result = (this.sum.floatValue() - data.sum.floatValue());
				this.sum = (N) result;
			} else {
				// Integer
				Integer result = (this.sum.intValue() - data.sum.intValue());
				this.sum = (N) result;
			}
		}
	}

	@Override
	public FenwickData copy() {
		return new RangeSumData<N>(index, sum);
	}

	@Override
	public FenwickData query(long startOfQuery, long endOfQuery) {
		if (endOfQuery < this.index || startOfQuery > this.index)
			return null;
		return copy();
	}

	@Override
	public int hashCode() {
		return 31 * (int) (this.index + this.sum.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof RangeSumData))
			return false;

		RangeSumData<N> data = (RangeSumData<N>) obj;
		if (this.index == data.index && this.sum.equals(data.sum))
			return true;
		return false;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append(" ");
		builder.append("sum=").append(sum);
		return builder.toString();
	}
}
