package tech.evelynn.collections.tree.segment;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("unchecked")
public final class IntervalData<O extends Object> extends SegmentTreeData {

	private Set<O> set = new TreeSet<O>(); // Sorted

	public IntervalData(long index, O object) {
		super(index);
		this.set.add(object);
	}

	public IntervalData(long start, long end, O object) {
		super(start, end);
		this.set.add(object);
	}

	public IntervalData(long start, long end, Set<O> set) {
		super(start, end);
		this.set = set;
	}

	public Collection<O> getData() {
		return Collections.unmodifiableCollection(this.set);
	}

	@Override
	public void clear() {
		super.clear();
		this.set.clear();
	}

	@Override
	public SegmentTreeData copy() {
		final Set<O> listCopy = new TreeSet<O>();
		listCopy.addAll(set);
		return new IntervalData<O>(start, end, listCopy);
	}

	@Override
	public SegmentTreeData query(long startOfQuery, long endOfQuery) {
		if (endOfQuery < this.start || startOfQuery > this.end)
			return null;
		return copy();
	}

	@Override
	public SegmentTreeData combined(SegmentTreeData data) {
		IntervalData<O> q = null;
		if (data instanceof IntervalData) {
			q = (IntervalData<O>) data;
			this.combined(q);
		}
		return this;
	}

	private void combined(IntervalData<O> data) {
		if (data.start < this.start)
			this.start = data.start;
		if (data.end > this.end)
			this.end = data.end;
		this.set.addAll(data.set);
	}

	@Override
	public int hashCode() {
		return 31 * (int) (this.start + this.end + this.set.size());
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof IntervalData))
			return false;

		final IntervalData<O> data = (IntervalData<O>) obj;
		if (this.start == data.start && this.end == data.end) {
			if (this.set.size() != data.set.size())
				return false;
			for (O o : set) {
				if (!data.set.contains(o))
					return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(super.toString()).append(" ");
		builder.append("set=").append(set);
		return builder.toString();
	}
}
