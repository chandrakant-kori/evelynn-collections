package tech.evelynn.collections.tree.bst;

public class SplayTree<T extends Comparable<T>> extends BinarySearchTree<T> {

	@Override
	protected BSTNode<T> addValue(T id) {
		BSTNode<T> nodeToReturn = super.addValue(id);
		BSTNode<T> nodeAdded = nodeToReturn;
		if (nodeAdded != null) {
			// Splay the new node to the root position
			while (nodeAdded.getParent() != null) {
				this.splay(nodeAdded);
			}
		}
		return nodeToReturn;
	}

	@Override
	protected BSTNode<T> removeValue(T value) {
		BSTNode<T> nodeToRemove = super.removeValue(value);
		if (nodeToRemove != null && nodeToRemove.getParent() != null) {
			BSTNode<T> nodeParent = nodeToRemove.getParent();
			// Splay the parent node to the root position
			while (nodeParent.getParent() != null) {
				this.splay(nodeParent);
			}
		}
		return nodeToRemove;
	}

	@Override
	public boolean contains(T value) {
		BSTNode<T> node = getNode(value);
		if (node != null) {
			// Splay the new node to the root position
			while (node.getParent() != null) {
				this.splay(node);
			}
			return true;
		}
		return false;
	}

	private void splay(BSTNode<T> node) {
		BSTNode<T> parent = node.getParent();
		BSTNode<T> grandParent = (parent != null) ? parent.getParent() : null;
		if (parent != null && parent == getRoot()) {
			grandParent = parent.getParent();
			// Zig step
			setRoot(node);
			node.setParent(null);

			if (parent != null) {
				if (node == parent.getLesser()) {
					parent.setLesser(node.getGreater());
					if (node.getGreater() != null)
						node.getGreater().setParent(parent);

					node.setGreater(parent);
					parent.setParent(node);
				} else {
					parent.setGreater(node.getLesser());
					if (node.getLesser() != null)
						node.getLesser().setParent(parent);

					node.setLesser(parent);
					parent.setParent(node);
				}
			}
			return;
		}

		if (parent != null && grandParent != null) {
			BSTNode<T> greatGrandParent = grandParent.getParent();
			if (greatGrandParent != null && greatGrandParent.getLesser() == grandParent) {
				greatGrandParent.setLesser(node);
				node.setParent(greatGrandParent);
			} else if (greatGrandParent != null && greatGrandParent.getGreater() == grandParent) {
				greatGrandParent.setGreater(node);
				node.setParent(greatGrandParent);
			} else {
				// I am now root!
				setRoot(node);
				node.setParent(null);
			}

			if ((node == parent.getLesser() && parent == grandParent.getLesser())
					|| (node == parent.getGreater() && parent == grandParent.getGreater())) {
				// Zig-zig step
				if (node == parent.getLesser()) {
					BSTNode<T> nodeGreater = node.getGreater();
					node.setGreater(parent);
					parent.setParent(node);

					parent.setLesser(nodeGreater);
					if (nodeGreater != null)
						nodeGreater.setParent(parent);

					BSTNode<T> parentGreater = parent.getGreater();
					parent.setGreater(grandParent);
					grandParent.setParent(parent);

					grandParent.setLesser(parentGreater);
					if (parentGreater != null)
						parentGreater.setParent(grandParent);
				} else {
					BSTNode<T> nodeLesser = node.getLesser();
					node.setLesser(parent);
					parent.setParent(node);

					parent.setGreater(nodeLesser);
					if (nodeLesser != null)
						nodeLesser.setParent(parent);

					BSTNode<T> parentLesser = parent.getLesser();
					parent.setLesser(grandParent);
					grandParent.setParent(parent);

					grandParent.setGreater(parentLesser);
					if (parentLesser != null)
						parentLesser.setParent(grandParent);
				}
				return;
			}

			// Zig-zag step
			if (node == parent.getLesser()) {
				BSTNode<T> nodeLesser = node.getGreater();
				BSTNode<T> nodeGreater = node.getLesser();

				node.setGreater(parent);
				parent.setParent(node);

				node.setLesser(grandParent);
				grandParent.setParent(node);

				parent.setLesser(nodeLesser);
				if (nodeLesser != null)
					nodeLesser.setParent(parent);

				grandParent.setGreater(nodeGreater);
				if (nodeGreater != null)
					nodeGreater.setParent(grandParent);
				return;
			}

			BSTNode<T> nodeLesser = node.getLesser();
			BSTNode<T> nodeGreater = node.getGreater();

			node.setLesser(parent);
			parent.setParent(node);

			node.setGreater(grandParent);
			grandParent.setParent(node);

			parent.setGreater(nodeLesser);
			if (nodeLesser != null)
				nodeLesser.setParent(parent);

			grandParent.setLesser(nodeGreater);
			if (nodeGreater != null)
				nodeGreater.setParent(grandParent);
		}
	}
}
