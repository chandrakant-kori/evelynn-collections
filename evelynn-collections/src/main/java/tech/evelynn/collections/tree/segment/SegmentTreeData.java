package tech.evelynn.collections.tree.segment;

public abstract class SegmentTreeData implements Comparable<SegmentTreeData> {

	protected long start = Long.MIN_VALUE;
	protected long end = Long.MAX_VALUE;

	public SegmentTreeData(long index) {
		this.start = index;
		this.end = index;
	}

	public SegmentTreeData(long start, long end) {
		this.start = start;
		this.end = end;
	}

	public void clear() {
		start = Long.MIN_VALUE;
		end = Long.MAX_VALUE;
	}

	public abstract SegmentTreeData combined(SegmentTreeData data);

	public abstract SegmentTreeData copy();

	public abstract SegmentTreeData query(long startOfRange, long endOfRange);

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append(start).append("->").append(end);
		return builder.toString();
	}

	@Override
	public int compareTo(SegmentTreeData d) {
		if (this.end < d.end)
			return -1;
		if (d.end < this.end)
			return 1;
		return 0;
	}

}
