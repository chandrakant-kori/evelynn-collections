package tech.evelynn.collections.tree.segment;

public abstract class Segment<D extends SegmentTreeData> implements Comparable<Segment<D>> {

	protected Segment<D>[] segments = null;
    protected int length = 0;
    protected int half = 0;
    protected long start = 0;
    protected long end = 0;
    protected D data = null;
    protected int minLength = 0;

    public Segment(int minLength) {
        this.minLength = minLength;
    }

    public abstract D query(long startOfQuery, long endOfQuery);

    protected boolean hasChildren() {
        return (segments != null);
    }

    protected Segment<D> getLeftChild() {
        return segments[0];
    }

    protected Segment<D> getRightChild() {
        return segments[1];
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(start).append("->");
        builder.append(end).append(" ");
        builder.append("Length=").append(length).append(" ");
        builder.append("Data={").append(data).append("}");
        return builder.toString();
    }

    @Override
    public int compareTo(Segment<D> p) {
        if (this.end < p.end)
            return -1;
        if (p.end < this.end)
            return 1;
        return 0;
    }
    
    
	
}
