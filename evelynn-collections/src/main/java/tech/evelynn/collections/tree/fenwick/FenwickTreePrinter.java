package tech.evelynn.collections.tree.fenwick;

import tech.evelynn.collections.list.ArrayList;
import tech.evelynn.collections.list.List;

@SuppressWarnings("unchecked")
public class FenwickTreePrinter {

	public static <D extends FenwickData> String getString(FenwickTree<D> tree) {
        if (tree.getArray().length == 0)
            return "Tree has no nodes.";

		final D first = (D) tree.getArray()[1];
        if (first == null)
            return "Tree has no nodes.";

        final StringBuilder builder = new StringBuilder();
        builder.append("└── dummy \n");
        builder.append(getString(tree, 0, tree.getArray().length, "", true));
        return builder.toString();
    }
	
	private static <D extends FenwickData> String getString(FenwickTree<D> tree, int start, int end, String prefix, boolean isTail) {
        if (end > tree.getArray().length || (end - start == 0))
            return "";

        final StringBuilder builder = new StringBuilder();

        final D value = (D) tree.getArray()[start];
        if (value != null)
            builder.append(prefix + (isTail ? "└── " : "├── ") + value + "\n");

        int next = start + 1;
        final List<Integer> children = new ArrayList<Integer>(2);
        while (next < end) {
            children.add(next);
            next = FenwickTree.next(next);
        }
        for (int i = 0; i < children.size() - 1; i++)
            builder.append(getString(tree, children.toList().get(i),children.toList().get(i+1), prefix + (isTail ? "    " : "│   "), false));
        if (children.size() >= 1)
            builder.append(getString(tree, children.toList().get(children.size()-1), end, prefix + (isTail ? "    " : "│   "), true));
        return builder.toString();
    }
	
}
