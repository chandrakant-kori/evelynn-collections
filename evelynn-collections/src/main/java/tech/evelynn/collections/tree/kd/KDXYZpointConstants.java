package tech.evelynn.collections.tree.kd;

import java.util.Comparator;

public class KDXYZpointConstants {

	protected static final int X_AXIS = 0;
    protected static final int Y_AXIS = 1;
    protected static final int Z_AXIS = 2;
	
	public static final Comparator<KDXYZPoint> X_COMPARATOR = new Comparator<KDXYZPoint>() {

		@Override
		public int compare(KDXYZPoint o1, KDXYZPoint o2) {
			if (o1.x < o2.x)
				return -1;
			if (o1.x > o2.x)
				return 1;
			return 0;
		}
	};

	public static final Comparator<KDXYZPoint> Y_COMPARATOR = new Comparator<KDXYZPoint>() {

		@Override
		public int compare(KDXYZPoint o1, KDXYZPoint o2) {
			if (o1.y < o2.y)
				return -1;
			if (o1.y > o2.y)
				return 1;
			return 0;
		}
	};

	public static final Comparator<KDXYZPoint> Z_COMPARATOR = new Comparator<KDXYZPoint>() {

		@Override
		public int compare(KDXYZPoint o1, KDXYZPoint o2) {
			if (o1.z < o2.z)
				return -1;
			if (o1.z > o2.z)
				return 1;
			return 0;
		}
	};
}
