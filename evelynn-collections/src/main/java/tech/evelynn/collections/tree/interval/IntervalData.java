package tech.evelynn.collections.tree.interval;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("unchecked")
public class IntervalData<O> implements Comparable<IntervalData<O>> {

	private long start = Long.MIN_VALUE;
	private long end = Long.MAX_VALUE;
	private Set<O> set = new HashSet<O>();

	public IntervalData(long index, O object) {
		this.start = index;
		this.end = index;
		this.set.add(object);
	}

	public IntervalData(long start, long end, O object) {
		this.start = start;
		this.end = end;
		this.set.add(object);
	}

	public IntervalData(long start, long end, Set<O> set) {
		this.start = start;
		this.end = end;
		this.set = set;
	}

	public long getStart() {
		return start;
	}

	public long getEnd() {
		return end;
	}

	public Collection<O> getData() {
		return Collections.unmodifiableCollection(this.set);
	}

	public void clear() {
		this.start = Long.MIN_VALUE;
		this.end = Long.MAX_VALUE;
		this.set.clear();
	}

	public IntervalData<O> combined(IntervalData<O> data) {
		if (data.start < this.start)
			this.start = data.start;
		if (data.end > this.end)
			this.end = data.end;
		this.set.addAll(data.set);
		return this;
	}

	public IntervalData<O> copy() {
		Set<O> copy = new HashSet<O>();
		copy.addAll(set);
		return new IntervalData<O>(start, end, copy);
	}

	public IntervalData<O> query(long index) {
		if (index < this.start || index > this.end)
			return null;

		return copy();
	}

	public IntervalData<O> query(long startOfQuery, long endOfQuery) {
		if (endOfQuery < this.start || startOfQuery > this.end)
			return null;

		return copy();
	}

	@Override
	public int hashCode() {
		return 31 * ((int) (this.start + this.end)) + this.set.size();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof IntervalData))
			return false;

		IntervalData<O> data = (IntervalData<O>) obj;
		if (this.start == data.start && this.end == data.end) {
			if (this.set.size() != data.set.size())
				return false;
			for (O o : set) {
				if (!data.set.contains(o))
					return false;
			}
			return true;
		}
		return false;
	}

	@Override
	public int compareTo(IntervalData<O> d) {
		if (this.end < d.end)
			return -1;
		if (d.end < this.end)
			return 1;
		return 0;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(start).append("->").append(end);
		builder.append(" set=").append(set);
		return builder.toString();
	}
}
