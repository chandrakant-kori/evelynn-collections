package tech.evelynn.collections.tree.bst;

import tech.evelynn.collections.list.ArrayList;

public class BinaryTreePrinter {

	public static <T extends Comparable<T>> String getString(BinarySearchTree<T> tree) {
		if (tree.getRoot() == null)
			return "Tree has no nodes.";
		return getString(tree.getRoot(), "", true);
	}

	private static <T extends Comparable<T>> String getString(BSTNode<T> node, String prefix, boolean isTail) {
		StringBuilder builder = new StringBuilder();

		if (node.getParent() != null) {
			String side = "left";
			if (node.equals(node.getParent().getGreater()))
				side = "right";
			builder.append(prefix + (isTail ? "└── " : "├── ") + "(" + side + ") " + node.getId() + "\n");
		} else {
			builder.append(prefix + (isTail ? "└── " : "├── ") + node.getId() + "\n");
		}
		ArrayList<BSTNode<T>> children = null;
		if (node.getLesser() != null || node.getGreater() != null) {
			children = new ArrayList<BSTNode<T>>(2);
			if (node.getLesser() != null)
				children.add(node.getLesser());
			if (node.getGreater() != null)
				children.add(node.getGreater());
		}
		if (children != null) {
			for (int i = 0; i < children.size() - 1; i++) {
				builder.append(getString(children.get(i), prefix + (isTail ? "    " : "│   "), false));
			}
			if (children.size() >= 1) {
				builder.append(getString(children.get(children.size() - 1), prefix + (isTail ? "    " : "│   "), true));
			}
		}

		return builder.toString();
	}
}
