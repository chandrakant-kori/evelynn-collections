package tech.evelynn.collections.tree.segment;

import java.util.ArrayList;
import java.util.List;

public class SegmentTreePrinter {

	public static <D extends SegmentTreeData> String getString(SegmentTree<D> tree) {
        if (tree.root == null)
            return "Tree has no nodes.";
        return getString(tree.root, "", true);
    }

    private static <D extends SegmentTreeData> String getString(Segment<D> segment, String prefix, boolean isTail) {
        final StringBuilder builder = new StringBuilder();
        builder.append(prefix + (isTail ? "└── " : "├── ") + segment.toString() + "\n");

        final List<Segment<D>> children = new ArrayList<Segment<D>>();
        if (segment.segments != null) {
            for (Segment<D> c : segment.segments)
                children.add(c);
        }

        for (int i = 0; i < children.size() - 1; i++)
            builder.append(getString(children.get(i), prefix + (isTail ? "    " : "│   "), false));

        if (children.size() > 1)
            builder.append(getString(children.get(children.size() - 1), prefix + (isTail ? "    " : "│   "), true));

        return builder.toString();
    }
	
}
