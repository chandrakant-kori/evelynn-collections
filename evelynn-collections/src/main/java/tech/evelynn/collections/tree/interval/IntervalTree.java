package tech.evelynn.collections.tree.interval;

import java.util.ArrayList;
import java.util.List;

public class IntervalTree<O extends Object> {

	private Interval<O> root = null;

	public IntervalTree(List<IntervalData<O>> intervals) {
        if (intervals.size() <= 0)
            return;

        setRoot(createFromList(intervals));
    }

	protected static final <O extends Object> Interval<O> createFromList(List<IntervalData<O>> intervals) {
        Interval<O> newInterval = new Interval<O>();
        if (intervals.size()==1) {
        	IntervalData<O> middle = intervals.get(0);
        	newInterval.setCenter(((middle.getStart() + middle.getEnd()) / 2));
        	newInterval.add(middle);
        	return newInterval;
        }

        int half = intervals.size() / 2;
        IntervalData<O> middle = intervals.get(half);
        newInterval.setCenter(((middle.getStart() + middle.getEnd()) / 2));
        List<IntervalData<O>> leftIntervals = new ArrayList<IntervalData<O>>();
        List<IntervalData<O>> rightIntervals = new ArrayList<IntervalData<O>>();
        for (IntervalData<O> interval : intervals) {
        	if (interval.getEnd() < newInterval.getCenter()) {
                leftIntervals.add(interval);
            } else if (interval.getStart() > newInterval.getCenter()) {
                rightIntervals.add(interval);
            } else {
                newInterval.add(interval);
            }
        }

        if (leftIntervals.size() > 0)
            newInterval.setLeft(createFromList(leftIntervals));
        if (rightIntervals.size() > 0)
            newInterval.setRight(createFromList(rightIntervals));

        return newInterval;
    }
	
	public IntervalData<O> query(long index) {
        return getRoot().query(index);
    }
	
	public IntervalData<O> query(long start, long end) {
        return getRoot().query(start, end);
    }
	
	@Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(IntervalTreePrinter.getString(this));
        return builder.toString();
    }

	public Interval<O> getRoot() {
		return root;
	}

	public void setRoot(Interval<O> root) {
		this.root = root;
	}

}
