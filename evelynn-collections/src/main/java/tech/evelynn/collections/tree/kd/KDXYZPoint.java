package tech.evelynn.collections.tree.kd;

public class KDXYZPoint implements Comparable<KDXYZPoint> {

	protected final double x;
	protected final double y;
	protected final double z;

	public KDXYZPoint(double x, double y) {
		this.x = x;
		this.y = y;
		this.z = 0;
	}

	public KDXYZPoint(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public KDXYZPoint(Double latitude, Double longitude) {
		x = Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(longitude));
		y = Math.cos(Math.toRadians(latitude)) * Math.sin(Math.toRadians(longitude));
		z = Math.sin(Math.toRadians(latitude));
	}

	public double euclideanDistance(KDXYZPoint o1) {
		return euclideanDistance(o1, this);
	}

	private static final double euclideanDistance(KDXYZPoint o1, KDXYZPoint o2) {
		return Math.sqrt(Math.pow((o1.x - o2.x), 2) + Math.pow((o1.y - o2.y), 2) + Math.pow((o1.z - o2.z), 2));
	}

	@Override
	public int hashCode() {
		return 31 * (int) (this.x + this.y + this.z);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof KDXYZPoint))
			return false;

		KDXYZPoint xyzPoint = (KDXYZPoint) obj;
		if (Double.compare(this.x, xyzPoint.x) != 0)
			return false;
		if (Double.compare(this.y, xyzPoint.y) != 0)
			return false;
		if (Double.compare(this.z, xyzPoint.z) != 0)
			return false;
		return true;
	}

	@Override
	public int compareTo(KDXYZPoint o) {
		int xComp = KDXYZpointConstants.X_COMPARATOR.compare(this, o);
		if (xComp != 0)
			return xComp;
		int yComp = KDXYZpointConstants.Y_COMPARATOR.compare(this, o);
		if (yComp != 0)
			return yComp;
		int zComp = KDXYZpointConstants.Z_COMPARATOR.compare(this, o);
		return zComp;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getZ() {
		return z;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("(");
		builder.append(x).append(", ");
		builder.append(y).append(", ");
		builder.append(z);
		builder.append(")");
		return builder.toString();
	}
}
