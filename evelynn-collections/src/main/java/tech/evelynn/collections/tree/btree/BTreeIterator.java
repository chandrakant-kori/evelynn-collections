package tech.evelynn.collections.tree.btree;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class BTreeIterator<C extends Comparable<C>> implements Iterator<C> {

	private BTree<C> tree = null;
	private BTreeNode<C> lastNode = null;
	private C lastValue = null;
	private int index = 0;
	private Deque<BTreeNode<C>> toVisit = new ArrayDeque<BTreeNode<C>>();

	protected BTreeIterator(BTree<C> tree) {
		this.tree = tree;
		if (tree.getRoot() != null && tree.getRoot().getKeysSize() > 0) {
			getToVisit().add(tree.getRoot());
		}
	}

	@Override
	public boolean hasNext() {
		if ((lastNode != null && index < lastNode.getKeysSize()) || (getToVisit().size() > 0))
			return true;
		return false;
	}

	@Override
	public C next() {
		if (lastNode != null && (index < lastNode.getKeysSize())) {
			lastValue = lastNode.getKey(index++);
			return lastValue;
		}
		while (getToVisit().size() > 0) {
			// Go thru the current nodes
			BTreeNode<C> n = getToVisit().pop();

			// Add non-null children
			for (int i = 0; i < n.getChildrenSize(); i++) {
				getToVisit().add(n.getChild(i));
			}

			// Update last node (used in remove method)
			index = 0;
			lastNode = n;
			lastValue = lastNode.getKey(index++);
			return lastValue;
		}
		return null;
	}

	@Override
	public void remove() {
		if (lastNode != null && lastValue != null) {
			// On remove, reset the iterator (very inefficient, I know)
			tree.remove(lastValue, lastNode);

			lastNode = null;
			lastValue = null;
			index = 0;
			getToVisit().clear();
			if (tree.getRoot() != null && tree.getRoot().getKeysSize() > 0) {
				getToVisit().add(tree.getRoot());
			}
		}
	}

	public Deque<BTreeNode<C>> getToVisit() {
		return toVisit;
	}

	public void setToVisit(Deque<BTreeNode<C>> toVisit) {
		this.toVisit = toVisit;
	}
}
