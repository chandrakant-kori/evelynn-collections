package tech.evelynn.collections.tree.rbt;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

import tech.evelynn.collections.tree.bst.BSTNode;

public class RedBlackTreeIterator<C extends Comparable<C>> implements Iterator<C> {

	private RedBlackTree<C> tree = null;
	private BSTNode<C> last = null;
	private Deque<BSTNode<C>> toVisit = new ArrayDeque<BSTNode<C>>();

	protected RedBlackTreeIterator(RedBlackTree<C> tree) {
		this.tree = tree;
		if (tree.getRoot() != null) {
			toVisit.add(tree.getRoot());
		}
	}

	@Override
	public boolean hasNext() {
		if (toVisit.size() > 0)
			return true;
		return false;
	}

	@Override
	public C next() {
		while (toVisit.size() > 0) {
			// Go thru the current nodes
			BSTNode<C> n = toVisit.pop();

			// Add non-null children
			if (n.getLesser() != null && n.getLesser().getId() != null) {
				toVisit.add(n.getLesser());
			}
			if (n.getGreater() != null && n.getGreater().getId() != null) {
				toVisit.add(n.getGreater());
			}

			last = n;
			return n.getId();
		}
		return null;
	}

	@Override
	public void remove() {
		tree.removeNode(last);
	}

}
