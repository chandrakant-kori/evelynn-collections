package tech.evelynn.collections.tree.btree;

public class BTreePrinter {

	public static <T extends Comparable<T>> String getString(BTree<T> tree) {
        if (tree.getRoot() == null) return "Tree has no nodes.";
        return getString(tree.getRoot(), "", true);
    }
	
	private static <T extends Comparable<T>> String getString(BTreeNode<T> bTreeNode, String prefix, boolean isTail) {
        StringBuilder builder = new StringBuilder();

        builder.append(prefix).append((isTail ? "└── " : "├── "));
        for (int i = 0; i < bTreeNode.numberOfKeys(); i++) {
            T value = bTreeNode.getKey(i);
            builder.append(value);
            if (i < bTreeNode.numberOfKeys() - 1)
                builder.append(", ");
        }
        builder.append("\n");

        if (bTreeNode.getChildren() != null) {
            for (int i = 0; i < bTreeNode.numberOfChildren() - 1; i++) {
                BTreeNode<T> obj = bTreeNode.getChild(i);
                builder.append(getString(obj, prefix + (isTail ? "    " : "│   "), false));
            }
            if (bTreeNode.numberOfChildren() >= 1) {
                BTreeNode<T> obj = bTreeNode.getChild(bTreeNode.numberOfChildren() - 1);
                builder.append(getString(obj, prefix + (isTail ? "    " : "│   "), true));
            }
        }

        return builder.toString();
    }
	
	

}
