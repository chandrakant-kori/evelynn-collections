package tech.evelynn.collections.tree.fenwick;

public abstract class FenwickData implements Comparable<FenwickData> {

	protected int index = Integer.MIN_VALUE;

	protected FenwickData(int index) {
		this.index = index;
	}

	public void clear() {
		index = Integer.MIN_VALUE;
	}

	public abstract FenwickData combined(FenwickData data);

	public abstract FenwickData separate(FenwickData data);

	public abstract FenwickData copy();

	public abstract FenwickData query(long startOfRange, long endOfRange);

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("[").append(index).append("]");
		return builder.toString();
	}

	@Override
	public int compareTo(FenwickData d) {
		if (this.index < d.index)
			return -1;
		if (d.index < this.index)
			return 1;
		return 0;
	}

}
