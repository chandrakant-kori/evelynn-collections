package tech.evelynn.collections.tree.suffix;

public class SuffixTreeLink implements Comparable<SuffixTreeLink> {

	private int node = 0;
	private int suffixNode = -1;

	public SuffixTreeLink(int node) {
		this.node = node;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("node=").append(node).append("\n");
		builder.append("suffixNode=").append(getSuffixNode()).append("\n");
		return builder.toString();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(SuffixTreeLink link) {
		if (link == null)
			return -1;

		if (node < link.node)
			return -1;
		if (node > link.node)
			return 1;

		if (getSuffixNode() < link.getSuffixNode())
			return -1;
		if (getSuffixNode() > link.getSuffixNode())
			return 1;

		return 0;
	}

	public int getSuffixNode() {
		return suffixNode;
	}

	public void setSuffixNode(int suffixNode) {
		this.suffixNode = suffixNode;
	}

}
