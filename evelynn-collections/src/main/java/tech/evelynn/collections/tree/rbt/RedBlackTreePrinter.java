package tech.evelynn.collections.tree.rbt;

import tech.evelynn.collections.list.ArrayList;
import tech.evelynn.collections.list.List;
import tech.evelynn.collections.tree.bst.BSTNode;

public class RedBlackTreePrinter {

	public static <T extends Comparable<T>> String getString(RedBlackTree<T> tree) {
        if (tree.getRoot() == null)
            return "Tree has no nodes.";
        return getString((RedBlackNode<T>) tree.getRoot(), "", true);
    }
	
	public static <T extends Comparable<T>> String getString(RedBlackNode<T> node) {
        if (node == null)
            return "Sub-tree has no nodes.";
        return getString(node, "", true);
    }
	
	private static <T extends Comparable<T>> String getString(RedBlackNode<T> node, String prefix, boolean isTail) {
        StringBuilder builder = new StringBuilder();

        builder.append(prefix + (isTail ? "└── " : "├── ") + "(" + ((node.color == RedBlackTreeConstants.RED) ? "RED" : "BLACK") + ") " + node.getId()
                       + " [parent=" + ((node.getParent()!=null)?node.getParent().getId():"NULL") 
                       + " grand-parent=" + ((node.getParent()!=null && node.getParent().getParent()!=null)?node.getParent().getParent().getId():"NULL")
                       + "]\n"
        );
        List<BSTNode<T>> children = null;
        if (node.getLesser() != null || node.getGreater() != null) {
            children = new ArrayList<BSTNode<T>>(2);
            if (node.getLesser() != null)
                children.add(node.getLesser());
            if (node.getGreater() != null)
                children.add(node.getGreater());
        }
        if (children != null) {
            for (int i = 0; i < children.size() - 1; i++) {
                builder.append(getString((RedBlackNode<T>) children.toList().get(i), prefix + (isTail ? "    " : "│   "), false));
            }
            if (children.size() >= 1) {
                builder.append(getString((RedBlackNode<T>) children.toList().get(children.size() - 1), prefix + (isTail ? "    " : "│   "), true));
            }
        }

        return builder.toString();
    }
}
