package tech.evelynn.collections.tree.rbt;

public class RedBlackTreeConstants {

	public static final boolean BLACK = false;
	public static final boolean RED = true;

}
