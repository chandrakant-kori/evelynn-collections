package tech.evelynn.collections.tree.tst;

public interface TSTNodeCreator {

	public TSTNode createNewNode(TSTNode parent, Character character, boolean isWord);
}
