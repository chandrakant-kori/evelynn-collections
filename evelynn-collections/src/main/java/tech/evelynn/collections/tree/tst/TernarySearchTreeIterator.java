package tech.evelynn.collections.tree.tst;

import java.util.Iterator;

@SuppressWarnings("unchecked")
public class TernarySearchTreeIterator<C extends CharSequence> implements Iterator<C> {

	private TernarySearchTree<C> tree = null;
	private TSTNode lastNode = null;
	private java.util.Iterator<java.util.Map.Entry<TSTNode, String>> iterator = null;

	public TernarySearchTreeIterator(TernarySearchTree<C> tree) {
		this.tree = tree;
		java.util.Map<TSTNode, String> map = new java.util.LinkedHashMap<TSTNode, String>();
		if (this.tree.root != null) {
			getNodesWhichRepresentsWords(this.tree.root, "", map);
		}
		iterator = map.entrySet().iterator();
	}

	private void getNodesWhichRepresentsWords(TSTNode node, String string, java.util.Map<TSTNode, String> nodesMap) {
		final StringBuilder builder = new StringBuilder(string);
		if (node.isWord())
			nodesMap.put(node, builder.toString());
		if (node.loKid != null) {
			TSTNode child = node.loKid;
			getNodesWhichRepresentsWords(child, builder.toString(), nodesMap);
		}
		if (node.kid != null) {
			TSTNode child = node.kid;
			getNodesWhichRepresentsWords(child, builder.append(node.getCharacter()).toString(), nodesMap);
		}
		if (node.hiKid != null) {
			TSTNode child = node.hiKid;
			getNodesWhichRepresentsWords(child, builder.toString(), nodesMap);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasNext() {
		if (iterator != null && iterator.hasNext())
			return true;
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public C next() {
		if (iterator == null)
			return null;

		java.util.Map.Entry<TSTNode, String> entry = iterator.next();
		lastNode = entry.getKey();
		return (C) entry.getValue();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void remove() {
		if (iterator == null || tree == null)
			return;

		iterator.remove();
		this.tree.remove(lastNode);
	}

}
