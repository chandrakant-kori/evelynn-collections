package tech.evelynn.collections.tree.kd;

public class KDNode implements Comparable<KDNode> {

	private final KDXYZPoint id;
	private final int k;
	private final int depth;

	private KDNode parent = null;
	private KDNode lesser = null;
	private KDNode greater = null;

	public KDNode(KDXYZPoint id) {
		this.id = id;
		this.k = 3;
		this.depth = 0;
	}

	public KDNode(KDXYZPoint id, int k, int depth) {
		this.id = id;
		this.k = k;
		this.depth = depth;
	}

	public static int compareTo(int depth, int k, KDXYZPoint o1, KDXYZPoint o2) {
		int axis = depth % k;
		if (axis == KDXYZpointConstants.X_AXIS)
			return KDXYZpointConstants.X_COMPARATOR.compare(o1, o2);
		if (axis == KDXYZpointConstants.Y_AXIS)
			return KDXYZpointConstants.Y_COMPARATOR.compare(o1, o2);
		return KDXYZpointConstants.Z_COMPARATOR.compare(o1, o2);
	}

	@Override
	public int hashCode() {
		return 31 * (this.getK() + this.getDepth() + this.getId().hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!(obj instanceof KDNode))
			return false;

		KDNode KDNode = (KDNode) obj;
		if (this.compareTo(KDNode) == 0)
			return true;
		return false;
	}

	@Override
	public int compareTo(KDNode o) {
		return compareTo(getDepth(), getK(), this.getId(), o.getId());
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("k=").append(getK());
		builder.append(" depth=").append(getDepth());
		builder.append(" id=").append(getId().toString());
		return builder.toString();
	}

	public KDXYZPoint getId() {
		return id;
	}

	public KDNode getLesser() {
		return lesser;
	}

	public void setLesser(KDNode lesser) {
		this.lesser = lesser;
	}

	public KDNode getGreater() {
		return greater;
	}

	public void setGreater(KDNode greater) {
		this.greater = greater;
	}

	public KDNode getParent() {
		return parent;
	}

	public void setParent(KDNode parent) {
		this.parent = parent;
	}

	public int getDepth() {
		return depth;
	}

	public int getK() {
		return k;
	}
}
