package tech.evelynn.collections.tree.rbt;

import tech.evelynn.collections.tree.bst.BSTNode;

public class RedBlackNode<T extends Comparable<T>> extends BSTNode<T> {

	protected boolean color = RedBlackTreeConstants.BLACK;

	protected RedBlackNode(BSTNode<T> parent, T id, boolean color) {
		super(parent, id);
		this.color = color;
	}

	protected RedBlackNode<T> getGrandParent() {
		if (getParent() == null || getParent().getParent() == null)
			return null;
		return (RedBlackNode<T>) getParent().getParent();
	}

	protected RedBlackNode<T> getUncle(RedBlackNode<T> grandParent) {
		if (grandParent == null)
			return null;
		if (grandParent.getLesser() != null && grandParent.getLesser() == getParent()) {
			return (RedBlackNode<T>) grandParent.getGreater();
		} else if (grandParent.getGreater() != null && grandParent.getGreater() == getParent()) {
			return (RedBlackNode<T>) grandParent.getLesser();
		}
		return null;
	}

	protected RedBlackNode<T> getUncle() {
		RedBlackNode<T> grandParent = getGrandParent();
		return getUncle(grandParent);
	}

	protected RedBlackNode<T> getSibling() {
		if (getParent() == null)
			return null;
		if (getParent().getLesser() == this) {
			return (RedBlackNode<T>) getParent().getGreater();
		} else if (getParent().getGreater() == this) {
			return (RedBlackNode<T>) getParent().getLesser();
		} else {
			throw new RuntimeException("Yikes! I'm not related to my parent. " + this.toString());
		}
	}

	protected boolean isLeaf() {
		if (getLesser() != null)
			return false;
		if (getGreater() != null)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "id=" + getId() + " color=" + ((color == RedBlackTreeConstants.RED) ? "RED" : "BLACK") + " isLeaf="
				+ isLeaf() + " parent=" + ((getParent() != null) ? getParent().getId() : "NULL") + " lesser="
				+ ((getLesser() != null) ? getLesser().getId() : "NULL") + " greater="
				+ ((getGreater() != null) ? getGreater().getId() : "NULL");
	}
}
