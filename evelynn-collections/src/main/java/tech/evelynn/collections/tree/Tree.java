package tech.evelynn.collections.tree;

import java.util.Collection;

public interface Tree<T> {

	public boolean add(T value);

	public T remove(T value);

	public void clear();

	public boolean contains(T value);

	public int size();

	public Collection<T> toCollection();

}
