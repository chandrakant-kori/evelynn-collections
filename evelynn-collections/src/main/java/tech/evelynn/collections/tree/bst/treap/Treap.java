package tech.evelynn.collections.tree.bst.treap;

import tech.evelynn.collections.tree.bst.BSTNode;
import tech.evelynn.collections.tree.bst.BSTNodeCreator;
import tech.evelynn.collections.tree.bst.BinarySearchTree;

public class Treap<T extends Comparable<T>> extends BinarySearchTree<T> {

	private static int randomSeed = Integer.MAX_VALUE; // This should be at least twice the number of Nodes

	public Treap() {
		this.creator = new BSTNodeCreator<T>() {
			@Override
			public BSTNode<T> createNewNode(BSTNode<T> parent, T id) {
				return (new TreapNode<T>(parent, id));
			}
		};
	}

	public Treap(int randomSeed) {
		this();
		Treap.randomSeed = randomSeed;
	}

	public Treap(BSTNodeCreator<T> creator) {
		super(creator);
	}

	public Treap(BSTNodeCreator<T> creator, int randomSeed) {
		this(randomSeed);
		this.creator = creator;
	}

	@Override
	protected BSTNode<T> addValue(T id) {
		BSTNode<T> nodeToReturn = super.addValue(id);

		TreapNode<T> nodeToAdd = (TreapNode<T>) nodeToReturn;
		heapify(nodeToAdd);

		return nodeToReturn;
	}

	private void heapify(TreapNode<T> current) {
        // Bubble up the heap, if needed
        TreapNode<T> parent = (TreapNode<T>) current.getParent();
        while (parent != null && current.getPriority() > parent.getPriority()) {
            BSTNode<T> grandParent = parent.getParent();
            if (grandParent != null) {
                if (grandParent.getGreater() != null && grandParent.getGreater() == parent) {
                    // My parent is my grandparents greater branch
                    grandParent.setGreater(current);
                    current.setParent(grandParent);
                } else if (grandParent.getLesser() != null && grandParent.getLesser() == parent) {
                    // My parent is my grandparents lesser branch
                    grandParent.setLesser(current);
                    current.setParent(grandParent);
                } else {
                    throw new RuntimeException("YIKES! Grandparent should have at least one non-NULL child which should be my parent.");
                }
                current.setParent(grandParent);
            } else {
                setRoot(current);
                getRoot().setParent(null);
            }

            if (parent.getLesser() != null && parent.getLesser() == current) {
                // LEFT
                parent.setLesser(null);

                if (current.getGreater() == null) {
                    current.setGreater(parent);
                    parent.setParent(current);
                } else {
                    BSTNode<T> lost = current.getGreater();
                    current.setGreater(parent);
                    parent.setParent(current);

                    parent.setLesser(lost);
                    lost.setParent(parent);
                }
            } else if (parent.getGreater() != null && parent.getGreater() == current) {
                // RIGHT
                parent.setGreater(null);

                if (current.getLesser() == null) {
                    current.setLesser(parent);
                    parent.setParent(current);
                } else {
                    BSTNode<T> lost = current.getLesser();
                    current.setLesser(parent);
                    parent.setParent(current);

                    parent.setGreater(lost);
                    lost.setParent(parent);
                }
            } else {
                // We really shouldn't get here
                throw new RuntimeException("YIKES! Parent should have at least one non-NULL child which should be me.");
            }

            parent = (TreapNode<T>) current.getParent();
        }
    }
}
