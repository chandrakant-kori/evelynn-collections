package tech.evelynn.collections.tree.avl;

import tech.evelynn.collections.list.ArrayList;
import tech.evelynn.collections.tree.bst.BSTNode;

public class AVLTreePrinter {

	public static <T extends Comparable<T>> String getString(AVLTree<T> tree) {
		if (tree.getRoot() == null)
			return "Tree has no nodes.";
		return getString((AVLNode<T>) tree.getRoot(), "", true);
	}

	public static <T extends Comparable<T>> String getString(AVLNode<T> node) {
		if (node == null)
			return "Sub-tree has no nodes.";
		return getString(node, "", true);
	}

	private static <T extends Comparable<T>> String getString(AVLNode<T> node, String prefix, boolean isTail) {
		StringBuilder builder = new StringBuilder();

		builder.append(prefix + (isTail ? "└── " : "├── ") + "(" + node.height + ") " + node.getId() + "\n");
		ArrayList<BSTNode<T>> children = null;
		if (node.getLesser() != null || node.getGreater() != null) {
			children = new ArrayList<BSTNode<T>>(2);
			if (node.getLesser() != null)
				children.add(node.getLesser());
			if (node.getGreater() != null)
				children.add(node.getGreater());
		}
		if (children != null) {
			for (int i = 0; i < children.size() - 1; i++) {
				builder.append(getString((AVLNode<T>) children.get(i), prefix + (isTail ? "    " : "│   "), false));
			}
			if (children.size() >= 1) {
				builder.append(getString((AVLNode<T>) children.get(children.size() - 1),
						prefix + (isTail ? "    " : "│   "), true));
			}
		}

		return builder.toString();
	}

}
