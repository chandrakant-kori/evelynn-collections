package tech.evelynn.collections.tree.suffix;

@SuppressWarnings("unused")
public class SuffixTreeEdge<C extends CharSequence> implements Comparable<SuffixTreeEdge<C>> {

	private static final int KEY_MOD = 2179; // Should be a prime that is
	// roughly 10% larger than the
	// String
	private static int count = 1;

	private SuffixTree<C> tree = null;

	private int startNode = -1;
	private int endNode = 0;
	private int firstCharIndex = 0;
	private int lastCharIndex = 0;

	public SuffixTreeEdge(SuffixTree<C> tree, int first, int last, int parent) {
		this.tree = tree;
		setFirstCharIndex(first);
		setLastCharIndex(last);
		setStartNode(parent);
		setEndNode(count++);
		insert(this);
	}

	private int getKey() {
		return key(getStartNode(), tree.getCharacters()[getFirstCharIndex()]);
	}

	private static int key(int node, char c) {
		return ((node << 8) + c) % KEY_MOD;
	}

	private void insert(SuffixTreeEdge<C> edge) {
		tree.getEdgeMap().put(edge.getKey(), edge);
	}

	private void remove(SuffixTreeEdge<C> edge) {
		int i = edge.getKey();
		SuffixTreeEdge<C> e = tree.getEdgeMap().remove(i);
		while (true) {
			e.setStartNode(-1);
			int j = i;
			while (true) {
				i = ++i % KEY_MOD;
				e = tree.getEdgeMap().get(i);
				if (e == null)
					return;
				int r = key(e.getStartNode(), tree.getCharacters()[e.getFirstCharIndex()]);
				if (i >= r && r > j)
					continue;
				if (r > j && j > i)
					continue;
				if (j > i && i >= r)
					continue;
				break;
			}
			tree.getEdgeMap().put(j, e);
		}
	}

	public static <C extends CharSequence> SuffixTreeEdge<C> find(SuffixTree<C> tree, int node, char c) {
		int key = key(node, c);
		return tree.getEdgeMap().get(key);
	}

	public int split(int originNode, int firstIndex, int lastIndex) {
		remove(this);
		SuffixTreeEdge<C> newEdge = new SuffixTreeEdge<C>(tree, this.getFirstCharIndex(),
				this.getFirstCharIndex() + lastIndex - firstIndex, originNode);
		SuffixTreeLink link = tree.getLinksMap().get(newEdge.getEndNode());
		if (link == null) {
			link = new SuffixTreeLink(newEdge.getEndNode());
			tree.getLinksMap().put(newEdge.getEndNode(), link);
		}
		tree.getLinksMap().get(newEdge.getEndNode()).setSuffixNode(originNode);
		this.setFirstCharIndex(this.getFirstCharIndex() + lastIndex - firstIndex + 1);
		this.setStartNode(newEdge.getEndNode());
		insert(this);
		return newEdge.getEndNode();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return getKey();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj instanceof SuffixTreeEdge)
			return false;

		@SuppressWarnings("unchecked")
		SuffixTreeEdge<C> e = (SuffixTreeEdge<C>) obj;
		if (getStartNode() == e.getStartNode()
				&& tree.getCharacters()[getFirstCharIndex()] == tree.getCharacters()[e.getFirstCharIndex()]) {
			return true;
		}

		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int compareTo(SuffixTreeEdge<C> edge) {
		if (edge == null)
			return -1;

		if (getStartNode() < edge.getStartNode())
			return -1;
		if (getStartNode() > edge.getStartNode())
			return 1;

		if (getEndNode() < edge.getEndNode())
			return -1;
		if (getEndNode() > edge.getEndNode())
			return 1;

		if (getFirstCharIndex() < edge.getFirstCharIndex())
			return -1;
		if (getFirstCharIndex() > edge.getFirstCharIndex())
			return 1;

		if (getLastCharIndex() < edge.getLastCharIndex())
			return -1;
		if (getLastCharIndex() > edge.getLastCharIndex())
			return 1;

		return 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("startNode=").append(getStartNode()).append("\n");
		builder.append("endNode=").append(getEndNode()).append("\n");
		builder.append("firstCharIndex=").append(getFirstCharIndex()).append("\n");
		builder.append("lastCharIndex=").append(getLastCharIndex()).append("\n");
		String s = tree.getString().substring(getFirstCharIndex(), getLastCharIndex() + 1);
		builder.append("string=").append(s).append("\n");
		return builder.toString();
	}

	public int getEndNode() {
		return endNode;
	}

	public void setEndNode(int endNode) {
		this.endNode = endNode;
	}

	public int getFirstCharIndex() {
		return firstCharIndex;
	}

	public void setFirstCharIndex(int firstCharIndex) {
		this.firstCharIndex = firstCharIndex;
	}

	public int getLastCharIndex() {
		return lastCharIndex;
	}

	public void setLastCharIndex(int lastCharIndex) {
		this.lastCharIndex = lastCharIndex;
	}

	public int getStartNode() {
		return startNode;
	}

	public void setStartNode(int startNode) {
		this.startNode = startNode;
	}
}
