package tech.evelynn.collections.tree.segment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("unchecked")
public class DynamicSegmentTree<D extends SegmentTreeData> extends SegmentTree<D> {

	public DynamicSegmentTree(List<D> data) {
		this(data, 1);
	}

	public DynamicSegmentTree(List<D> data, int minLength) {
		if (data.size() <= 0)
			return;

		// Check for gaps
		final List<OverlappingSegment<D>> segments = new ArrayList<OverlappingSegment<D>>();
		for (int i = 0; i < data.size(); i++) {
			if (i < data.size() - 1) {
				final SegmentTreeData d1 = data.get(i);
				final OverlappingSegment<D> s1 = new OverlappingSegment<D>(minLength, d1.start, d1.end, (D) d1);
				segments.add(s1);

				final SegmentTreeData d2 = data.get(i + 1);
				if (d2.start - d1.end > 1) {
					final SegmentTreeData d3 = d1.copy();
					d3.clear();
					d3.start = d1.end + 1;
					d3.end = d2.start - 1;

					final OverlappingSegment<D> s3 = new OverlappingSegment<D>(minLength, d3.start, d3.end, (D) d3);
					segments.add(s3);
				}
			} else {
				final SegmentTreeData d1 = data.get(i);
				final OverlappingSegment<D> s1 = new OverlappingSegment<D>(minLength, d1.start, d1.end, (D) d1);
				segments.add(s1);
			}
		}

		// First start first
		Collections.sort(segments, SegmentTreeConstants.START_COMPARATOR);
		final OverlappingSegment<D> startNode = segments.get(0);
		final long start = startNode.start - 1;
		final OverlappingSegment<D> s1 = new OverlappingSegment<D>(minLength, start, startNode.start, null);
		segments.add(0, s1);

		// Last end last
		Collections.sort(segments, SegmentTreeConstants.END_COMPARATOR);
		final OverlappingSegment<D> endNode = segments.get(segments.size() - 1);
		final long end = endNode.end + 1;
		final OverlappingSegment<D> s2 = new OverlappingSegment<D>(minLength, endNode.end, end, null);
		segments.add(s2);

		final int length = (int) (end - start) + 1;
		root = OverlappingSegment.createFromList(minLength, segments, start, length);
	}

	@Override
	public D query(long index) {
		return this.query(index, index);
	}

	@Override
	public D query(long startOfQuery, long endOfQuery) {
		if (root == null)
			return null;

		long s = startOfQuery;
		long e = endOfQuery;
		if (s < root.start)
			s = root.start;
		if (e > root.end)
			e = root.end;

		final D result = root.query(s, e);
		// reset the start and end, it can change during the query
		result.start = startOfQuery;
		result.end = endOfQuery;
		return result;
	}

}
