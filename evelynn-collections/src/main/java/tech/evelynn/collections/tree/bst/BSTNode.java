package tech.evelynn.collections.tree.bst;

public class BSTNode<T extends Comparable<T>> {

	private T id = null;
	private BSTNode<T> parent = null;
	private BSTNode<T> lesser = null;
	private BSTNode<T> greater = null;

	protected BSTNode(BSTNode<T> parent, T id) {
		this.setParent(parent);
		this.setId(id);
	}

	@Override
	public String toString() {
		return "id=" + getId() + " parent=" + ((getParent() != null) ? getParent().getId() : "NULL") + " lesser="
				+ ((getLesser() != null) ? getLesser().getId() : "NULL") + " greater=" + ((getGreater() != null) ? getGreater().getId() : "NULL");
	}

	public T getId() {
		return id;
	}

	public void setId(T id) {
		this.id = id;
	}

	public BSTNode<T> getLesser() {
		return lesser;
	}

	public void setLesser(BSTNode<T> lesser) {
		this.lesser = lesser;
	}

	public BSTNode<T> getGreater() {
		return greater;
	}

	public void setGreater(BSTNode<T> greater) {
		this.greater = greater;
	}

	public BSTNode<T> getParent() {
		return parent;
	}

	public void setParent(BSTNode<T> parent) {
		this.parent = parent;
	}
}
