package tech.evelynn.collections.tree.tst;

public class TernarySearchTreePrinter {

	public static <C extends CharSequence> void print(TernarySearchTree<C> tree) {
		System.out.println(getString(tree));
	}

	public static <C extends CharSequence> String getString(TernarySearchTree<C> tree) {
		if (tree.root == null)
			return "Tree has no nodes.";
		return getString(tree.root, "", null, true);
	}

	protected static String getString(TSTNode node, String prefix, String previousString, boolean isTail) {
		final StringBuilder builder = new StringBuilder();
		String string = "";
		if (previousString != null)
			string = previousString;
		builder.append(prefix + (isTail ? "└── " : "├── ")
				+ ((node.isWord() == true)
						? ("(" + node.getCharacter() + ") " + string + String.valueOf(node.getCharacter()))
						: node.getCharacter())
				+ "\n");
		if (node.loKid != null)
			builder.append(getString(node.loKid, prefix + (isTail ? "    " : "│   "), string, false));
		if (node.kid != null)
			builder.append(getString(node.kid, prefix + (isTail ? "    " : "│   "),
					string + String.valueOf(node.getCharacter()), false));
		if (node.hiKid != null)
			builder.append(getString(node.hiKid, prefix + (isTail ? "    " : "│   "), string, true));
		return builder.toString();
	}

}
