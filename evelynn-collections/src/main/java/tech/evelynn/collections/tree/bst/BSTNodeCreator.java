package tech.evelynn.collections.tree.bst;

public interface BSTNodeCreator<T extends Comparable<T>> {

	public BSTNode<T> createNewNode(BSTNode<T> parent, T id);

}
