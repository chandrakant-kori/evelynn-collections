package tech.evelynn.collections.tree.rbt;

import java.util.AbstractCollection;
import java.util.Iterator;

@SuppressWarnings("unchecked")
public class JavaCompatibleRedBlackTree<T extends Comparable<T>> extends AbstractCollection<T> {

	private RedBlackTree<T> tree = null;

    public JavaCompatibleRedBlackTree() {
        this.tree = new RedBlackTree<T> ();
    }

    public JavaCompatibleRedBlackTree(RedBlackTree<T> tree) {
        this.tree = tree;
    }
    
    @Override
    public boolean add(T value) {
        return tree.add(value);
    }
    
	@Override
    public boolean remove(Object value) {
        return (tree.remove((T)value)!=null);
    }
    
    @Override
    public boolean contains(Object value) {
        return tree.contains((T)value);
    }
    
    @Override
    public int size() {
        return tree.size();
    }
    
    @Override
    public Iterator<T> iterator() {
        return (new RedBlackTreeIterator<T>(this.tree));
    }

    
}
