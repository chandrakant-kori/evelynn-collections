package tech.evelynn.collections.tree.suffix;

import java.util.LinkedList;
import java.util.List;

public class SuffixTreePrinter {

	public static <C extends CharSequence> void printNode(SuffixTree<C> tree) {
		System.out.println(getString(tree, null, "", true));
	}

	public static <C extends CharSequence> String getString(SuffixTree<C> tree) {
		return getString(tree, null, "", true);
	}

	private static <C extends CharSequence> String getString(SuffixTree<C> tree, SuffixTreeEdge<C> e, String prefix,
			boolean isTail) {
		StringBuilder builder = new StringBuilder();
		int value = 0;
		if (e != null) {
			value = e.getEndNode();
			String string = tree.getString().substring(e.getFirstCharIndex(), e.getLastCharIndex() + 1);
			int index = string.indexOf(tree.getEndSeqChar());
			if (index >= 0)
				string = string.substring(0, index + 1);
			builder.append(prefix + (isTail ? "└── " : "├── ") + "(" + value + ") " + string + "\n");
		} else {
			builder.append(prefix + (isTail ? "└── " : "├── ") + "(" + 0 + ")" + "\n");
		}

		if (tree.getEdgeMap().size() > 0) {
			List<SuffixTreeEdge<C>> children = new LinkedList<SuffixTreeEdge<C>>();
			for (SuffixTreeEdge<C> edge : tree.getEdgeMap().values()) {
				if (edge != null && (edge.getStartNode() == value)) {
					children.add(edge);
				}
			}
			if (children.size() > 0) {
				for (int i = 0; i < children.size() - 1; i++) {
					SuffixTreeEdge<C> edge = children.get(i);
					builder.append(getString(tree, edge, prefix + (isTail ? "    " : "│   "), false));
				}
				if (children.size() >= 1) {
					SuffixTreeEdge<C> edge = children.get(children.size() - 1);
					builder.append(getString(tree, edge, prefix + (isTail ? "    " : "│   "), true));
				}
			}
		}
		return builder.toString();
	}

}
