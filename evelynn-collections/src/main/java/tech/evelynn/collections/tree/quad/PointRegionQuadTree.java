package tech.evelynn.collections.tree.quad;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unchecked")
public class PointRegionQuadTree<P extends XYPoint> extends QuadTree<P> {

	private static final XYPoint XY_POINT = new XYPoint();
	private static final AxisAlignedBoundingBox RANGE = new AxisAlignedBoundingBox();

	private PointRegionQuadNode<P> root = null;

	public PointRegionQuadTree(double x, double y, double width, double height) {
		this(x, y, width, height, 4, 20);
	}

	public PointRegionQuadTree(double x, double y, double width, double height, int leafCapacity) {
		this(x, y, width, height, leafCapacity, 20);
	}

	public PointRegionQuadTree(double x, double y, double width, double height, int leafCapacity, int maxTreeHeight) {
		XYPoint xyPoint = new XYPoint(x, y);
		AxisAlignedBoundingBox aabb = new AxisAlignedBoundingBox(xyPoint, width, height);
		PointRegionQuadNode.maxCapacity = leafCapacity;
		PointRegionQuadNode.maxHeight = maxTreeHeight;
		root = new PointRegionQuadNode<P>(aabb);
	}

	@Override
	public QuadNode<P> getRoot() {
		return root;
	}

	@Override
	public boolean insert(double x, double y) {
		XYPoint xyPoint = new XYPoint(x, y);
		return root.insert((P) xyPoint);
	}

	@Override
	public boolean remove(double x, double y) {
		XY_POINT.set(x, y);
		return root.remove((P) XY_POINT);
	}

	@Override
	public Collection<P> queryRange(double x, double y, double width, double height) {
		if (root == null)
			return Collections.EMPTY_LIST;

		XY_POINT.set(x, y);
		RANGE.set(XY_POINT, width, height);

		List<P> pointsInRange = new LinkedList<P>();
		root.queryRange(RANGE, pointsInRange);
		return pointsInRange;
	}

}
