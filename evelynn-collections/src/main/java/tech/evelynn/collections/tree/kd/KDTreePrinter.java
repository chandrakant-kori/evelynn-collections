package tech.evelynn.collections.tree.kd;

import tech.evelynn.collections.list.ArrayList;
import tech.evelynn.collections.list.List;

public class KDTreePrinter {

	public static <T extends KDXYZPoint> String getString(KDTree<T> tree) {
		if (tree.getRoot() == null)
			return "Tree has no nodes.";
		return getString(tree.getRoot(), "", true);
	}

	private static String getString(KDNode node, String prefix, boolean isTail) {
		StringBuilder builder = new StringBuilder();

		if (node.getParent() != null) {
			String side = "left";
			if (node.getParent().getGreater() != null && node.getId().equals(node.getParent().getGreater().getId()))
				side = "right";
			builder.append(prefix + (isTail ? "└── " : "├── ") + "[" + side + "] " + "depth=" + node.getDepth() + " id="
					+ node.getId() + "\n");
		} else {
			builder.append(
					prefix + (isTail ? "└── " : "├── ") + "depth=" + node.getDepth() + " id=" + node.getId() + "\n");
		}
		List<KDNode> children = null;
		if (node.getLesser() != null || node.getGreater() != null) {
			children = new ArrayList<KDNode>(2);
			if (node.getLesser() != null)
				children.add(node.getLesser());
			if (node.getGreater() != null)
				children.add(node.getGreater());
		}
		if (children != null) {
			for (int i = 0; i < children.size() - 1; i++) {
				builder.append(getString(children.toList().get(i), prefix + (isTail ? "    " : "│   "), false));
			}
			if (children.size() >= 1) {
				builder.append(getString(children.toList().get(children.size() - 1),
						prefix + (isTail ? "    " : "│   "), true));
			}
		}

		return builder.toString();
	}
}
