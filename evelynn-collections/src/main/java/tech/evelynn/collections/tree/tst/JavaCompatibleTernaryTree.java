package tech.evelynn.collections.tree.tst;

@SuppressWarnings("unchecked")
public class JavaCompatibleTernaryTree<C extends CharSequence> extends java.util.AbstractCollection<C> {

	private TernarySearchTree<C> tree = null;

    public JavaCompatibleTernaryTree(TernarySearchTree<C> tree) {
        this.tree = tree;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(C value) {
        return tree.add(value);
    }

    /**
     * {@inheritDoc}
     */
	@Override
    public boolean remove(Object value) {
        return (tree.remove((C)value)!=null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return tree.contains((C)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return tree.getSize();
    }

    /**
     * {@inheritDoc}
     * 
     * WARNING: This iterator makes a copy of the tree's contents during it's construction!
     */
    @Override
    public java.util.Iterator<C> iterator() {
        return (new TernarySearchTreeIterator<C>(tree));
    }

}
