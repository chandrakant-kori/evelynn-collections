package tech.evelynn.collections.tree.btree;

import tech.evelynn.collections.tree.Tree;

public class BTree<T extends Comparable<T>> implements Tree<T> {

	private int minKeySize = 1;
	private int minChildrenSize = minKeySize + 1; // 2
	private int maxKeySize = 2 * minKeySize; // 2
	private int maxChildrenSize = maxKeySize + 1; // 3

	private BTreeNode<T> root = null;
	private int size = 0;

	public BTree() {
	}

	public BTree(int order) {
		this.minKeySize = order;
		this.minChildrenSize = minKeySize + 1;
		this.maxKeySize = 2 * minKeySize;
		this.maxChildrenSize = maxKeySize + 1;
	}

	@Override
	public boolean add(T value) {
		if (getRoot() == null) {
			setRoot(new BTreeNode<T>(null, maxKeySize, maxChildrenSize));
			getRoot().addKey(value);
		} else {
			BTreeNode<T> BTreeNode = getRoot();
			while (BTreeNode != null) {
				if (BTreeNode.numberOfChildren() == 0) {
					BTreeNode.addKey(value);
					if (BTreeNode.numberOfKeys() <= maxKeySize) {
						// A-OK
						break;
					}
					// Need to split up
					split(BTreeNode);
					break;
				}
				// Navigate

				// Lesser or equal
				T lesser = BTreeNode.getKey(0);
				if (value.compareTo(lesser) <= 0) {
					BTreeNode = BTreeNode.getChild(0);
					continue;
				}

				// Greater
				int numberOfKeys = BTreeNode.numberOfKeys();
				int last = numberOfKeys - 1;
				T greater = BTreeNode.getKey(last);
				if (value.compareTo(greater) > 0) {
					BTreeNode = BTreeNode.getChild(numberOfKeys);
					continue;
				}

				// Search internal BTreeNodes
				for (int i = 1; i < BTreeNode.numberOfKeys(); i++) {
					T prev = BTreeNode.getKey(i - 1);
					T next = BTreeNode.getKey(i);
					if (value.compareTo(prev) > 0 && value.compareTo(next) <= 0) {
						BTreeNode = BTreeNode.getChild(i);
						break;
					}
				}
			}
		}

		size++;

		return true;
	}

	private void split(BTreeNode<T> BTreeNodeToSplit) {
		BTreeNode<T> BTreeNode = BTreeNodeToSplit;
		int numberOfKeys = BTreeNode.numberOfKeys();
		int medianIndex = numberOfKeys / 2;
		T medianValue = BTreeNode.getKey(medianIndex);

		BTreeNode<T> left = new BTreeNode<T>(null, maxKeySize, maxChildrenSize);
		for (int i = 0; i < medianIndex; i++) {
			left.addKey(BTreeNode.getKey(i));
		}
		if (BTreeNode.numberOfChildren() > 0) {
			for (int j = 0; j <= medianIndex; j++) {
				BTreeNode<T> c = BTreeNode.getChild(j);
				left.addChild(c);
			}
		}

		BTreeNode<T> right = new BTreeNode<T>(null, maxKeySize, maxChildrenSize);
		for (int i = medianIndex + 1; i < numberOfKeys; i++) {
			right.addKey(BTreeNode.getKey(i));
		}
		if (BTreeNode.numberOfChildren() > 0) {
			for (int j = medianIndex + 1; j < BTreeNode.numberOfChildren(); j++) {
				BTreeNode<T> c = BTreeNode.getChild(j);
				right.addChild(c);
			}
		}

		if (BTreeNode.parent == null) {
			// new root, height of tree is increased
			BTreeNode<T> newRoot = new BTreeNode<T>(null, maxKeySize, maxChildrenSize);
			newRoot.addKey(medianValue);
			BTreeNode.parent = newRoot;
			setRoot(newRoot);
			BTreeNode = getRoot();
			BTreeNode.addChild(left);
			BTreeNode.addChild(right);
		} else {
			// Move the median value up to the parent
			BTreeNode<T> parent = BTreeNode.parent;
			parent.addKey(medianValue);
			parent.removeChild(BTreeNode);
			parent.addChild(left);
			parent.addChild(right);

			if (parent.numberOfKeys() > maxKeySize)
				split(parent);
		}
	}

	@Override
	public T remove(T value) {
		T removed = null;
		BTreeNode<T> BTreeNode = this.getBTreeNode(value);
		removed = remove(value, BTreeNode);
		return removed;
	}

	public T remove(T value, BTreeNode<T> BTreeNode) {
		if (BTreeNode == null)
			return null;

		T removed = null;
		int index = BTreeNode.indexOf(value);
		removed = BTreeNode.removeKey(value);
		if (BTreeNode.numberOfChildren() == 0) {
			// leaf BTreeNode
			if (BTreeNode.parent != null && BTreeNode.numberOfKeys() < minKeySize) {
				this.combined(BTreeNode);
			} else if (BTreeNode.parent == null && BTreeNode.numberOfKeys() == 0) {
				// Removing root BTreeNode with no keys or children
				setRoot(null);
			}
		} else {
			// internal BTreeNode
			BTreeNode<T> lesser = BTreeNode.getChild(index);
			BTreeNode<T> greatest = this.getGreatestBTreeNode(lesser);
			T replaceValue = this.removeGreatestValue(greatest);
			BTreeNode.addKey(replaceValue);
			if (greatest.parent != null && greatest.numberOfKeys() < minKeySize) {
				this.combined(greatest);
			}
			if (greatest.numberOfChildren() > maxChildrenSize) {
				this.split(greatest);
			}
		}

		size--;

		return removed;
	}

	private T removeGreatestValue(BTreeNode<T> BTreeNode) {
        T value = null;
        if (BTreeNode.numberOfKeys() > 0) {
            value = BTreeNode.removeKey(BTreeNode.numberOfKeys() - 1);
        }
        return value;
    }
	
	@Override
    public void clear() {
        setRoot(null);
        size = 0;
    }
	
	@Override
    public boolean contains(T value) {
        BTreeNode<T> BTreeNode = getBTreeNode(value);
        return (BTreeNode != null);
    }
	
	private BTreeNode<T> getBTreeNode(T value) {
        BTreeNode<T> BTreeNode = getRoot();
        while (BTreeNode != null) {
            T lesser = BTreeNode.getKey(0);
            if (value.compareTo(lesser) < 0) {
                if (BTreeNode.numberOfChildren() > 0)
                    BTreeNode = BTreeNode.getChild(0);
                else
                    BTreeNode = null;
                continue;
            }

            int numberOfKeys = BTreeNode.numberOfKeys();
            int last = numberOfKeys - 1;
            T greater = BTreeNode.getKey(last);
            if (value.compareTo(greater) > 0) {
                if (BTreeNode.numberOfChildren() > numberOfKeys)
                    BTreeNode = BTreeNode.getChild(numberOfKeys);
                else
                    BTreeNode = null;
                continue;
            }

            for (int i = 0; i < numberOfKeys; i++) {
                T currentValue = BTreeNode.getKey(i);
                if (currentValue.compareTo(value) == 0) {
                    return BTreeNode;
                }

                int next = i + 1;
                if (next <= last) {
                    T nextValue = BTreeNode.getKey(next);
                    if (currentValue.compareTo(value) < 0 && nextValue.compareTo(value) > 0) {
                        if (next < BTreeNode.numberOfChildren()) {
                            BTreeNode = BTreeNode.getChild(next);
                            break;
                        }
                        return null;
                    }
                }
            }
        }
        return null;
    }

	private BTreeNode<T> getGreatestBTreeNode(BTreeNode<T> BTreeNodeToGet) {
        BTreeNode<T> BTreeNode = BTreeNodeToGet;
        while (BTreeNode.numberOfChildren() > 0) {
            BTreeNode = BTreeNode.getChild(BTreeNode.numberOfChildren() - 1);
        }
        return BTreeNode;
    }
	
	private boolean combined(BTreeNode<T> BTreeNode) {
        BTreeNode<T> parent = BTreeNode.parent;
        int index = parent.indexOf(BTreeNode);
        int indexOfLeftNeighbor = index - 1;
        int indexOfRightNeighbor = index + 1;

        BTreeNode<T> rightNeighbor = null;
        int rightNeighborSize = -minChildrenSize;
        if (indexOfRightNeighbor < parent.numberOfChildren()) {
            rightNeighbor = parent.getChild(indexOfRightNeighbor);
            rightNeighborSize = rightNeighbor.numberOfKeys();
        }

        // Try to borrow neighbor
        if (rightNeighbor != null && rightNeighborSize > minKeySize) {
            // Try to borrow from right neighbor
            T removeValue = rightNeighbor.getKey(0);
            int prev = getIndexOfPreviousValue(parent, removeValue);
            T parentValue = parent.removeKey(prev);
            T neighborValue = rightNeighbor.removeKey(0);
            BTreeNode.addKey(parentValue);
            parent.addKey(neighborValue);
            if (rightNeighbor.numberOfChildren() > 0) {
                BTreeNode.addChild(rightNeighbor.removeChild(0));
            }
        } else {
            BTreeNode<T> leftNeighbor = null;
            int leftNeighborSize = -minChildrenSize;
            if (indexOfLeftNeighbor >= 0) {
                leftNeighbor = parent.getChild(indexOfLeftNeighbor);
                leftNeighborSize = leftNeighbor.numberOfKeys();
            }

            if (leftNeighbor != null && leftNeighborSize > minKeySize) {
                // Try to borrow from left neighbor
                T removeValue = leftNeighbor.getKey(leftNeighbor.numberOfKeys() - 1);
                int prev = getIndexOfNextValue(parent, removeValue);
                T parentValue = parent.removeKey(prev);
                T neighborValue = leftNeighbor.removeKey(leftNeighbor.numberOfKeys() - 1);
                BTreeNode.addKey(parentValue);
                parent.addKey(neighborValue);
                if (leftNeighbor.numberOfChildren() > 0) {
                    BTreeNode.addChild(leftNeighbor.removeChild(leftNeighbor.numberOfChildren() - 1));
                }
            } else if (rightNeighbor != null && parent.numberOfKeys() > 0) {
                // Can't borrow from neighbors, try to combined with right neighbor
                T removeValue = rightNeighbor.getKey(0);
                int prev = getIndexOfPreviousValue(parent, removeValue);
                T parentValue = parent.removeKey(prev);
                parent.removeChild(rightNeighbor);
                BTreeNode.addKey(parentValue);
                for (int i = 0; i < rightNeighbor.getKeysSize(); i++) {
                    T v = rightNeighbor.getKey(i);
                    BTreeNode.addKey(v);
                }
                for (int i = 0; i < rightNeighbor.getChildrenSize(); i++) {
                    BTreeNode<T> c = rightNeighbor.getChild(i);
                    BTreeNode.addChild(c);
                }

                if (parent.parent != null && parent.numberOfKeys() < minKeySize) {
                    // removing key made parent too small, combined up tree
                    this.combined(parent);
                } else if (parent.numberOfKeys() == 0) {
                    // parent no longer has keys, make this BTreeNode the new root
                    // which decreases the height of the tree
                    BTreeNode.parent = null;
                    setRoot(BTreeNode);
                }
            } else if (leftNeighbor != null && parent.numberOfKeys() > 0) {
                // Can't borrow from neighbors, try to combined with left neighbor
                T removeValue = leftNeighbor.getKey(leftNeighbor.numberOfKeys() - 1);
                int prev = getIndexOfNextValue(parent, removeValue);
                T parentValue = parent.removeKey(prev);
                parent.removeChild(leftNeighbor);
                BTreeNode.addKey(parentValue);
                for (int i = 0; i < leftNeighbor.getKeysSize(); i++) {
                    T v = leftNeighbor.getKey(i);
                    BTreeNode.addKey(v);
                }
                for (int i = 0; i < leftNeighbor.getChildrenSize(); i++) {
                    BTreeNode<T> c = leftNeighbor.getChild(i);
                    BTreeNode.addChild(c);
                }

                if (parent.parent != null && parent.numberOfKeys() < minKeySize) {
                    // removing key made parent too small, combined up tree
                    this.combined(parent);
                } else if (parent.numberOfKeys() == 0) {
                    // parent no longer has keys, make this BTreeNode the new root
                    // which decreases the height of the tree
                    BTreeNode.parent = null;
                    setRoot(BTreeNode);
                }
            }
        }

        return true;
    }

	private int getIndexOfPreviousValue(BTreeNode<T> BTreeNode, T value) {
        for (int i = 1; i < BTreeNode.numberOfKeys(); i++) {
            T t = BTreeNode.getKey(i);
            if (t.compareTo(value) >= 0)
                return i - 1;
        }
        return BTreeNode.numberOfKeys() - 1;
    }
	
	private int getIndexOfNextValue(BTreeNode<T> BTreeNode, T value) {
        for (int i = 0; i < BTreeNode.numberOfKeys(); i++) {
            T t = BTreeNode.getKey(i);
            if (t.compareTo(value) >= 0)
                return i;
        }
        return BTreeNode.numberOfKeys() - 1;
    }
	
	@Override
    public int size() {
        return size;
    }
	
	@Override
    public java.util.Collection<T> toCollection() {
        return (new JavaCompatibleBTree<T>(this));
    }

	@Override
    public String toString() {
        return BTreePrinter.getString(this);
    }

	public BTreeNode<T> getRoot() {
		return root;
	}

	public void setRoot(BTreeNode<T> root) {
		this.root = root;
	}

}
