package tech.evelynn.collections.tree.interval;

import java.util.ArrayList;
import java.util.List;

public class IntervalTreePrinter {

	public static <O extends Object> String getString(IntervalTree<O> tree) {
		if (tree.getRoot() == null)
			return "Tree has no nodes.";
		return getString(tree.getRoot(), "", true);
	}

	private static <O extends Object> String getString(Interval<O> interval, String prefix, boolean isTail) {
		StringBuilder builder = new StringBuilder();

		builder.append(prefix + (isTail ? "└── " : "├── ") + interval.toString() + "\n");
		List<Interval<O>> children = new ArrayList<Interval<O>>();
		if (interval.getLeft() != null)
			children.add(interval.getLeft());
		if (interval.getRight() != null)
			children.add(interval.getRight());
		if (children.size() > 0) {
			for (int i = 0; i < children.size() - 1; i++)
				builder.append(getString(children.get(i), prefix + (isTail ? "    " : "│   "), false));
			if (children.size() > 0)
				builder.append(getString(children.get(children.size() - 1), prefix + (isTail ? "    " : "│   "), true));
		}

		return builder.toString();
	}

}
