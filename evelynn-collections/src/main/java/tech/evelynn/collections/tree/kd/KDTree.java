package tech.evelynn.collections.tree.kd;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import tech.evelynn.collections.list.ArrayList;
import tech.evelynn.collections.list.List;

@SuppressWarnings("unchecked")
public class KDTree<T extends KDXYZPoint> implements Iterable<T> {

	private int k = 3;
	private KDNode root = null;

	public KDTree() {
	}

	public KDTree(List<KDXYZPoint> list) {
		super();
		setRoot(createNode(list, k, 0));
	}

	public KDTree(List<KDXYZPoint> list, int k) {
		super();
		setRoot(createNode(list, k, 0));
	}

	private static KDNode createNode(List<KDXYZPoint> list, int k, int depth) {
		if (list == null || list.size() == 0)
			return null;

		int axis = depth % k;
		if (axis == KDXYZpointConstants.X_AXIS)
			Collections.sort(list.toList(), KDXYZpointConstants.X_COMPARATOR);
		else if (axis == KDXYZpointConstants.Y_AXIS)
			Collections.sort(list.toList(), KDXYZpointConstants.Y_COMPARATOR);
		else
			Collections.sort(list.toList(), KDXYZpointConstants.Z_COMPARATOR);

		KDNode node = null;
		List<KDXYZPoint> less = new ArrayList<KDXYZPoint>(list.size());
		List<KDXYZPoint> more = new ArrayList<KDXYZPoint>(list.size());
		if (list.size() > 0) {
			int medianIndex = list.size() / 2;
			node = new KDNode(list.toList().get(medianIndex), k, depth);
			// Process list to see where each non-median point lies
			for (int i = 0; i < list.size(); i++) {
				if (i == medianIndex)
					continue;
				KDXYZPoint p = list.toList().get(i);
				// Cannot assume points before the median are less since they could be equal
				if (KDNode.compareTo(depth, k, p, node.getId()) <= 0) {
					less.add(p);
				} else {
					more.add(p);
				}
			}

			if ((medianIndex - 1 >= 0) && less.size() > 0) {
				node.setLesser(createNode(less, k, depth + 1));
				node.getLesser().setParent(node);
			}

			if ((medianIndex <= list.size() - 1) && more.size() > 0) {
				node.setGreater(createNode(more, k, depth + 1));
				node.getGreater().setParent(node);
			}
		}

		return node;
	}

	public boolean add(T value) {
		if (value == null)
			return false;

		if (getRoot() == null) {
			setRoot(new KDNode(value));
			return true;
		}

		KDNode node = getRoot();
		while (true) {
			if (KDNode.compareTo(node.getDepth(), node.getK(), value, node.getId()) <= 0) {
				// Lesser
				if (node.getLesser() == null) {
					KDNode newNode = new KDNode(value, k, node.getDepth() + 1);
					newNode.setParent(node);
					node.setLesser(newNode);
					break;
				}
				node = node.getLesser();
			} else {
				// Greater
				if (node.getGreater() == null) {
					KDNode newNode = new KDNode(value, k, node.getDepth() + 1);
					newNode.setParent(node);
					node.setGreater(newNode);
					break;
				}
				node = node.getGreater();
			}
		}

		return true;
	}

	public boolean contains(T value) {
		if (value == null || getRoot() == null)
			return false;

		KDNode node = getNode(this, value);
		return (node != null);
	}

	private static final <T extends KDXYZPoint> KDNode getNode(KDTree<T> tree, T value) {
		if (tree == null || tree.getRoot() == null || value == null)
			return null;

		KDNode node = tree.getRoot();
		while (true) {
			if (node.getId().equals(value)) {
				return node;
			} else if (KDNode.compareTo(node.getDepth(), node.getK(), value, node.getId()) <= 0) {
				// Lesser
				if (node.getLesser() == null) {
					return null;
				}
				node = node.getLesser();
			} else {
				// Greater
				if (node.getGreater() == null) {
					return null;
				}
				node = node.getGreater();
			}
		}
	}

	public boolean remove(T value) {
		if (value == null || getRoot() == null)
			return false;

		KDNode node = getNode(this, value);
		if (node == null)
			return false;

		KDNode parent = node.getParent();
		if (parent != null) {
			if (parent.getLesser() != null && node.equals(parent.getLesser())) {
				List<KDXYZPoint> nodes = getTree(node);
				if (nodes.size() > 0) {
					parent.setLesser(createNode(nodes, node.getK(), node.getDepth()));
					if (parent.getLesser() != null) {
						parent.getLesser().setParent(parent);
					}
				} else {
					parent.setLesser(null);
				}
			} else {
				List<KDXYZPoint> nodes = getTree(node);
				if (nodes.size() > 0) {
					parent.setGreater(createNode(nodes, node.getK(), node.getDepth()));
					if (parent.getGreater() != null) {
						parent.getGreater().setParent(parent);
					}
				} else {
					parent.setGreater(null);
				}
			}
		} else {
			// root
			List<KDXYZPoint> nodes = getTree(node);
			if (nodes.size() > 0)
				setRoot(createNode(nodes, node.getK(), node.getDepth()));
			else
				setRoot(null);
		}

		return true;
	}

	private static final List<KDXYZPoint> getTree(KDNode root) {

		List<KDXYZPoint> list = new ArrayList<KDXYZPoint>();
		if (root == null)
			return list;

		if (root.getLesser() != null) {
			list.add(root.getLesser().getId());
			for (KDXYZPoint xyzpoint : getTree(root.getLesser()).toList()) {
				list.add(xyzpoint);
			}

		}
		if (root.getGreater() != null) {
			list.add(root.getGreater().getId());
			for (KDXYZPoint xyzpoint : getTree(root.getGreater()).toList()) {
				list.add(xyzpoint);
			}
		}

		return list;
	}

	public Collection<T> nearestNeighbourSearch(int K, T value) {
		if (value == null || getRoot() == null)
			return Collections.EMPTY_LIST;

		// JDK Collection is used here,

		// Map used for results
		TreeSet<KDNode> results = new TreeSet<KDNode>(new KDEuclideanComparator<KDNode>(value));

		// Find the closest leaf node
		KDNode prev = null;
		KDNode node = getRoot();
		while (node != null) {
			if (KDNode.compareTo(node.getDepth(), node.getK(), value, node.getId()) <= 0) {
				// Lesser
				prev = node;
				node = node.getLesser();
			} else {
				// Greater
				prev = node;
				node = node.getGreater();
			}
		}
		KDNode leaf = prev;

		if (leaf != null) {
			// Used to not re-examine nodes
			Set<KDNode> examined = new HashSet<KDNode>();

			// Go up the tree, looking for better solutions
			node = leaf;
			while (node != null) {
				// Search node
				searchNode(value, node, K, results, examined);
				node = node.getParent();
			}
		}

		// Load up the collection of the results
		Collection<T> collection = new java.util.ArrayList<T>(K);
		for (KDNode kdNode : results)
			collection.add((T) kdNode.getId());
		return collection;
	}

	private static final <T extends KDXYZPoint> void searchNode(T value, KDNode node, int K, TreeSet<KDNode> results,
			Set<KDNode> examined) {
		examined.add(node);

		// Search node
		KDNode lastNode = null;
		Double lastDistance = Double.MAX_VALUE;
		if (results.size() > 0) {
			lastNode = results.last();
			lastDistance = lastNode.getId().euclideanDistance(value);
		}
		Double nodeDistance = node.getId().euclideanDistance(value);
		if (nodeDistance.compareTo(lastDistance) < 0) {
			if (results.size() == K && lastNode != null)
				results.remove(lastNode);
			results.add(node);
		} else if (nodeDistance.equals(lastDistance)) {
			results.add(node);
		} else if (results.size() < K) {
			results.add(node);
		}
		lastNode = results.last();
		lastDistance = lastNode.getId().euclideanDistance(value);

		int axis = node.getDepth() % node.getK();
		KDNode lesser = node.getLesser();
		KDNode greater = node.getGreater();

		// Search children branches, if axis aligned distance is less than
		// current distance
		if (lesser != null && !examined.contains(lesser)) {
			examined.add(lesser);

			double nodePoint = Double.MIN_VALUE;
			double valuePlusDistance = Double.MIN_VALUE;
			if (axis == KDXYZpointConstants.X_AXIS) {
				nodePoint = node.getId().x;
				valuePlusDistance = value.x - lastDistance;
			} else if (axis == KDXYZpointConstants.Y_AXIS) {
				nodePoint = node.getId().y;
				valuePlusDistance = value.y - lastDistance;
			} else {
				nodePoint = node.getId().z;
				valuePlusDistance = value.z - lastDistance;
			}
			boolean lineIntersectsCube = ((valuePlusDistance <= nodePoint) ? true : false);

			// Continue down lesser branch
			if (lineIntersectsCube)
				searchNode(value, lesser, K, results, examined);
		}
		if (greater != null && !examined.contains(greater)) {
			examined.add(greater);

			double nodePoint = Double.MIN_VALUE;
			double valuePlusDistance = Double.MIN_VALUE;
			if (axis == KDXYZpointConstants.X_AXIS) {
				nodePoint = node.getId().x;
				valuePlusDistance = value.x + lastDistance;
			} else if (axis == KDXYZpointConstants.Y_AXIS) {
				nodePoint = node.getId().y;
				valuePlusDistance = value.y + lastDistance;
			} else {
				nodePoint = node.getId().z;
				valuePlusDistance = value.z + lastDistance;
			}
			boolean lineIntersectsCube = ((valuePlusDistance >= nodePoint) ? true : false);

			// Continue down greater branch
			if (lineIntersectsCube)
				searchNode(value, greater, K, results, examined);
		}
	}

	private static <T extends KDXYZPoint> void search(final KDNode node, final Deque<T> results) {
		if (node != null) {
			results.add((T) node.getId());
			search(node.getGreater(), results);
			search(node.getLesser(), results);
		}
	}

	@Override
	public String toString() {
		return KDTreePrinter.getString(this);
	}

	public KDNode getRoot() {
		return root;
	}

	public void setRoot(KDNode root) {
		this.root = root;
	}

	@Override
	public Iterator<T> iterator() {
		final Deque<T> results = new ArrayDeque<T>();
		search(root, results);
		return results.iterator();
	}

	public Iterator<T> reverse_iterator() {
		final Deque<T> results = new ArrayDeque<T>();
		search(root, results);
		return results.descendingIterator();
	}

}
