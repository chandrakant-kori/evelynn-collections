package tech.evelynn.collections.set.disjoint;

public class DisjointSet<T extends Object> {

	private DisjointSet() {
	}

	public static final <T extends Object> DisjointItem<T> makeSet(T v) {
		final DisjointItem<T> item = new DisjointItem<T>(null, v);
		item.setParent(item);
		return item;
	}

	public static final <T extends Object> DisjointItem<T> find(DisjointItem<T> x) {
		if (x == null)
			return null;

		if ((x.getParent() != null) && !(x.getParent().equals(x)))
			x.setParent(find(x.getParent()));
		return x.getParent();
	}

	public static final <T extends Object> DisjointItem<T> union(DisjointItem<T> x, DisjointItem<T> y) {
		final DisjointItem<T> xRoot = find(x);
		final DisjointItem<T> yRoot = find(y);
		if (xRoot == null && yRoot == null)
			return null;
		if (xRoot == null && yRoot != null)
			return yRoot;
		if (yRoot == null && xRoot != null)
			return xRoot;

		// x and y are in the same set
		if (xRoot.equals(yRoot))
			return xRoot;

		if (xRoot.getRank() < yRoot.getRank()) {
			xRoot.setParent(yRoot);
			return yRoot;
		} else if (xRoot.getRank() > yRoot.getRank()) {
			yRoot.setParent(xRoot);
			return xRoot;
		}
		// else
		yRoot.setParent(xRoot);
		xRoot.setRank(xRoot.getRank() + 1);
		return xRoot;
	}

	@Override
	public String toString() {
		return "Nothing here to see, yet.";
	}

}
