package tech.evelynn.collections.set.skiplist;

import java.util.Random;

import tech.evelynn.collections.set.Set;

public class SkipListSet<T extends Comparable<T>> implements Set<T> {
	
	private static final Random seedGenerator = new Random();
	private static final int MAX = 31;

    private SkipListNodeCreator<T> creator = null;
    private int randomSeed = -1;
    private int size = 0;

    private SkipListNode<T> head = null;
    
    public SkipListSet() {
        randomSeed = seedGenerator.nextInt() | 0x0100;
    }

    public SkipListSet(SkipListNodeCreator<T> creator) {
        this();
        this.creator = creator;
    }
    
    private int getRandom() {
        int x = randomSeed;
        x ^= x << 13;
        x ^= x >>> 17;
        randomSeed = x ^= x << 5;
        if ((x & 0x8001) != 0) // test highest and lowest bits
            return 0;
        int level = 1;
        while (((x >>>= 1) & 1) != 0) ++level;
        return level;
    }

    
    public SkipListNode<T> addValue(T value) {
        SkipListNode<T> toReturn = null;      
        if (getHead()==null) {
            // new list
            SkipListNode<T> node = null; 
            if (creator==null) node = new SkipListNode<T>(MAX,value);
            else node = creator.createNewNode(MAX, value);
            setHead(node);
            toReturn = node;
        } else {
            int level = getRandom();
            SkipListNode<T> node = null; 
            if (creator==null) node = new SkipListNode<T>(level,value);
            else node = creator.createNewNode(level, value);

            SkipListNode<T> prev = getHead();
            if (getHead().getData().compareTo(value)>0) {
                // handle case where head is greater than new node, just swap values
                //T oldHeadValue = head.data;
                //head.data = value;
                // Swap the old head value into the new node
                //node.data = oldHeadValue;
                if (creator==null) swapNode(node,getHead());
                else creator.swapNode(node, getHead());
                toReturn = getHead();
            } else {
                toReturn = node;
            }

            // Start from the top and work down to update the pointers
            for (int i=MAX; i>=0; i--) {
                SkipListNode<T> next = prev.getNext(i);
                while (next!=null) {
                    if (next.getData().compareTo(value)>0) break;
                    prev = next;
                    // It's important to set next since the node we are looking for
                    // on the next level cannot be behind this "prev" node.
                    next = prev.getNext(i);
                }
                if (i <= level) {
                    // If we are on a level where the new node exists, update the linked list
                    node.setNext(i, next);
                    prev.setNext(i, node);
                }
            }
        }
        size++;
        return toReturn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(T value) {
        SkipListNode<T> node = addValue(value);
        return (node!=null);
    }

    private SkipListNodeLevelPair<T> getPredecessor(T value) {
        SkipListNode<T> node = getHead();
        if (node==null) return null; 
        if (node.getData().compareTo(value)==0) return null;

        // Current node is not the node we are looking for; Keep moving down
        // until you find a node with a non-null "next" pointer.
        int level = node.getLevel();
        SkipListNode<T> next = node.getNext(level);
        while (next==null) {
            // If next is null, move down
            if (level>0) next = node.getNext(--level);
            else break;
        }

        // Found a node with a next node OR I reached the bottom level
        while (next!=null) {
            int comp = next.getData().compareTo(value);
            if (comp==0) {
                // Found the node who's next node is the node we are looking for!
                SkipListNodeLevelPair<T> pair = new SkipListNodeLevelPair<T>(level,node);
                return pair;
            } else if (comp>=1) {
                // Found a node that's greater, move down a level
                if (level>0) level--;
                else return null;

                // Update the next pointer
                next = node.getNext(level);
            } else {
                // Next is less then the value we are looking for, keep moving to next.
                node = next;
                next = node.getNext(level);
                while (next==null && level>0) {
                    next = node.getNext(--level);
                }
            }
        }
        return null;
    }

    public SkipListNode<T> getNode(T value) {
        if (getHead()==null) return null; 
        if (getHead().getData().compareTo(value)==0) return getHead();

        SkipListNodeLevelPair<T> pair = getPredecessor(value);
        if (pair==null) return null;
        return pair.getNode().getNext(pair.getLevel());
    }

    protected void swapNode(SkipListNode<T> node, SkipListNode<T> next) {
        T value = node.getData();
        node.setData(next.getData());
        next.setData(value);
    }

    public SkipListNode<T> removeValue(T value) {
        if (getHead()==null) return null;

        SkipListNode<T> node = null;
        SkipListNode<T> prev = null;
        int lvl = 0;
        if (getHead().getData().compareTo(value)==0) {
            node = getHead();
        } else {
            // Find the predecessor of the node we are looking for and
            // which level it is found on.
            SkipListNodeLevelPair<T> pair = getPredecessor(value);
            if (pair!=null) {
                prev = pair.getNode();
                lvl = pair.getLevel();
            }
            // Cannot find predecessor of value
            if (prev == null)
                return null;
            // Use predecessor to get actual node
            node = prev.getNext(lvl);
            // SkipListNode doesn't exist
            if (node == null || node.getData().compareTo(value)!=0) 
                return null;
        }

        SkipListNode<T> next = null;
        // Head node is the only node without a prev node
        if (prev == null) {
            next = node.getNext(0);
            // Removing head
            if (next != null) {
                // Switch the value of the next into the head node
                if (creator==null) swapNode(node,next);
                else creator.swapNode(node, next);
                // Update the prev and node pointer
                prev = node;
                node = next;
            } else {
                // If head doesn't have a new node then list is empty
                setHead(null);
            }
        } else {
            // Set the next node pointer
            next = node.getNext(lvl);
        }

        // Start from the top level and move down removing the node
        int level = node.getLevel();
        for (int i=level; i>=0; i--) {
            next = node.getNext(i);
            if (prev!=null) {
                prev.setNext(i, next);
                if (i > 0) {
                    // Move down a level and look for the 'next' previous node
                    SkipListNode<T> temp = prev.getNext(i - 1);
                    while (temp != null && temp.getData().compareTo(value) != 0) {
                        prev = temp;
                        temp = temp.getNext(i - 1);
                    }
                } 
            }
        }
        size--;
        return node;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(T value) {
        SkipListNode<T> node = removeValue(value);
        return (node!=null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        setHead(null);
        size = 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(T value) {
        return (getNode(value)!=null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return size;
    }

    @Override
    public java.util.Set<T> toSet() {
        return (new JavaCompatibleSkipListSet<T>(this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public java.util.Collection<T> toCollection() {
        return (new JavaCompatibleSkipListSet<T>(this));
    }
    
 // Output a String version of the skip list. If a value and level is passed
    // then output with that node highlighted.
    public String getString(T value, int level) {
        StringBuilder builder = new StringBuilder();
        builder.append("size=").append(size).append("\n");
        SkipListNode<T> node = getHead();
        if (node!=null) {
            int iLevel = node.getLevel();
            for (int i=iLevel; i>=0; i--) {
                builder.append("[").append(i).append("] ");
                node = getHead();
                while (node != null) {
                    if (level==i && value!=null && node.getData().compareTo(value)==0) 
                        builder.append("(").append(node.getData()).append(")");
                    else 
                        builder.append(node.getData());
                    SkipListNode<T> next = node.getNext(i);
                    if (next != null)
                        builder.append("->");
                    node = next;
                }
                if (i>0) builder.append("\n");
            }
        }
        builder.append("\n");
        return builder.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return getString(null,-1);
    }

	public SkipListNode<T> getHead() {
		return head;
	}

	public void setHead(SkipListNode<T> head) {
		this.head = head;
	}

}
