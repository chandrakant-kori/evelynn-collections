package tech.evelynn.collections.set.skiplist;

import java.util.Iterator;

public class SkipListSetIterator<T extends Comparable<T>> implements Iterator<T> {

	private SkipListSet<T> list = null;
    private SkipListNode<T> next = null;
    private SkipListNode<T> last = null;

    public SkipListSetIterator(SkipListSet<T> list) {
        this.list = list;
        this.next = list.getHead();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void remove() {
        if (last==null) return;
        list.remove(last.getData());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasNext() {
        return (next!=null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T next() {
        if (next == null) throw new java.util.NoSuchElementException();

        last = next;
        next = next.getNext(0);
        return last.getData();
    }
}
