package tech.evelynn.collections.set.skiplist;

public interface SkipListNodeCreator<T extends Comparable<T>> {

	public SkipListNode<T> createNewNode(int level, T data);

	public void swapNode(SkipListNode<T> node, SkipListNode<T> next);
}
