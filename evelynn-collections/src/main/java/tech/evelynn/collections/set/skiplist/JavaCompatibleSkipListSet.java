package tech.evelynn.collections.set.skiplist;

import java.util.AbstractSet;

@SuppressWarnings("unchecked")
public class JavaCompatibleSkipListSet<T extends Comparable<T>> extends AbstractSet<T> {

	private SkipListSet<T> list = null;

    public JavaCompatibleSkipListSet(SkipListSet<T> list) {
        this.list = list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(T value) {
        return list.add(value);
    }

    /**
     * {@inheritDoc}
     */
	@Override
    public boolean remove(Object value) {
        return list.remove((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object value) {
        return list.contains((T)value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return list.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public java.util.Iterator<T> iterator() {
        return (new SkipListSetIterator<T>(list));
    }
}
