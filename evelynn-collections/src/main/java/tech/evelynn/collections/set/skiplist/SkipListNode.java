package tech.evelynn.collections.set.skiplist;

@SuppressWarnings("unchecked")
public class SkipListNode<T extends Comparable<T>> {

	private SkipListNode<T>[] next = null;

	private T data = null;

	protected SkipListNode(int level, T data) {
		this.next = new SkipListNode[level + 1];
		this.setData(data);
	}

	protected int getLevel() {
		return next.length - 1;
	}

	protected void setNext(int idx, SkipListNode<T> node) {
		this.next[idx] = node;
	}

	public SkipListNode<T> getNext(int idx) {
		if (idx >= this.next.length)
			return null;
		return this.next[idx];
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("data=").append(getData());
		if (next != null) {
			builder.append("\n").append("next=[");
			int size = next.length;
			for (int i = 0; i < size; i++) {
				SkipListNode<T> n = next[i];
				if (n != null)
					builder.append(n.getData());
				else
					builder.append("none");
				if (i != size - 1)
					builder.append(", ");
			}
			builder.append("]");
		}
		return builder.toString();
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
