package tech.evelynn.collections.set.disjoint;

@SuppressWarnings("unchecked")
public final class DisjointItem<T extends Object> {

	private DisjointItem<T> parent;
	private T value;
	/**
	 * Rank is not the actual depth of the tree rather it is an upper bound. As
	 * such, on a find operation, the rank is allowed to get out of sync with the
	 * depth.
	 **/
	private long rank;

	public DisjointItem(DisjointItem<T> parent, T value) {
		this.setParent(parent);
		this.value = value;
		this.setRank(0);
	}

	public T getValue() {
		return value;
	}

	public long getRank() {
		return rank;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof DisjointItem))
			return false;

		final DisjointItem<T> i = (DisjointItem<T>) o;
		if ((i.getParent() != null && getParent() != null) && !(i.getParent().value.equals(getParent().value)))
			return false;
		if ((i.value != null && value != null) && !(i.value.equals(value)))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "parent=" + (getParent() != null ? getParent().value : null) + " " + (getRank() > 0 ? "rank=" + getRank() + " " : "")
				+ "value=" + (value != null ? value : null);
	}

	public DisjointItem<T> getParent() {
		return parent;
	}

	public void setParent(DisjointItem<T> parent) {
		this.parent = parent;
	}

	public void setRank(long rank) {
		this.rank = rank;
	}
}
