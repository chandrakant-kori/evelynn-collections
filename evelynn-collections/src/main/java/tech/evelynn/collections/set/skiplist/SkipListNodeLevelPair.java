package tech.evelynn.collections.set.skiplist;

public final class SkipListNodeLevelPair<T extends Comparable<T>> {

	private int level = -1;
	private SkipListNode<T> node = null;

	public SkipListNodeLevelPair(int level, SkipListNode<T> node) {
		this.setLevel(level);
		this.setNode(node);
	}

	public SkipListNode<T> getNode() {
		return node;
	}

	public void setNode(SkipListNode<T> node) {
		this.node = node;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
