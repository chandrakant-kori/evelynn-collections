package tech.evelynn.collections.set;

public interface Set<T> {

	public boolean add(T value);

	public boolean remove(T value);

	public void clear();

	public boolean contains(T value);

	public int size();

	public java.util.Set<T> toSet();

	public java.util.Collection<T> toCollection();

}
